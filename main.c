#include "main.h"
#include <time.h>
#include <dirent.h>

CORE core = {NULL};

#define OUTPUT_ERRORS 1
#define OUTPUT_VERBOSE 2
#define OUTPUT_DETAILED 4

bool test_png = false;
char const *image_file = "./openstreetmap.png";

int SetDefaults( ALLOC *Alloc, BLOCKS *Blocks, CORE *Core )
{
	memset( Alloc, 0, sizeof(ALLOC) );
	Alloc->ulloc = defaultUlloc;
	Alloc->alloc = defaultAlloc;
	Alloc->out = errout;

	memset( Blocks, 0, sizeof(BLOCKS) );
	Blocks->Alloc = Alloc;

	memset( Core, 0, sizeof(CORE) );
	InitCore( Core, Blocks );
	Core->TestMode = true;
	Core->Span.x = 640;
	Core->Span.y = 480;

	return 0;
}

int TestPNG( CORE *Core, uch const * path );
int LaunchCore( CORE *Core );

int main( int argc, char *argv[] )
{
	ALLOC Alloc = {0};
	BLOCKS Blocks = {0};
	int ret = EXIT_FAILURE, err = SetDefaults( &Alloc, &Blocks, &core );
	FILE *out = Alloc.out;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		goto done;
	}

	// ./pngsuite/basn0g01.png
	// ./pngsuite/basn6a08.png
	// ./pngsuite/s01n3p01.png
	// make run ARGS="--detailed -f ./pngsuite/f00n0g08.png"
	// make debug ARGS="--detailed -f ./pngsuite/f00n0g08.png"
	// make gede ARGS="--detailed -f ./pngsuite/f00n0g08.png"

	err = ProcessArgv( &Blocks, argc, argv );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		goto done;
	}

	err = test_png ?
		TestPNG( &core, image_file ) : LaunchCore( &core );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		goto done;
	}

	ret = EXIT_SUCCESS;

	done:
	TermCore( &core );
	TermBlocks( &Blocks );
	return ret;
}

int TestPNG( CORE *Core, uch const * path )
{
	GFX *Gfx = GrabCoreGfxHandle( Core );
	IMG_FILE *ImgFile = GrabGfxImgFile( Gfx );
	FILE *file, *out = GrabImgFileBlocks( ImgFile )->Alloc->out;
	dint i = 0;
	dint err = WantImgGfxImage( Gfx, &i, path );
	GFX_IMAGE *Image;

	if ( err )
	{
		ECHO
		(
			out,
			ECHO_ERR( out, err );
			fprintf( out, "Failed to load PNG '%s'\n", path )
		);
		return EXIT_FAILURE;
	}

	Image = GrabGfxImage( Gfx, i );

	EchoImgFile( ImgFile );

	file = fopen("pic.ppm","w");
	err = errno;

	if ( !file )
	{
		fprintf( errout, "Failed to open pic.ppm for writing" );
		return err;
	}

	SaveImgFilePPM( file, &(Image->Img) );

	fclose( file );

	system("eog ./pic.ppm");

	return 0;
}

dint PreLoadImages( CORE *Core )
{
	GFX *Gfx = GrabCoreGfxHandle( Core );
	dint i;
	uch *paths[] =
	{
		u8"assets/pngsuite/f00n0g08.png",
		u8"assets/pngsuite/f00n2c08.png",
		u8"assets/pngsuite/f01n0g08.png",
		u8"assets/pngsuite/f01n2c08.png",
		u8"assets/pngsuite/f02n0g08.png",
		u8"assets/pngsuite/f02n2c08.png",
		u8"assets/pngsuite/f03n0g08.png",
		u8"assets/pngsuite/f03n2c08.png",
		u8"assets/pngsuite/f04n0g08.png",
		u8"assets/pngsuite/f04n2c08.png",
		u8"assets/pngsuite/f99n0g04.png",
		NULL
	};

	for ( i = 0; paths[i]; ++i )
	{
		dint j, err = WantImgGfxImage( Gfx, &j, paths[i] );

		if ( err )
		{
			FILE *out = GrabGfxBlocks( Gfx )->Alloc->out;
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	return 0;
}

dint NameGpuConfig( CORE *Core )
{
	GPU_COVERED *GpuCovered = &(Core->GpuCovered);
	GPU_TRAITS *GpuTraits = &(Core->GpuTraits);
	GPU_THINGS *GpuThings = &(Core->GpuThings);

	GpuTraits->Var[TRAIT_PLACE	] = "Place";
	GpuTraits->Var[TRAIT_COLOR	] = "Color";
	GpuTraits->Var[TRAIT_TEXEL	] = "Texel";
	GpuTraits->Var[TRAIT_NORMAL	] = "Normal";
	GpuTraits->Var[TRAIT_TANGENT	] = "Tangent";
	GpuTraits->Var[TRAIT_BINORMAL	] = "BiNormal";

	GpuCovered->Var = "Covered";

	GpuThings->Var[THING_STRIP] = "Strip";
	GpuThings->Var[THING_GFX_MODEL] = "GfxModel";
	GpuThings->Var[THING_STAGE] = "Stage";

	return 0;
}

dint FindGpuConfig( CORE *Core, GPU_BINARY *GpuBinary )
{
	GPU_COVERED *GpuCovered = &(Core->GpuCovered);
	GPU_TRAITS *GpuTraits = &(Core->GpuTraits);
	GPU_THINGS *GpuThings = &(Core->GpuThings);
	dint i;

	for ( i = 0; i < TRAIT_COUNT; ++i )
	{
		uint *Loc = GpuTraits->Loc + i;
		char const *var = GpuTraits->Var[i];
		glX( Core, *Loc = glGetAttribLocation( GpuBinary->ID, var ) );
	}

	GpuCovered->Loc = glGetUniformLocation( GpuBinary->ID, GpuCovered->Var );

	for ( int thing = 0; thing < THING_COUNT; ++thing )
	{
		uint *Loc = GpuThings->Loc + thing;
		char const *var = GpuThings->Var[thing];
		glX( Core, *Loc = glGetUniformLocation( GpuBinary->ID, var ) );
	}

	return 0;
}

int LaunchCore( CORE *Core )
{
	dint err;
	GPU_BINARY *GpuBinary;
	//TASK *Builder, *Painter;
	GLFWwindow *Window = NULL;
	GFX *Gfx = GrabCoreGfxHandle( Core );
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	FILE *out = Blocks->Alloc->out;

	if( !glfwInit() )
	{
		ECHO( out, fputs( "[FATAL ERROR] Couldn't initialise GLFW\n", out ) );
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL

	Window = Core->Window =
		glfwCreateWindow( Core->Span.x, Core->Span.y, "GL Example", NULL, NULL );

	if ( !Window )
	{
		ECHO( out, fputs( "[FATAL ERROR] Couldn't create window\n", out ) );
		return -1;
	}

	glfwMakeContextCurrent( Window ); // Initialize GLEW

	if ( glewInit() != GLEW_OK )
	{
		ECHO( out, fputs( "[FATAL ERROR] Couldn't initialise GLEW\n", out ) );
		return -1;
	}

	/* Initialise OpenGL */
	glDebugMessageCallback( glDebugMsgCB, Core );
	GpuBinary = Core->GpuBinary;

	err = InitGpuBinary( GpuBinary, Blocks );

	if ( err )
	{
		ECHO
		(
			out,
			ECHO_ERR( out, err );
			fputs( "Failed to setup graphics process object on CPU\n", out )
		);
		return -1;
	}

	err = WantGpuShader( GrabGpuShaders( GpuBinary ), Blocks, u8"assets/builder.glsl", GL_VERTEX_SHADER );

	if ( err )
	{
		ECHO
		(
			out,
			ECHO_ERR( out, err );
			fputs("Failed to compile vertex shader\n", out )
		);
		return -1;
	}

	err = WantGpuShader( GrabGpuShaders( GpuBinary ), Blocks, u8"assets/painter.glsl", GL_FRAGMENT_SHADER );

	if ( err )
	{
		ECHO
		(
			out,
			ECHO_ERR( out, err );
			fputs("Failed to compile fragment shader\n", out )
		);
		return -1;
	}

	err = MakeGpuBinary( GpuBinary );

	if ( err )
	{
		ECHO
		(
			out,
			ECHO_ERR( out, err );
			fputs("Failed to build graphics process\n", out )
		);
		return -1;
	}

	FindGpuConfig( Core, GpuBinary );

	if ( Core->TestMode )
	{
		err = WantTestGfxModels( Core );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}
	else
	{
		return 0;
	}

	err = WantGpuIDs( Core );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	glfwSetFramebufferSizeCallback( Window, OnResizeCB );
	glfwSetKeyCallback( Window, OnKeyCB );
	glfwSetWindowRefreshCallback( Window, RenderView );

	while ( !glfwWindowShouldClose( Window ) )
	{
		RenderView(Window);
		glfwPollEvents();
	}

	return 0;
}

void OnResizeCB( GLFWwindow *Win, int w, int h )
{
	core.Span.x = w;
	core.Span.y = h;
	glViewport( 0, 0, w, h );
}

void OnKeyCB( GLFWwindow *Win, int key, int code, int action, int mods )
{
	switch ( key )
	{
		case GLFW_KEY_ESCAPE:
			if ( action == GLFW_PRESS )
				glfwSetWindowShouldClose( Win, true );
			break;
	}
}
