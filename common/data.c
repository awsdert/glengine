#include "data.h"
void * defaultUlloc( void *ud, void *mem, size_t cap )
{
	if ( cap )
		return mem ? realloc( mem, cap ) : malloc( cap );

	if ( mem )
		free( mem );

	return NULL;
}

void * defaultAlloc( void *ud, void *mem, size_t now, size_t cap )
{
	if ( now >= cap )
		return mem;

	return defaultUlloc( ud, mem, cap );
}

void * GrabChunk( struct _BLOCK *Block, size_t size, dint i )
{
	if ( i >= 0 )
	{
		size_t pos = size * (size_t)i;

		if ( Block->size >= pos + size )
		{
			uchar *data = Block->addr;
			return data + pos;
		}
	}

	return NULL;
}

int		WantChunk( BLOCK *Block, size_t size, dint *I, dint add )
{
	int i = Block->used;

	*I = -1;

	if ( Block->have <= i )
	{
		int err = GrowBlock( Block, size, i + add );

		if ( err )
			return err;
	}

	*I = i;
	Block->used++;

	return 0;
}

int		GrowBlock( BLOCK *Block, size_t size, int want )
{
	ALLOC *Alloc = Block->Alloc;
	uchar *mem = Block->addr;
	size_t get, had = Block->size, num = Block->have;

	if ( !size )
	{
		if ( !had || !num )
			return EINVAL;

		size = had / num;
	}

	want = (want > 0) ? want : 0;
	get = (size_t)want * size;

	if ( get <= had )
		return 0;

	mem = Alloc->ulloc( Alloc->ud, mem, get );

	if ( mem )
	{
		had = Block->used * size;
		memset( mem + had, 0, get - had );

		Block->addr = mem;
		Block->size = get;
		Block->have = get / size;

		return 0;
	}

	return ENOMEM;
}

dint	CropBlock( BLOCK *Block, size_t size, int want )
{
	ALLOC *Alloc = Block->Alloc;
	size_t get, had = Block->size;
	uchar * mem = Block->addr;

	if ( want < 1 )
	{
		if ( had )
			Alloc->ulloc( Alloc->ud, mem, 0 );
		memset( Block, 0, sizeof(BLOCK) );
		return 0;
	}

	if ( !size )
		return EINVAL;

	get = size * want;

	if ( get > had )
		return EINVAL;

	if ( get == had )
		return 0;

	memset( mem + get, 0, had - get );
	mem = Alloc->ulloc( Alloc->ud, mem, get );

	if ( mem )
	{
		Block->addr = mem;
		Block->size = get;
		Block->have = get / size;

		if ( Block->used > Block->have )
			Block->used = Block->have;

		return 0;
	}

	return ENOMEM;
}

void EchoBlockDetails( FILE *file, BLOCK *Block )
{
	fprintf
	(
		file,
		"Block Details: addr = %p, "
		"used = %d, have = %d, size = %" PRIuMAX "\n",
		Block->addr, Block->used, Block->have, (uintmax_t)(Block->size)
	);
}
