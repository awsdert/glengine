#define _LARGEFILE64_SOURCE
#include "path.h"

uch const * LocFileGivenPath( LOC_FILE *LocFile )
{
	BLOCK *GivenPath = GrabBlock( LocFile->Blocks, LocFile->GivenPathID );
	return GivenPath->addr;
}

uch const * LocFileFixedPath( LOC_FILE *LocFile )
{
	BLOCK *FixedPath = GrabBlock( LocFile->Blocks, LocFile->FixedPathID );
	return FixedPath->addr;
}

dint	GivenPathToFixedPath( BLOCK *FixedPath, uch const * givenPath )
{
	FILE *out = FixedPath->Alloc->out;
	uint size = utf8not0(givenPath) + 1, d = 0, s = 0;
	dint err = GrowBlock( FixedPath, sizeof(uch), size );
	uch *fixedPath;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	fixedPath = FixedPath->addr;
	memset( fixedPath, 0, FixedPath->size );

	if ( *givenPath == '~' )
	{
		char const * HOME = getenv("HOME");
		char const * HOMEPATH = getenv("HOMEPATH");
		char const * PATH = HOME ? HOME : HOMEPATH;
		d = ansinot0( PATH );

		err = GrowBlock( FixedPath, sizeof(uch), d + size + 1 );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}

		++s;
		fixedPath = FixedPath->addr;
		memcpy( fixedPath, PATH, d );
		fixedPath[d++] = '/';
	}

	--size;
	memcpy( fixedPath + d, givenPath, size );
	d += size;
	fixedPath[d] = 0;
	FixedPath->used = d;

	return 0;
}

typedef struct _LOC_FILE_SEEMS_LIKE_CB
{
	long reads;
	LocFileSeemsLike_cb SeemsLikeCB;
	void *LoadCB;
} LOC_FILE_SEEMS_LIKE_CB;

dint GiveLocFileSeemLikeCB
(
	BLOCK *SeemsLikeCallbacks,
	LocFileSeemsLike_cb SeemsLikeCB,
	long reads,
	void *LoadCB
)
{
	LOC_FILE_SEEMS_LIKE_CB *callbacks = SeemsLikeCallbacks->addr, *callback;
	int i, err, num = SeemsLikeCallbacks->used;

	for ( i = 0; i < num; ++i )
	{
		callback = callbacks + i;

		if ( callback->SeemsLikeCB == SeemsLikeCB )
			return EALREADY;
	}

	err = WantChunk
	(
		SeemsLikeCallbacks,
		sizeof(LOC_FILE_SEEMS_LIKE_CB),
		&i, 8
	);

	if ( err )
	{
		FILE *out = SeemsLikeCallbacks->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	callbacks = SeemsLikeCallbacks->addr;
	callback = callbacks + i;
	callback->reads = reads;
	callback->SeemsLikeCB = SeemsLikeCB;
	callback->LoadCB = LoadCB;

	return 0;
}
ach const * LocFileSeemsLike
(
	BLOCK *SeemsLikeCallbacks,
	LOC_FILE *LocFile,
	void **LoadCB
)
{
	LOC_FILE_SEEMS_LIKE_CB *callbacks = SeemsLikeCallbacks->addr, *callback;
	long most = 0;
	int i, num = SeemsLikeCallbacks->used;
	ach const *desc;

	*LoadCB = NULL;

	for ( i = 0; i < num; ++i )
	{
		callback = callbacks + i;

		if ( callback->reads > most )
			most = callback->reads;
	}

	while ( most > 0 )
	{
		for ( i = 0; i < num; ++i )
		{
			callback = callbacks + i;

			if ( callback->reads != most )
				continue;

			SeekLocFileOffset( LocFile, 0, SEEK_SET );
			desc = callback->SeemsLikeCB( LocFile );

			if ( desc )
			{
				*LoadCB = callback->LoadCB;
				SeekLocFileOffset( LocFile, 0, SEEK_SET );
				return desc;
			}
		}

		--most;
	}

	SeekLocFileOffset( LocFile, 0, SEEK_SET );

	return NULL;
}

uintmax_t TellLocFileOffset( LOC_FILE *LocFile )
{
	return LocFile->fromEnd ? UINTMAX_MAX - LocFile->pos : LocFile->pos;
}

dint SeekLocFileOffset( LOC_FILE *LocFile, intmax_t seek, int from )
{
#ifdef _WIN32
	/* TODO: Implement Win32 file seek */
	return ENOSYS;
#else
	dlong did;
	dint err, fd = LocFile->fd;

	if ( from != SEEK_CUR )
	{
		errno = 0;
		did = lseek( fd, 0, from );
		err = errno;

		if ( did < 0 )
			return err;

		LocFile->pos = 0;
		LocFile->fromEnd = (from == SEEK_END);
	}

	while ( seek > LONG_MAX )
	{
		errno = 0;
		did = lseek( fd, LONG_MAX, SEEK_CUR );
		err = errno;

		if ( did < 0 )
			return err;

		seek -= LONG_MAX;
		LocFile->pos += LONG_MAX;
	}

	while ( seek < -LONG_MAX )
	{
		errno = 0;
		did = lseek( fd, -LONG_MAX, SEEK_CUR );
		err = errno;

		if ( did < 0 )
			return err;

		seek += LONG_MAX;
		LocFile->pos += LONG_MAX;
	}

	errno = 0;
	did = lseek( fd, seek, SEEK_CUR );
	err = errno;

	if ( did < 0 )
		return err;

	LocFile->pos += (seek < 0) ? -seek : seek;
	return 0;
#endif
}
