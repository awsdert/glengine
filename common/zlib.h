#ifndef INC_BUFF_ZLIB_H
#define INC_BUFF_ZLIB_H

#include "data.h"
#include "text.h"
#include "memory.h"
#include "stream.h"

void large_endian_to_local_endian( void *data, size_t size );

extern uint  zlib_lod;

typedef struct _ZLIB_SYMBOL
{
	ulong use : 1;
	ulong len : 4;
	ulong lit : 16;
	ulong src : 16;
	ulong uid;
	ulong nxt[2];
} ZLIB_SYMBOL;

typedef struct _ZLIB_IMPLIED
{
	uint get[32];
	uint cpy[32];
} ZLIB_IMPLIED;

typedef struct _ZLIB_SYMBOLS
{
	int foresee;
	uint longest;
	uint highest;
	int id, used, have;
	BLOCKS *Blocks;
	ZLIB_SYMBOL *symbols;
} ZLIB_SYMBOLS;

typedef enum _ZLIB_SYMBOLS_ID
{
	ZLIB_SYMBOLS_ID_TYPE = 0,
	ZLIB_SYMBOLS_ID_CODE,
	ZLIB_SYMBOLS_ID_LOOP,
	ZLIB_SYMBOLS_ID_COUNT
} ZLIB_SYMBOLS_ID;

#define ZLIB_MAX_TYPES (1u << 8)
#define ZLIB_MAX_CODES (1u << 16)
#define ZLIB_MAX_LOOPS (1u << 17)
#define ZLIB_TOTAL_SYMBOLS (ZLIB_MAX_TYPES + ZLIB_MAX_CODES + ZLIB_MAX_LOOPS)

typedef struct _ZLIB
{
	int IntoID;
	uint method;
	uint xflags;
	ISTREAM *IStream;
	BLOCKS *Blocks;
	ZLIB_SYMBOLS Symbols[ZLIB_SYMBOLS_ID_COUNT];
	ZLIB_SYMBOL symbols[ZLIB_TOTAL_SYMBOLS];
} ZLIB;

typedef int (*ExpandZlibIStreamTypeCB)( ZLIB *zlib );

int ExpandZlibIStreamType0( ZLIB *zlib );
int ExpandZlibIStreamType1( ZLIB *zlib );
int ExpandZlibIStreamType2( ZLIB *zlib );
int ExpandZlibIStreamType3( ZLIB *zlib );
int ExpandZlibIStream( ZLIB *zlib, ISTREAM *IStream, int IntoID );
int ExpandZlibArchive( ZLIB *zlib, int DataID, int IntoID );

extern ExpandZlibIStreamTypeCB ExpandZlibIStreamType[4];

void EchoZlibDetails( ZLIB *zlib );
void InitZlibSymbolUIDs( ZLIB_SYMBOLS *Symbols );
ZLIB_SYMBOL * SlowSeekZlibSymbol( ISTREAM *IStream, ZLIB_SYMBOLS *Symbols );
ZLIB_SYMBOL * SeekZlibSymbol( ISTREAM *IStream, ZLIB_SYMBOLS *Symbols );
int SortZlibSymbolsByUID( ZLIB *zlib, ZLIB_SYMBOLS_ID id );

int LoadZlibTypes( ZLIB *zlib );
int LoadZlibHuffs( ZLIB *zlib );

void EchoZlibSymbolDetails( FILE *dst, ZLIB *zlib, ZLIB_SYMBOL *symbol );
void EchoZlibSymbolsListDetails( FILE *dst, ZLIB *zlib, int id );

int SetupZlibObject( ZLIB *zlib, BLOCKS *buffers );

#endif
