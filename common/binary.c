#include "data.h"

int 	GrowBinary( struct _BINARY *Binary, intmax_t want )
{
	BIT Used = Binary->Used;
	BLOCK *Data = &(Binary->Block);
	intmax_t have = Binary->Have.abs;
	uchar *data = Data->addr;

	Data->used = Used.pos + (Used.mask > 1);

	if ( !data || want > have )
	{
		int err;
		BIT Want;

		GrabBit( &Want, data, want );
		err = GrowBlock( Data, 1, Want.pos + 2 );

		if ( err )
			return err;

		data = Data->addr;
		Want.abs = Data->size;
		Want.abs *= CHAR_BIT;

		GrabBit( &(Binary->Used), data, Binary->Used.abs );
		GrabBit( &(Binary->Have), data, Want.abs );
		Used = Binary->Used;
	}

	if ( want < 1 )
		return 0;

	for ( ; Used.abs < have; IncBit( &Used, data ) )
		*((uchar*)(Used.data)) &= ~(Used.mask);

	return 0;
}

int		CropBinary( struct _BINARY *Binary, intmax_t want )
{
	BIT Want;
	BLOCK *Block = &(Binary->Block);

	want = (want > 0) ? want : 0;

	if ( want > Binary->Used.abs )
		return EINVAL;

	GrabBit( &Want, Block->addr, want );
	return CropBlock( Block, 1, want ? Want.pos + !!(Want.mask) : 0 );
}

int		GrabBinaryBit( struct _BINARY *Binary, BIT *bit, intmax_t i )
{
	if ( i >= 0 && Binary->Used.abs > i )
	{
		GrabBit( bit, Binary->Block.addr, i );
		return 0;
	}

	memset( bit, 0, sizeof(BIT) );
	return ERANGE;
}
