#ifndef INC_COMMON_PATH_H
#define INC_COMMON_PATH_H

#include "data.h"
#include "text.h"

#ifndef _WIN32
#include <fcntl.h>
#endif

#include <stdlib.h>

/* Warning, this will not be implemented for a while */
typedef struct _NET_FILE
{
	int err;
	BLOCKS *Blocks;
} NET_FILE;

typedef struct _LOC_FILE
{
	int err, GivenPathID, FixedPathID, BriefDataID;
	uintmax_t pos;
	bool fromEnd;
	BLOCKS *Blocks;
#ifdef _WIN32
	void * handle;
#else
	int fd;
#endif
} LOC_FILE;


#define FLG_FILE_OWN_EX		0100
#define FLG_FILE_OWN_WR		0200
#define FLG_FILE_OWN_RD		0400
#define FLG_FILE_OWN_RW		0600
#define FLG_FILE_OWN_RWX	0700

#define FLG_FILE_GRP_EX		010
#define FLG_FILE_GRP_WR		020
#define FLG_FILE_GRP_RD		040
#define FLG_FILE_GRP_RW		060
#define FLG_FILE_GRP_RWX	070

#define FLG_FILE_ANY_EX		01
#define FLG_FILE_ANY_WR		02
#define FLG_FILE_ANY_RD		04
#define FLG_FILE_ANY_RW		06
#define FLG_FILE_ANY_RWX	07

#define FLG_FILE_ALL_EX		0111
#define FLG_FILE_ALL_WR		0222
#define FLG_FILE_ALL_RD		0444
#define FLG_FILE_ALL_RW		0666
#define FLG_FILE_ALL_RWX	0777

#define FLG_FILE_RD			0444
#define FLG_FILE_RDX		0555
#define FLG_FILE_RW			0644
#define FLG_FILE_RWX		0755

uch const *LocFileGivenPath( LOC_FILE *LocFile );
uch const *LocFileFixedPath( LOC_FILE *LocFile );
dint	GivenPathToFixedPath( BLOCK *FixedPath, uch const *givenPath );

dint	InitLocFileObject( LOC_FILE *BinFile, BLOCKS *Blocks );
void	TermLocFileObject( LOC_FILE *LocFile );
/* Pass NULL to path to close an open file, flags & mode will be ignored in
 * that case, passing a different path will also close the file so that the
 * next file can be opened */
dint	SeekBinFile( LOC_FILE *BinFile, uch const *path, uint opt, uint mode );
/* If the value returned is not NULL it is treated as a match */
typedef ach const * (*LocFileSeemsLike_cb)( LOC_FILE *BinFile );
/* Callbacks that read the most bytes will be called 1st, any below will be
 * called after, this ensures no files can be mis-identified as one that uses
 * a smaller signature, loadCB can have any number of parameters so we just
 * note it's address (can be NULL, won't be called by this API) so that we can
 * pass it onto the function that called LocFileSeemsLike() */
dint	GiveLocFileSeemLikeCB
(
	BLOCK *SeemsLikeCallbacks,
	LocFileSeemsLike_cb SeemsLikeCB,
	long reads,
	void *loadCB
);
ach const * LocFileSeemsLike
(
	BLOCK *SeemsLikeCallbacks,
	LOC_FILE *LocFile,
	void **loadCB
);
uintmax_t TellLocFileOffset( LOC_FILE *LocFile );
dint	SeekLocFileOffset( LOC_FILE *LocFile, intmax_t seek, int from );
dllong	CopyEntireBinFile( LOC_FILE *BinFile, BLOCK *Into, dint attempts );

int CopyBinFileRegion
(
	LOC_FILE 	*BinFile,
	void 		*data,
	long		size,
	long	 	*done,
	dint		attempts
);
int FillBinFileRegion
(
	LOC_FILE 	*BinFile,
	void const *data,
	long		size,
	long	 	*done,
	dint		attempts
);

#endif
