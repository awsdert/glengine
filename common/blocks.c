#include "data.h"

int MoreBlocks( BLOCKS *Blocks, int want )
{
	BINARY *Using = &(Blocks->Using);
	BIT *Used = &(Using->Used);
	int num = Used->abs + want;

	if ( num < 0 )
		return ERANGE;

	return GrowSeries( Blocks, sizeof(BLOCK), num );
}

void TermBlocks( BLOCKS *Blocks )
{
	int i;
	ALLOC *Alloc = Blocks->Alloc;
	BLOCK *blocks = Blocks->Block.addr;

	Blocks->Block.Alloc = Alloc;
	Blocks->Using.Block.Alloc = Alloc;

	for ( i = 0; i < Blocks->Using.Used.abs; ++i )
	{
		BLOCK *Block = blocks + 1;
		Block->Alloc = Alloc;
		CropBlock( Block, 1, 0 );
	}

	CropSeries( Blocks, sizeof(BLOCK), 0 );
}

BLOCK * GrabBlock( BLOCKS *Blocks, int i )
{
	BLOCK *Block = GrabEntry( Blocks, sizeof(BLOCK), i );

	if ( Block )
	{
		Block->Alloc = Blocks->Alloc;
		return Block;
	}

	return NULL;
}

void	FreeBlock( BLOCKS *Blocks, int i )
{
	BLOCK *Block = GrabBlock( Blocks, i );

	if ( Block )
	{
		Block->used = 0;
		Block->have = 0;
		FreeEntry( Blocks, i );
	}
}

int WantBlock( BLOCKS *Blocks, int *I, size_t size, int want )
{
	int i = 0, err, num = Blocks->Block.used, biggest = -1;
	uchar *active = Blocks->Using.Block.addr;
	BLOCK *blocks = Blocks->Block.addr, *Block, *Biggest = NULL;
	FILE *out = Blocks->Alloc->out;
	size_t need = size * (size_t)want;
	BIT pos;

	*I = -1;

	if ( !size )
		return EINVAL;

	while ( 1 )
	{
		(void)UsableEntry( Blocks, sizeof(BLOCK), &i, 0 );

		if ( i < 1 )
			break;

		blocks = Blocks->Block.addr;
		Block = blocks + i;

		if ( !Biggest || Biggest->size < Block->size )
		{
			Biggest = Block;
			biggest = i;
		}

		if ( Block->size < need )
			continue;

		goto done;
	}

	if ( Biggest )
	{
		i = biggest;
		goto done;
	}

	i = 0;
	err = UsableEntry( Blocks, sizeof(BLOCK), &i, 32 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	blocks = Blocks->Block.addr;
	active = Blocks->Using.Block.addr;

	done:
	*I = i;
	GrabBit( &pos, active, i );
	*((uchar*)(pos.data)) |= pos.mask;
	Block = blocks + i;
	Block->Alloc = Blocks->Alloc;
	Block->have = Block->size / size;
	Block->used = 0;

	if ( i >= num )
	{
		err = UsableEntries( Blocks, i + 1 );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	if ( !want )
		return 0;

	return GrowBlock( Block, size, want );
}
