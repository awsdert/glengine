#include "path.h"

int InitLocFileObject( LOC_FILE *BinFile, BLOCKS *Blocks )
{
	int err,
		*GivenPathID = &(BinFile->GivenPathID),
		*FixedPathID = &(BinFile->FixedPathID),
		*BriefDataID = &(BinFile->BriefDataID);
	FILE *out = Blocks->Alloc->out;

	BinFile->Blocks = Blocks;

	err = WantBlock( Blocks, GivenPathID, sizeof(uch), 1 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermLocFileObject( BinFile );
		return err;
	}

	err = WantBlock( Blocks, FixedPathID, sizeof(uch), 1 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermLocFileObject( BinFile );
		return err;
	}

	err = WantBlock( Blocks, BriefDataID, sizeof(tch), 1 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermLocFileObject( BinFile );
		return err;
	}

	return 0;
}

int SeekBinFile( LOC_FILE *BinFile, uch const *path, uint flags, uint mode )
{
	BLOCKS *Blocks = BinFile->Blocks;
	BLOCK *GivenPath, *FixedPath, *BriefData;
	int err = 0;
	tch *localPath;
	uch *givenPath = NULL, *fixedPath = NULL;
	FILE *out = Blocks->Alloc->out;

	GivenPath = GrabBlock( Blocks, BinFile->GivenPathID );
	FixedPath = GrabBlock( Blocks, BinFile->FixedPathID );
	BriefData = GrabBlock( Blocks, BinFile->BriefDataID );

	if ( !GivenPath || !FixedPath || !BriefData )
	{
		err =  EINVAL;
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	if ( !path )
	{
		GivenPath->used = 0;
		FixedPath->used = 0;
		BriefData->used = 0;

#ifdef _WIN32
		if ( BinFile->handle )
		{
			CloseHandle( BinFile->handle );
			BinFile->handle = NULL;
		}
#else
		if ( BinFile->fd >=0 )
		{
			close( BinFile->fd );
			BinFile->fd = -1;
		}
#endif
		return 0;
	}

	err = utf8tobuff( GivenPath, path );

	if ( err )
	{
		ECHO( out,ECHO_ERR( out, err ) );
		return err;
	}

	givenPath = GivenPath->addr;
	err = GivenPathToFixedPath( FixedPath, givenPath );

	if ( err )
	{
		ECHO( out,ECHO_ERR( out, err ) );
		return err;
	}

	fixedPath = FixedPath->addr;
	err = GrowBlock( BriefData, sizeof(tch), FixedPath->used + 1 );

	if ( err )
	{
		ECHO( out,ECHO_ERR( out, err ) );
		return err;
	}

	localPath = BriefData->addr;
	err = utf8totext( localPath, BriefData->have, fixedPath );

	if ( err )
	{
		ECHO( out,ECHO_ERR( out, err ) );
		return err;
	}

#ifdef _WIN32
	SetLastError(0);
	BinFile->handle = OpenFile( locEncPath );
	err = GetLastError();

	switch ( err )
	{
	case ERROR_FILE_DOES_NOT_EXIT: err = ENOENT; break;
	}

	return handle ? 0 : err;
#else
	if ( access( localPath, 0 ) != 0 )
	{
		BinFile->fd = creat( localPath, mode );
		BinFile->err = errno;
	}
	else
	{
		uint todo = 0;
		if ( flags & 01 )
		{
			flags |= 04;
			/* TODO: Use execution flags instead of quiting */
			return EINVAL;
		}

		if ( flags & 02 )
			todo |= (flags & 04) ? O_RDWR : O_WRONLY;
		else if ( flags & 04 )
			todo |= O_RDONLY;

		errno = 0;
		BinFile->fd = open( localPath, todo );
		BinFile->err = err = errno;
	}

	return (BinFile->fd >= 0) ? 0 : err;
#endif
}

int FillBinFileRegion( LOC_FILE *BinFile, void const *data, long size, long *DONE, dint attempts )
{
	int i = 0, err;
	long done = 0;

	/* If something needs this many tries to read then there's an issue outside
	 * the software */
	if ( attempts < 0 )
		attempts = SCHAR_MAX;

#ifdef _WIN32
	do
	{
		BOOL ret;
		DWORD did;

		SetLastError(0);
		ret = WriteFile( BinFile->handle, data, size, &did );
		err = GetLastError(0);

		if ( did == TRUE )
		{
			done += bytes;

			if ( err == 0 )
				return done;
		}

		++i;
	}
	while ( i < attempts );
#else
	do
	{
		ssize_t did;

		errno = 0;
		did = write( BinFile->fd, data, size );
		err = errno;

		if ( did >= 0 )
		{
			done += did;

			if ( err == 0 )
				break;
		}

		ECHO( errout, ECHO_ERR( errout, err ) );

		if ( err == EBADF )
			break;

		++i;
	}
	while ( i < attempts );
#endif
	if ( DONE )
		*DONE = done;

	return err;
}

dint CopyBinFileRegion( LOC_FILE *BinFile, void *data, long size, long *DONE, dint attempts )
{
	int i = 0, err = 0;
	long done = 0;

	/* If something needs this many tries to read then there's an issue outside
	 * the software */
	if ( attempts < 0 )
		attempts = SCHAR_MAX;

#ifdef _WIN32
	do
	{
		BOOL ret;
		DWORD did;

		SetLastError(0);
		ret = ReadFile( BinFile->handle, data, size, &did );
		err = GetLastError(0);

		if ( did == TRUE )
		{
			done += bytes;

			if ( err == 0 )
				break;
		}

		++i;
	}
	while ( i < attempts );
#else
	do
	{
		ssize_t did;

		errno = 0;
		did = read( BinFile->fd, data, size );
		err = errno;

		if ( did >= 0 )
		{
			done += did;

			if ( err == 0 )
				break;
		}

		ECHO( errout, ECHO_ERR( errout, err ) );

		if ( err == EBADF )
			break;

		++i;
	}
	while ( i < attempts );
#endif
	if ( DONE )
		*DONE = done;

	return err;
}

dllong CopyEntireBinFile( LOC_FILE *BinFile, BLOCK *Into, dint attempts )
{
	dllong done = 0, need = 0;
	long size = 0;

	do
	{
		uchar *into;
		int err;
		done += size;
		need += BUFSIZ;
		err = GrowBlock( Into, 1, need );

		if ( err )
			return done;

		into = Into->addr;
		err = CopyBinFileRegion( BinFile, into + done, BUFSIZ, &size, attempts );
	}
	while ( size == BUFSIZ );

	if ( size < 0 )
		return done;

	return done + size;
}

void TermLocFileObject( LOC_FILE *BinFile )
{
	BLOCKS *Blocks = BinFile->Blocks;

	if ( Blocks )
	{
		SeekBinFile( BinFile, NULL, 0, 0 );

		FreeBlock( Blocks, BinFile->BriefDataID );
		FreeBlock( Blocks, BinFile->FixedPathID );
		FreeBlock( Blocks, BinFile->GivenPathID );

		memset( BinFile, 0, sizeof(LOC_FILE) );
	#ifndef _WIN32
		BinFile->fd = -1;
	#endif
	}
}
