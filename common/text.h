#ifndef INC_BUFF_TEXT_H
#define INC_BUFF_TEXT_H

#include "data.h"
#include <wchar.h>
#include <uchar.h>

typedef char ach;

#if CHAR_BIT < 8
typedef wchar_t uch;
#define UCH_MAX WCHAR_MAX
#define UCH_MIN WCHAR_MIN
#else
typedef char uch;
#define UCH_MAX CHAR_MAX
#define UCH_MIN CHAR_MIN
#endif

#ifndef CHAR16_MAX
#define CHAR16_MAX UINT_LEAST16_MAX
#endif

#ifndef CHAR32_MAX
#define CHAR32_MAX UINT_LEAST32_MAX
#endif

typedef char16_t c16;
#define C16_MAX CHAR16_MAX
typedef char32_t c32;
#define C32_MAX CHAR32_MAX

#ifdef _WIN32
typedef WCHAR	wch;
typedef TCHAR	tch;
typedef enum _CHMODE
{
	CHMODE_SWAP = 0,
	CHMODE_SKIP = WC_NO_BEST_FIT_CHARS,
	CHMODE_NEAR = WC_COMPOSITECHECK
} CHMODE;
typedef UINT CHTYPE;
typedef struct	_CTOKEN
{
	bool 	wide;
	uint	left;
	void *	data;
	UINT	type;
} CTOKEN;
typedef struct	_CHCONV
{
	/* Absolute as in no characters had to be swapped out for sub or similar */
	bool	abs;
	int		err;
	ach *	sub;
	CHMODE	mode;
	CTOKEN	src;
	CTOKEN	dst;
} CHCONV;
#define WhatTextEncoding() ((AreFileApisANSI() == TRUE) ? CP_ACP : CP_OEMCP)
#define CHTYPE_UTF7	CP_UTF7
#define CHTYPE_UTF8	CP_UTF8
#else
#include <iconv.h>
typedef wchar_t	wch;
typedef char	tch;
typedef enum _CHMODE
{
	CHMODE_SWAP = 0,
	CHMODE_SKIP,
	CHMODE_NEAR,
} CHMODE;
#define CHTYPE_BUFSIZ 32
typedef char * CHTYPE;
typedef struct	_CTOKEN
{
	bool 	wide;
	size_t	left;
	void *	data;
	char	type[CHTYPE_BUFSIZ];
} CTOKEN;
typedef struct _CHCONV
{
	bool	abs;
	int		err;
	ach *	sub;
	CHMODE	mode;
	CTOKEN	src;
	CTOKEN	dst;
	iconv_t _iconv;
} CHCONV;
#define WhatTextEncoding() "UTF8"
#define CHTYPE_UTF7	"UTF7"
#define CHTYPE_UTF8	"UTF8"
#endif

/* Besides _iconv you MUST initialise the members manually,
 * pre-initialisation of {0} during declarition is recommended to catch
 * uninitialised members */
size_t	Wide2Text( CHCONV *ctx );
size_t	Text2Wide( CHCONV *ctx );

/* Disregards if multi-byte character */
size_t	ansinot0( ach const *text );
size_t	utf8not0( uch const *text );
size_t	widenot0( wch const *text );
size_t	textnot0( tch const *text );

/* Treats multi-byte characters as one whole character */
size_t	ansileng( ach const *text );
size_t	utf8leng( uch const *text );
size_t	wideleng( wch const *text );
size_t	textleng( tch const *text );

dint	ansicmpi( ach const *a, ach const *b );

/* Pass into a uint_least32_t[2] array and recast to (uch*) or (ach*) to then
 * append onto relevant string */
uint_least32_t wch2ach( wch c );
uint_least32_t wch2uch( wch c );
uint_least32_t tch2ach( tch c );
uint_least32_t tch2uch( tch c );

dint 	ansitoansi( ach *into, size_t size, ach const *text );
dint	utf8toansi( ach *into, size_t size, uch const *text );
dint 	widetoansi( ach *into, size_t size, wch const *text );
dint	texttoansi( ach *into, size_t size, tch const *text );
dint	ansitobuff( BLOCK *Into, ach const *text );

dint	ansitoutf8( uch *into, size_t size, ach const *text );
dint	utf8toutf8( uch *into, size_t size, uch const *text );
dint	widetoutf8( uch *into, size_t size, wch const *text );
dint	texttoutf8( uch *into, size_t size, tch const *text );
dint	utf8tobuff( BLOCK *Into, uch const *text );

dint	ansitowide( wch *into, size_t size, ach const *text );
dint	utf8towide( wch *into, size_t size, uch const *text );
dint	widetowide( wch *into, size_t size, wch const *text );
dint	texttowide( wch *into, size_t size, tch const *text );
dint	widetobuff( BLOCK *Into, wch const *text );

dint	ansitotext( tch *into, size_t size, ach const *text );
dint	utf8totext( tch *into, size_t size, uch const *text );
dint	widetotext( tch *into, size_t size, wch const *text );
dint	texttotext( tch *into, size_t size, tch const *text );
dint	texttobuff( BLOCK *Into, tch const *text );

#endif
