#ifndef INC_BUFF_COMMON_H
#define INC_BUFF_COMMON_H

#include "unit.h"

#define LECHO( PATH, LINE, FILE, CODE ) \
	if ( FILE ) { fprintf( FILE, "%s:%u: ", PATH, LINE ); CODE; }

#define ECHO( FILE, CODE ) LECHO( __FILE__, __LINE__, FILE, CODE )

#define ECHO_ERR( FILE, ERR ) \
	fprintf( FILE, "Error 0x%08X (%d) '%s'\n", (ERR), (ERR), strerror(ERR) )

#ifdef _DEBUG
#define errout stderr
#else
#define errout stdout
#endif

typedef void* (*UllocCB)( void *ud, void *mem, size_t cap );
typedef void* (*AllocCB)( void *ud, void *ptr, size_t now, size_t nxt );
void * defaultUlloc( void *ud, void *mem, size_t cap );
void * defaultAlloc( void *ud, void *ptr, size_t now, size_t cap );

typedef struct _ALLOC
{
	int fault;
	void *ud;
	UllocCB ulloc;
	AllocCB alloc;
	uint  lod; /* Level of Detail */
	FILE *out; /* stdout, stderr, fopen(path,"w") */
	bool out_must_be_shut;
} ALLOC;

/* pos = abs / CHAR_BIT, rel = abs % CHAR_BIT */
typedef struct _BIT
{
	intmax_t abs;
	long pos;
	long rel;
	uchar const * data;
	uchar	mask;
} BIT;

void GrabBit( BIT *b, void const *root, intmax_t abs );
#define AddBit( BIT, ROOT, ABS ) GrabBit( BIT, ROOT, (BIT)->abs + (ABS) )
#define SubBit( BIT, ROOT, ABS ) GrabBit( BIT, ROOT, (BIT)->abs + (ABS) )
#define IncBit( BIT, ROOT ) GrabBit( BIT, ROOT, (BIT)->abs + 1 )
#define DecBit( BIT, ROOT ) GrabBit( BIT, ROOT, (BIT)->abs - 1 )

void EchoBits( FILE *file, void const *src, intmax_t pos, intmax_t num, intmax_t pad, int padWith );
void ForeBits( void *DST, void const *SRC, intmax_t pos, intmax_t num );
void BackBits( void *DST, void const *SRC, intmax_t pos, intmax_t num );
void FlipBits( void *SRC, intmax_t num );

typedef struct _BLOCK
{
	ALLOC *Alloc;
	size_t size;
	int have;
	int used;
	void *addr;
} BLOCK, ANSI, UTF8, WIDE, TEXT;

void	EchoBlock( BLOCK *Block );
dint	InitBlock( BLOCK *Block, ALLOC *Alloc );
dint	GrowBlock( BLOCK *Block, size_t size, int want );
dint	CropBlock( BLOCK *Block, size_t size, int want );
void *	GrabChunk( BLOCK *Block, size_t size, int i );
dint	WantChunk( BLOCK *Block, size_t size, int *I, int add );

typedef struct _BINARY
{
	BIT Used, Have;
	BLOCK Block;
} BINARY;

dint	GrowBinary( struct _BINARY *Binary, intmax_t want );
dint	CropBinary( struct _BINARY *Binary, intmax_t want );
dint	GrabBinaryBit( struct _BINARY *Binary, BIT *bit, intmax_t i );

typedef struct _SERIES
{
	ALLOC *Alloc;
	BINARY Using;
	BLOCK Block;
} SERIES, BLOCKS;

dint	InitSeries( struct _SERIES *Series, ALLOC *Alloc );
dint	GrowSeries( struct _SERIES *Series, size_t size, int want );
dint	CropSeries( struct _SERIES *Series, size_t size, int want );
void *	GrabEntry( struct _SERIES *Series, size_t size, int i );
dint	UsableEntry( struct _SERIES *Series, size_t size, int *I, int add );
dint	UsableEntries( struct _SERIES *Series, int count );
dint	WantEntry( struct _SERIES *Series, size_t size, int *I, int add );
void	FreeEntry( struct _SERIES *Series, int i );

#define InitBlocks InitSeries
void	TermBlocks( BLOCKS *Blocks );
dint	MoreBlocks( BLOCKS *Blocks, int want );
dint	WantBlock( BLOCKS *Blocks, int *block, size_t size, int want );
BLOCK *	GrabBlock( BLOCKS *Blocks, int i );
void	FreeBlock( BLOCKS *Blocks, int i );

typedef struct _DATA
{
	ALLOC *Alloc;
	size_t size;
	void *addr;
} DATA;

/* If take is less than 0 then the value stored is returned, otherwise the
 * value will be overwritten and take returned on success, negative return
 * value always means a failure */

#endif

