#include "stream.h"

long IStreamDataFromBufferObject( void *mem, void *ud, long pos, long num )
{
	BLOCK *Data = ud;
	uchar *data;
	long upto;
	long size = Data->size / Data->have;
	long used = Data->used * size;

	if ( pos >= used )
		return 0;

	data = Data->addr;
	upto = used - pos;

	num = (num <= upto) ? num : upto;
	memcpy( mem, data + pos, num );
	return num;
}

long IStreamDataFromBufferIndex( void *mem, void *ud, long pos, long num )
{
	ISTREAM_BLOCK_ID *Id = ud;
	BLOCK *Data = GrabBlock( Id->Blocks, Id->id );
	return IStreamDataFromBufferObject( mem, Data, pos, num );
}

long OStreamDataIntoBufferObject( void *mem, void *ud, long pos, long num )
{
	BLOCK *Data = ud;
	uchar *data;
	long end = pos + num;

	if ( end < 0 )
		return -1;

	if ( (size_t)end >= Data->size )
	{
		long get = intervalof( end, BUFSIZ );
		int err = GrowBlock( Data, 1, get );

		if ( err )
			return err;

		data = Data->addr;
	}

	data = Data->addr;
	end = Data->used - pos;

	num = (num <= end) ? num : end;
	memcpy( data + pos, mem, num );
	return num;
}

long OStreamDataIntoBufferIndex( void *mem, void *ud, long pos, long num )
{
	ISTREAM_BLOCK_ID *Id = ud;
	BLOCK *Data = GrabBlock( Id->Blocks, Id->id );
	return IStreamDataFromBufferObject( mem, Data, pos, num );
}

int OStreamPrepFromBufferObject( void *ud, size_t size )
{
	BLOCK *Data = ud;
	return GrowBlock( Data, 1, size );
}

int OStreamPrepFromBufferIndex( void *ud, size_t size )
{
	ISTREAM_BLOCK_ID *Id = ud;
	BLOCK *Data = GrabBlock( Id->Blocks, Id->id );
	return OStreamPrepFromBufferObject( Data, size );
}

void EchoBits( FILE *file, void const *src, intmax_t pos, intmax_t num, intmax_t pad, int padWith )
{
	intmax_t max;
	BIT b;

	if ( pos < 0 )
		return;

	max = pos + num;

	while ( pad > num )
	{
		fputc( padWith, file );
		--pad;
	}

	for ( GrabBit( &b, src, max - 1 ); b.abs >= pos; DecBit( &b, src ) )
		fputc( '0' + !!(*(b.data) & b.mask), file );
}

void EchoBitDetails( FILE *dst, BIT *bit )
{
	fprintf
	(
		dst,
		"Bit Details: abs = %" PRIdMAX ", "
		"pos = %ld, rel = %ld, byte = %p\n",
		bit->abs,
		bit->pos,
		bit->rel,
		(void*)(bit->data)
	);
}

void GrabBit( BIT *bit, void const *root, long abs )
{
	uchar const *base = root;
	bit->abs = abs;
	bit->pos = abs / CHAR_BIT;
	bit->rel = abs % CHAR_BIT;
	bit->mask = ((uintmax_t)1) << bit->rel;
	bit->data = base + bit->pos;
}

void ForeBits( void *DST, void const *SRC, long POS, long GET )
{
	long got;
	BIT d = {0}, s;

	GrabBit( &d, DST, 0 );
	GrabBit( &s, SRC, POS );

	for ( got = 0; got < GET; ++got )
	{
		*((uchar*)(d.data)) |= (*(s.data) & s.mask) ? d.mask : 0;
		IncBit( &d, DST );
		IncBit( &s, SRC );
	}
}

void BackBits( void *DST, void const *SRC, long POS, long GET )
{
	long got;
	BIT d = {0}, s;

	GrabBit( &d, DST, GET - 1 );
	GrabBit( &s, SRC, POS );

	for ( got = 0; got < GET; ++got )
	{
		*((uchar*)(d.data)) |= (*(s.data) & s.mask) ? d.mask : 0;
		DecBit( &d, DST );
		DecBit( &s, SRC );
	}
}

uintmax_t GetBits( void const * data, uint pos, uint bits, bool backwards )
{
	uintmax_t value = 0;

	bits = (bits < bitsof(uintmax_t)) ? bits : bitsof(uintmax_t);

	if ( backwards )
		BackBits( &value, data, pos, bits );
	else
		ForeBits( &value, data, pos, bits );

	return  value;
}

void FlipBits( void *SRC, intmax_t num )
{
	BIT d, s;

	GrabBit( &d, SRC, 0 );
	GrabBit( &s, SRC, num - 1 );

	while ( d.abs < s.abs )
	{
		/* Store state */
		uchar dst = *(d.data) & d.mask;
		uchar src = *(s.data) & s.mask;

		/* Clear bit */
		*((uchar*)(d.data)) &= ~(d.mask);
		*((uchar*)(s.data)) &= ~(s.mask);

		/* Use state to set bit */
		*((uchar*)(d.data)) |= src ? d.mask : 0;
		*((uchar*)(s.data)) |= dst ? s.mask : 0;

		IncBit( &d, SRC );
		DecBit( &s, SRC );
	}
}

void IStreamPull( ISTREAM *IStream, uint bits )
{
	intmax_t max = BUFSIZ;
	uchar *src = IStream->feed;
	max *= CHAR_MAX;
	IStream->took += bits;
	IStream->used += bits;

	if ( IStream->took >= max && IStream->temp > 0 )
	{
		IStream->have = IStream->temp * CHAR_BIT;
		IStream->temp = 0;
		IStream->back = false;
		memmove( src, src + BUFSIZ, BUFSIZ );
	}
	else if ( IStream->eod && IStream->took >= max )
		IStream->err = EOF;
}

long IStreamBlock( ISTREAM * IStream )
{
	uchar *src = IStream->feed;
	long add = -1;

	if ( IStream->temp > 0 )
	{
		src += BUFSIZ;
		return IStream->temp;
	}

	memset( src, 0, BUFSIZ );

	if ( IStream->eod )
		IStream->err = EOF;
	else
	{
		add = IStream->DataCB( src, IStream->ud, IStream->byte, BUFSIZ );
		IStream->eod = (add < BUFSIZ);

		if ( add >= 0 )
			IStream->byte += add;

		if ( IStream->back )
			IStream->temp = add;
	}

	return add;
}

int OStreamPush( OSTREAM *OStream )
{
	ulong give = OStream->have;
	long done = OStream->DataCB(
		OStream->feed, OStream->ud, OStream->byte, give );

	if ( done < 0 )
		return -1;

	memset( OStream->feed, 0, BUFSIZ );
	OStream->gave += done * CHAR_BIT;
	OStream->byte += done;
	OStream->have = 0;
	OStream->give = 0;
	return done;
}

int OStreamData( OSTREAM *OStream, void const *mem, intmax_t num, bool push )
{
	BIT pos, bit;
	ulong max;
	uchar *src;

	if ( !mem || num < 1 )
		return EINVAL;

	max = 0L ;
	src = OStream->feed;

	GrabBit( &bit, mem, 0 );
	GrabBit( &pos, src, 0 );

	while ( bit.abs < num )
	{
		while ( pos.abs >= max )
		{
			int done = OStreamPush( OStream );

			if ( done < 0 )
				return -1;

			pos.abs -= max;
		}

		while ( pos.abs < max && bit.abs < num )
		{
			*((uchar*)bit.data) |= (*(pos.data) & pos.mask) ? bit.mask : 0;
			IncBit( &pos, src );
			IncBit( &bit, mem );
		}
	}

	return 0;
}

int OStreamBits( OSTREAM *OStream, uintmax_t val, uint num, bool push )
{
	uchar raw[sizeof(uintmax_t)] = {0};
	size_t i;

	for ( i = 0; i < sizeof(uintmax_t); ++i )
		raw[i] = (val >> (i * CHAR_BIT)) & UCHAR_MAX;

	return OStreamData( OStream, raw, num, push );
}

int IStreamData( ISTREAM *IStream, void *mem, intmax_t num, bool pull )
{
	BIT d, s;
	intmax_t max = IStream->have;
	intmax_t use;
	uchar * src = IStream->feed;

	if ( !max )
	{
		IStream->took = 0;
		max = IStreamBlock( IStream );

		if ( max < 0 )
			return IStream->err;

		if ( !max )
		{
			IStream->err = EOF;
			return EOF;
		}

		max *= CHAR_BIT;
		IStream->have = max;
	}

	use = max - IStream->took;

	GrabBit( &d, mem, 0 );
	GrabBit( &s, src, IStream->took );

	while ( d.abs < num )
	{
		while ( s.abs < max && d.abs < num )
		{
			if ( *(s.data) & s.mask )
				*((uchar*)(d.data)) |= d.mask;

			IncBit( &d, mem );
			IncBit( &s, src );
		}

		if ( d.abs == num )
		{
			if ( pull )
				IStreamPull( IStream, num );

			return 0;
		}
		else if ( !pull )
		{
			max = BUFSIZ;
			max *= CHAR_BIT;

			if ( num - d.abs > max || src != IStream->feed )
				return ERANGE;

			IStream->back = true;
			src += BUFSIZ;
		}

		max = IStreamBlock( IStream );

		if ( max < 0 )
			return EOF;

		if ( !max )
			return ENODATA;

		max *= CHAR_BIT;

		if ( pull )
		{
			IStream->took = 0;
			IStream->have = max;
			IStreamPull( IStream, use );
		}

		use = max;
	}

	return 0;
}

uintmax_t IStreamBits( ISTREAM *IStream, uint num, bool pull )
{
	uchar raw[sizeof(uintmax_t)] = {0};
	uintmax_t val = 0;
	int i;

	if ( num > bitsof(uintmax_t) )
	{
		IStream->err = ERANGE;
		return 0;
	}

	IStream->err = IStreamData( IStream, raw, num, pull );

	/* This prevents endian based issues when passing on what has been
	 * retrieved */
	for ( i = 0; i < sizeof(uintmax_t); ++i )
		val |= ((uintmax_t)raw[i]) << (i * CHAR_BIT);

	return val;
}

void EchoIStreamDetails( FILE *file, ISTREAM *IStream )
{
	fprintf
	(
		file,
		"IStream Details: "
		"End Of Data = %5s, "
		"End Of IStream = %5s, "
		"data = %p, "
		"byte = %ld, "
		"took = %" PRIdMAX ", "
		"used = %" PRIdMAX "\n",
		BOOL2STR( IStream->eod ),
		BOOL2STR( IStream->err == EOF ),
		IStream->ud,
		IStream->byte,
		IStream->took,
		IStream->used
	);
}
