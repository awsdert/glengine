#ifndef INC_UNIT_COMMON_H
#define INC_UNIT_COMMON_H

#ifdef _WIN323
#include <windwos.h>
#else
#include <unistd.h>
#endif

#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <assert.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

typedef signed char dchar;
typedef signed short int dshort;
typedef signed int dint;
typedef signed long int dlong;
typedef signed long long int dllong;
typedef unsigned char uchar;
typedef unsigned short int ushort;
typedef unsigned int uint;
typedef unsigned long int ulong;
typedef unsigned long long int ullong;

#ifndef U8_MAX
#ifdef UINT8_MAX
typedef uint8_t u8;
#define U8_MAX UINT8_MAX
#else
typedef uint_least8_t u8;
#define U8_MAX UINT_LEAST8_MAX
#endif
#endif

#ifndef S8_MAX
#ifdef INT8_MAX
typedef int8_t s8;
#define S8_MAX INT8_MAX
#define S8_MIN INT8_MIN
#else
typedef int_least8_t s8;
#define S8_MAX INT_LEAST8_MAX
#define S8_MIN INT_LEAST8_MIN
#endif
#endif

#ifndef U16_MAX
#ifdef UINT16_MAX
typedef uint16_t u16;
#define U16_MAX UINT16_MAX
#else
typedef uint_least16_t u16;
#define U16_MAX UINT_LEAST16_MAX
#endif
#endif

#ifndef S16_MAX
#ifdef INT16_MAX
typedef int16_t s16;
#define S16_MAX INT16_MAX
#define S16_MIN INT16_MIN
#else
typedef int_least16_t s16;
#define S16_MAX INT_LEAST16_MAX
#define S16_MIN INT_LEAST16_MIN
#endif
#endif

#ifndef U32_MAX
#ifdef UINT32_MAX
typedef uint32_t u32;
#define U32_MAX UINT32_MAX
#else
typedef uint_least32_t u32;
#define U32_MAX UINT_LEAST32_MAX
#endif
#endif

#ifndef S32_MAX
#ifdef INT32_MAX
typedef int32_t s32;
#define S32_MAX INT32_MAX
#define S32_MIN INT32_MIN
#elif defined( INT_LEAST32_MAX )
typedef int_least32_t s32;
#define S32_MAX INT_LEAST32_MAX
#define S32_MIN INT_LEAST32_MIN
#endif
#endif

#ifndef U64_MAX
#ifdef UINT64_MAX
typedef uint64_t u64;
#define U64_MAX UINT64_MAX
#elif defined( UINT_LEAST64_MAX )
typedef uint_least64_t u64;
#define U64_MAX UINT_LEAST64_MAX
#endif
#endif

#ifndef S64_MAX
#ifdef INT64_MAX
typedef int64_t s64;
#define S64_MAX INT64_MAX
#define S64_MIN INT64_MIN
#elif defined( INT_LEAST64_MAX )
typedef int_least64_t s64;
#define S64_MAX INT_LEAST64_MAX
#define S64_MIN INT_LEAST64_MIN
#endif
#endif

#ifndef bitsof
#define bitsof( T ) ( sizeof(T) * CHAR_BIT )
#endif

#ifndef intervalof
#define intervalof( I, V ) ((V) + ( (I) - ( (V) % (I) ) ))
#endif

#define BOOL2STR( VAL ) ((VAL) ? "true" : "false")

#endif
