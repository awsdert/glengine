#include "zlib.h"
#include <stdlib.h>

uint zlib_lod = 0;

#define ZLIB_COPY_LIT0 257
#define ZLIB_LOOP_LIT0 288

ZLIB_IMPLIED implied_type_data =
{
	/* get extra bits */
	{
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		2, 3, 7, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0
	},
	/* base cpy value */
	{
#if 1
		1, 1,  1, 1, 1, 1, 1, 1,
        1, 1,  1, 1, 1, 1, 1, 1,
#else
		0, 0,  0, 0, 0, 0, 0, 0,
		0, 0,  0, 0, 0, 0, 0, 0,
#endif
		3, 3, 11, 0, 0, 0, 0, 0,
		0, 0,  0, 0, 0, 0, 0, 0
	}
};

ZLIB_IMPLIED implied_code_data =
{
	/* get extra bits */
	{
		0, 0, 0, 0, 0, 0, 0, 0,
		1, 1, 1, 1, 2, 2, 2, 2,
		3, 3, 3, 3,	4, 4, 4, 4,
		5, 5, 5, 5, 0, 0, 0, 0
	},
	/* base cpy value */
	{
		  3,   4,   5,   6,   7,   8,   9,  10,
		 11,  13,  15,  17,  19,  23,  27,  31,
		 35,  43,  51,  59,  67,  83,  99, 115,
		131, 163, 195, 227, 258,   0,   0,   0,
	}
};

ZLIB_IMPLIED implied_loop_data =
{
	/* get extra bits */
	{
		 0,  0,  0,  0,  1,  1,  2,  2,
		 3,  3,  4,  4,  5,  5,  6,  6,
		 7,  7,  8,  8,  9,  9, 10, 10,
		11, 11, 12, 12, 13, 13,	 0,  0
	},
	/* base cpy value */
	{
			 1,    2,    3,     4,     5,     7,    9,    13,
			17,   25,   33,    49,    65,    97,  129,   193,
		   257,  385,  513,   769,  1025,  1537, 2049,  3073,
		  4097, 6145, 8193, 12289, 16385, 24577,    0,     0
	}
};

typedef struct _ZLIB_FULL_SYMBOL
{
	bool use;
	long src, lit;
	long get, cpy;
	long len, uid;
	long nxt[2];
	char *grp;
} ZLIB_FULL_SYMBOL;

void ZlibFullSymbol( ZLIB *zlib, ZLIB_FULL_SYMBOL *full, ZLIB_SYMBOL *part )
{
	int get = 0, cpy = 0, i = part->lit;
	char *grp = NULL;

	if ( part >= zlib->Symbols[ZLIB_SYMBOLS_ID_LOOP].symbols )
	{
		i -= ZLIB_LOOP_LIT0;
		get = implied_loop_data.get[i];
		cpy = implied_loop_data.cpy[i];
		grp = "loop";
	}
	else if ( part >= zlib->Symbols[ZLIB_SYMBOLS_ID_CODE].symbols )
	{
		i -= ZLIB_COPY_LIT0;
		get = implied_code_data.get[i];
		cpy = implied_code_data.cpy[i];
		grp = "code";
	}
	else
	{
		get = implied_type_data.get[i];
		cpy = implied_type_data.cpy[i];
		grp = "loop";
	}

	memset( full, 0, sizeof(ZLIB_FULL_SYMBOL) );

	full->use = part->use;
	full->src = part->src;
	full->lit = part->lit;
	full->len = part->len;
	full->uid = part->uid;
	full->get = get;
	full->cpy = cpy;
	full->grp = grp;
}

int SetupZlibObject( ZLIB *zlib, BLOCKS *Blocks )
{
	int id;
	ZLIB_SYMBOL *symbols = zlib->symbols;
	ZLIB_SYMBOLS *Symbols;

	if ( !Blocks )
	{
		ECHO
		(
			stderr,
			ECHO( stderr, EINVAL );
			fputs( "'Blocks' must not be NULL!\n", stderr )
		);
		return EINVAL;
	}

	zlib->Blocks = Blocks;

	for ( id = 0; id < ZLIB_SYMBOLS_ID_COUNT; ++id )
	{
		Symbols = &(zlib->Symbols[id]);

		memset( Symbols, 0, sizeof(ZLIB_SYMBOLS) );

		Symbols->id = id;
		Symbols->Blocks = Blocks;
	}

	Symbols = &(zlib->Symbols[ZLIB_SYMBOLS_ID_TYPE]);
	Symbols->have = ZLIB_MAX_TYPES;
	Symbols->symbols = symbols;

	Symbols = &(zlib->Symbols[ZLIB_SYMBOLS_ID_CODE]);
	Symbols->have = ZLIB_MAX_CODES;
	Symbols->symbols = symbols + ZLIB_MAX_TYPES;

	Symbols = &(zlib->Symbols[ZLIB_SYMBOLS_ID_LOOP]);
	Symbols->have = ZLIB_MAX_LOOPS;
	Symbols->symbols =
		zlib->Symbols[ZLIB_SYMBOLS_ID_CODE].symbols + ZLIB_MAX_CODES;

	return 0;
}

int CompareZlibSymbols( void const *A, void const *B )
{
	ZLIB_SYMBOL const *a = A, *b = B;

	if ( a->use != b->use )
		return b->use - a->use;

	if ( a->uid != b->uid )
		return a->uid - b->uid;

	if ( a->len != b->len )
		return a->len - b->len;

	if ( a->lit != b->lit )
		return a->lit - b->lit;

	return a->src - b->src;
}

void InitZlibSymbolUIDs( ZLIB_SYMBOLS *Symbols )
{
	int i;
	uint uid = 0, len;
	ZLIB_SYMBOL *symbols = Symbols->symbols, *symbol;

	for ( len = 1; len <= Symbols->longest; ++len )
	{
		for ( i = 0; i < Symbols->used; ++i )
		{
			symbol = symbols + i;

			if ( symbol->len != len )
				continue;

			Symbols->highest = uid;
			symbol->uid = uid++;
		}

		uid <<= 1;
	}
}

ZLIB_SYMBOL * SeekZlibSymbol( ISTREAM *IStream, ZLIB_SYMBOLS *Symbols )
{
	BLOCKS *Blocks = Symbols->Blocks;
	ZLIB_SYMBOL *symbols = Symbols->symbols;
	FILE *out = Blocks->Alloc->out;
	uint len, uid = 0;

	for ( len = 1; len <= Symbols->longest; ++len )
	{
		ZLIB_SYMBOL *symbol;

		uid = IStreamBits( IStream, len, false );
		FlipBits( &uid, len );
		symbol = symbols + uid;

		if ( (int)uid >= Symbols->used )
		{
			ECHO
			(
				out,
				ECHO_ERR( out, ERANGE );
				fprintf( out, "Unable to identify symbol, last UID sought: " );
				EchoBits( out, &uid, 0, len, len, ' ' );
				fprintf( out, " , permitted upto: " );
				EchoBits( out, &(Symbols->used), 0, Symbols->longest + 1, Symbols->longest + 1, ' ' );
				putchar('\n')
			);
			return NULL;
		}

		if ( !(symbol->use) || symbol->len != len )
			continue;

		if ( symbol->uid == uid )
		{
			if ( zlib_lod > 2 )
			{
				ECHO
				(
					out,
					fprintf( out, "uid = " );
					EchoBits( out, &uid, 0, Symbols->longest, Symbols->longest, ' ' );
					fputc( '\n', out )
				);
			}
			IStreamPull( IStream, symbol->len );
			return symbol;
		}
	}

	ECHO
	(
		out,
		ECHO_ERR( out, ERANGE );
		fprintf( out, "Unable to identify symbol, last UID sought: " );
		EchoBits( out, &uid, 0, len, len, ' ' );
		fprintf( out, " , permitted upto: " );
		EchoBits( out, &(Symbols->used), 0, len + 1, len + 1, ' ' );
		putchar('\n')
	);

	return NULL;
}

int SortZlibSymbolsByUID( ZLIB *zlib, ZLIB_SYMBOLS_ID id )
{
	uint j = 0;
	ZLIB_SYMBOLS *Symbols = &(zlib->Symbols[id]);
	ZLIB_SYMBOL *symbols = Symbols->symbols, *dst, *src;
	ulong top = ~(~0L << Symbols->longest);

	qsort( symbols, Symbols->used, sizeof(ZLIB_SYMBOL), CompareZlibSymbols );

	for ( j = Symbols->used; j; )
	{
		src = symbols + --j;
		dst = symbols + src->uid;

		if ( src->uid > j )
		{
			*dst = *src;
			memset( src, 0, sizeof(ZLIB_SYMBOL) );
		}
	}

	Symbols->used = top + 1;

	return 0;
}

int LoadZlibTypes( ZLIB *zlib )
{
	ZLIB_SYMBOLS_ID id = ZLIB_SYMBOLS_ID_TYPE;
	ISTREAM *IStream = zlib->IStream;
	ZLIB_SYMBOLS *Types = &(zlib->Symbols[id]);
	int i = Types->foresee;
	ZLIB_SYMBOL *types = Types->symbols;

	for ( i = 0; i < Types->foresee && IStream->err == 0; ++i )
	{
		static uint adjust[] =
		{
			16, 17, 18, 0,  8, 7,
			 9,  6, 10, 5, 11, 4,
			12,  3, 13, 2, 14, 1,
			15, 19
		};

		uint pos = (i < 20) ? adjust[i] : 31;

		ZLIB_SYMBOL *Type = types + pos;

		Type->src = i;
		Type->lit = pos;
		Type->len = IStreamBits( IStream, 3, true );
		Type->use = !!(Type->len);

		if ( Type->len > Types->longest )
			Types->longest = Type->len;
	}

	Types->used = 19;
	InitZlibSymbolUIDs( Types );
	SortZlibSymbolsByUID( zlib, id );

	return 0;
}

int LoadZlibHuffs( ZLIB *zlib )
{
	ZLIB_SYMBOLS *Types = &(zlib->Symbols[ZLIB_SYMBOLS_ID_TYPE]);
	ZLIB_SYMBOLS *Codes = &(zlib->Symbols[ZLIB_SYMBOLS_ID_CODE]);
	ZLIB_SYMBOLS *Loops = &(zlib->Symbols[ZLIB_SYMBOLS_ID_LOOP]);
	ZLIB_SYMBOL *codes = Codes->symbols, *Code;
	ZLIB_FULL_SYMBOL fullType = {0}, Temp;
	ISTREAM *IStream = zlib->IStream;
	int done = 0, pos = ZLIB_MAX_TYPES, lit = 0, prv = 0,
		keep = Codes->foresee,
		total = keep + Loops->foresee, stop = keep, lose = 0;

	while ( done < total )
	{
		while ( done < stop && IStream->err == 0 )
		{
			int i = 0;
			ZLIB_SYMBOL *Type = SeekZlibSymbol( IStream, Types );

			memset( &Temp, 0, sizeof(ZLIB_FULL_SYMBOL) );

			ZlibFullSymbol( zlib, &fullType, Type );
			Temp.cpy = fullType.cpy;

			if ( Type->lit < 16 )
				Temp.len = Type->lit;
			else
			{
				Temp.cpy += IStreamBits( IStream, fullType.get, true );
				Temp.len = (Type->lit == 16) ? prv : codes->len;

				if ( !Temp.len )
				{
					lit += Temp.cpy;
					pos += Temp.cpy;
					done += Temp.cpy;
					Temp.cpy = 0;
				}
			}

			Temp.use = !!(Temp.len);
			prv = Temp.len;

			if ( Temp.len > Codes->longest )
				Codes->longest = Temp.len;

			for ( i = 0; i < Temp.cpy; ++i, ++lit, ++pos, ++done )
			{
				Code = codes + pos;

				Code->use = Temp.use;
				Code->len = Temp.len;
				Code->lit = lit;
				Code->src = done - lose;
			}
		}

		lit = ZLIB_LOOP_LIT0;
		pos = ZLIB_MAX_TYPES + ZLIB_MAX_CODES;
		lose = keep;
		stop = total;
	}

	Codes->used = ZLIB_MAX_CODES;
	Loops->used = ZLIB_MAX_LOOPS;
	Loops->longest = Codes->longest;

	InitZlibSymbolUIDs( Codes );
	InitZlibSymbolUIDs( Loops );

	SortZlibSymbolsByUID( zlib, ZLIB_SYMBOLS_ID_CODE );
	SortZlibSymbolsByUID( zlib, ZLIB_SYMBOLS_ID_LOOP );

	return 0;
}

void EchoZlibSymbolDetails( FILE *dst, ZLIB *zlib, ZLIB_SYMBOL *symbol )
{
	ZLIB_FULL_SYMBOL full = {0};
	ZLIB_SYMBOLS *Symbols = zlib->Symbols;
	ZLIB_SYMBOL *symbols = zlib->symbols;
	ulong pos;

	for ( pos = ZLIB_SYMBOLS_ID_LOOP; pos; --pos )
	{
		if ( Symbols[pos].symbols <= symbol )
			break;
	}

	symbols = Symbols[pos].symbols;
	pos = symbol - symbols;

	ZlibFullSymbol( zlib, &full, symbol );

	fprintf
	(
		dst,
		"%s_symbols[%10lu]: "
		"src = %3ld, lit = %3ld, "
		"use = %5s, get = %2ld, cpy = %5ld, "
		"len = %2ld, uid = ",
		full.grp, pos,
		full.src, full.lit,
		BOOL2STR( full.use ),
		full.get, full.cpy, full.len
	);
	EchoBits
	(
		dst,
		&(symbol->uid),
		0, symbol->len,
		Symbols[ZLIB_SYMBOLS_ID_CODE].longest, ' '
	);
	fputc('\n', dst);
}

void EchoZlibSymbolsListDetails( FILE *dst, ZLIB *zlib, int id )
{
	ZLIB_SYMBOLS *Symbols = &(zlib->Symbols[id]);
	char *names[ZLIB_SYMBOLS_ID_COUNT] = {NULL};
	names[ZLIB_SYMBOLS_ID_TYPE] = "Type";
	names[ZLIB_SYMBOLS_ID_CODE] = "Code";
	names[ZLIB_SYMBOLS_ID_LOOP] = "Loop";

	fprintf
	(
		dst,
		"%s Symbols List Details: "
		"foresee: %3u, longest = %2u, highest = %2u, have = %7u, used = %7u\n",
		names[id], Symbols->foresee, Symbols->longest, Symbols->highest,
		Symbols->have, Symbols->used
	);

	if ( 1 )
	{
		int i;
		ZLIB_SYMBOL *symbols = Symbols->symbols;

		for ( i = 0; i < Symbols->used; ++i )
		{
			ZLIB_SYMBOL *symbol = symbols + i;

			if ( symbol->use )
				EchoZlibSymbolDetails( dst, zlib, symbol );
		}
	}

	fflush( dst );
}

int ExpandZlibIStreamType0( ZLIB *zlib )
{
	int err;
	BLOCKS *Blocks = zlib->Blocks;
	BLOCK *Into = GrabBlock( Blocks, zlib->IntoID );
	ISTREAM *IStream = zlib->IStream;
	uch *into = Into->addr;
	uint i, leng, nlen, left = IStream->used % 8;
	FILE *out  = zlib->Blocks->Alloc->out;

	if ( left )
		IStreamBits( IStream, 8 - left, true );

	leng = IStreamBits( IStream, 16, true );
	nlen = IStreamBits( IStream, 16, true );

	if ( zlib_lod )
		ECHO( out, fprintf( out, "leng = %u, nlen = %u\n", leng, nlen ) );

	err = GrowBlock( Into, sizeof(uch), leng );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	into = Into->addr;

	for ( i = 0; i < leng && IStream->err == 0; ++i )
	{
		uchar c = (uchar)IStreamBits( IStream, 8, true );

		if ( zlib_lod > 2 )
			fprintf( out, " %02X%s", c, (i % 32) ? "" : "\n" );

		if ( Into->used == Into->have )
		{
			err = GrowBlock
			(
				Into,
				sizeof(uch),
				Into->used + BUFSIZ
			);

			if ( err )
			{
				ECHO( out, ECHO_ERR( out, err ) );
				return err;
			}

			into = Into->addr;
		}

		into[Into->used++] = c;
	}

	if ( zlib_lod > 2 )
		fputc( '\n', out );

	return 0;
}

int ExpandZlibIStreamType1( ZLIB *zlib )
{
	BLOCKS *Blocks = zlib->Blocks;
	BLOCK *Into = GrabBlock( Blocks, zlib->IntoID );
	ISTREAM *IStream = zlib->IStream;
	uch *into = Into->addr;
	uint lookup = 0, search = 0, i = Into->used;
	FILE *out = zlib->Blocks->Alloc->out;
	int err;

	while ( IStream->err == 0 )
	{
		uint lit = IStreamBits( IStream, 8, false );
		FlipBits( &lit, 8 );

		if ( lit >= 48 )
		{
			lit -= 48;
			IStreamPull( IStream, 8 );

			err = GrowBlock( Into, sizeof(uch), i + 1 );

			if ( err )
				return err;

			into = Into->addr;
			into[i++] = (uchar)lit;
			lookup = i;

			if ( zlib_lod > 1 )
				ECHO( out, fprintf( out, "lit = %02X\n", lit ) );
		}
		else
		{
			uint leng = 0, li = 0, l = 0;
			uint loop = 0, di = 0;
			uint bits = 0;

			li = IStreamBits( IStream, 7, true );
			FlipBits( &li, 7 );
			li--;
			lit = li + 257;

			if ( lit == 256 )
			{
				if ( zlib_lod > 1 )
				{
					ECHO
					(
						out,
						fprintf
						(
							out,
							"lit = %3u, li = %2u, breaking...\n",
							lit, li
						)
					);
				}
				break;
			}

			bits = implied_code_data.get[li];
			leng = IStreamBits( IStream, bits, true );
			FlipBits( &leng, bits );
			leng += implied_code_data.cpy[li];

			if ( zlib_lod > 1 )
			{
				ECHO
				(
					out,
					fprintf
					(
						out,
						"lit = %u, "
						"li = %2u, leng = %3u, "
						"di = %2u, loop = %3u\n",
						lit, li, leng, di, loop
					);
					EchoIStreamDetails( out, IStream )
				);
			}

			di = IStreamBits( IStream, 5, true );
			FlipBits( &di, 5 );

			bits = implied_loop_data.get[di];
			loop = IStreamBits( IStream, bits, true );
			FlipBits( &loop, bits );
			loop += implied_loop_data.cpy[di];

			if ( loop > lookup )
			{
				ECHO( out, ECHO_ERR( out, EINVAL ) );
				return EINVAL;
			}

			search = lookup - loop;

			if ( zlib_lod > 1 )
			{
				ECHO
				(
					out,
					fprintf
					(
						out,
						"lit = %u, "
						"li = %2u, leng = %3u, "
						"di = %2u, loop = %3u\n",
						lit, li, leng, di, loop
					);
					EchoIStreamDetails( out, IStream )
				);
			}

			err = GrowBlock( Into, sizeof(uch), i + leng + 1 );

			if ( err )
				return err;

			into = Into->addr;

			for ( l = 0; l < leng; ++l, ++search )
			{
				lit = (uchar)(into[search]);
				into[i++] = lit;

				if ( zlib_lod > 1 )
				{
					ECHO
					(
						out,
						fprintf( out, "lit = %u, l = %3u\n", lit, l )
					);
				}
			}

			lookup = i;
		}
	}

	Into->used = i;

	return IStream->err;
}

int ExpandZlibIStreamType2( ZLIB *zlib )
{
	ISTREAM *IStream = zlib->IStream;
	BLOCKS *Blocks = zlib->Blocks;
	BLOCK *Into = GrabBlock( Blocks, zlib->IntoID );
	uch *into = Into->addr;
	int i = 0, j = 0, err;
	ZLIB_SYMBOLS *Types = &(zlib->Symbols[ZLIB_SYMBOLS_ID_TYPE]);
	ZLIB_SYMBOLS *Codes = &(zlib->Symbols[ZLIB_SYMBOLS_ID_CODE]);
	ZLIB_SYMBOLS *Loops = &(zlib->Symbols[ZLIB_SYMBOLS_ID_LOOP]);
	ZLIB_SYMBOL *Code, *Loop;
	ZLIB_FULL_SYMBOL fullCode = {0}, fullLoop = {0};
	FILE *out = Blocks->Alloc->out;

	Codes->foresee = IStreamBits( IStream, 5, true ) + 257;
	Loops->foresee = IStreamBits( IStream, 5, true ) + 1;
	Types->foresee = IStreamBits( IStream, 4, true ) + 4;
	Types->used = 0;
	Codes->used = 0;
	Loops->used = 0;

	memset( zlib->symbols, 0, ZLIB_TOTAL_SYMBOLS * sizeof(ZLIB_SYMBOL) );

	err = LoadZlibTypes( zlib );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	if ( zlib_lod > 1 )
	{
		ECHO
		(
			out,
			EchoZlibSymbolsListDetails( out, zlib, ZLIB_SYMBOLS_ID_TYPE )
		);
	}

	Codes->used = 0;
	Loops->used = 0;

	err = LoadZlibHuffs( zlib );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	if ( zlib_lod )
	{
		ECHO
		(
			out,
			EchoIStreamDetails( out, IStream );
			EchoZlibSymbolsListDetails( out, zlib, ZLIB_SYMBOLS_ID_CODE );
			EchoZlibSymbolsListDetails( out, zlib, ZLIB_SYMBOLS_ID_LOOP )
		);
	}

	i = Into->used; j = 0;
	while ( IStream->err == 0 )
	{
		Code = SeekZlibSymbol( IStream, Codes );

		if ( Code->lit == 256 )
			break;
		else if ( Code->lit < 256 )
		{
			Into->used = i;
			err = GrowBlock( Into, sizeof(uch), i + 1 );

			if ( err )
				return err;

			into = Into->addr;
			into[i++] = Code->lit;
		}
		else if ( Code->lit < 286 )
		{
			int qry, cpy, get;

			ZlibFullSymbol( zlib, &fullCode, Code );
			get = fullCode.get;
			cpy = fullCode.cpy;

			if ( get > 0 )
				cpy += IStreamBits( IStream, get, true );

			Loop = SeekZlibSymbol( IStream, Loops );

			ZlibFullSymbol( zlib, &fullLoop, Loop );
			get = fullLoop.get;
			qry = fullLoop.cpy;

			if ( get > 0 )
				qry += IStreamBits( IStream, get, true );

			qry = i - qry;

			Into->used = i;
			err = GrowBlock( Into, sizeof(uch), i + cpy + 1 );

			if ( err )
			{
				ECHO( out, ECHO_ERR( out, err ) );
				return err;
			}

			into = Into->addr;

			if ( qry > Into->have )
				qry = 0;

			for ( j = 0; j < cpy; ++j, ++i, ++qry )
				into[i] = into[qry];

			j = i - 1;
		}
		else
		{
			ECHO( out, ECHO_ERR( out, EINVAL ) );
			return EINVAL;
		}
	}

	Into->used = i;

	return IStream->err;
}

int ExpandZlibIStreamType3( ZLIB *zlib ) { (void)zlib; return EINVAL; }

ExpandZlibIStreamTypeCB ExpandZlibIStreamType[4] =
{
	ExpandZlibIStreamType0,
	ExpandZlibIStreamType1,
	ExpandZlibIStreamType2,
	ExpandZlibIStreamType3
};

int ExpandZlibIStream( ZLIB *zlib, ISTREAM *IStream, int IntoID )
{
	bool last = false;
	FILE *out = zlib->Blocks->Alloc->out;
	zlib->IntoID = IntoID;
	zlib->IStream = IStream;

	while ( !last && IStream->err != EOF )
	{
		int err = 0;
		uint type;

		last = IStreamBits( IStream, 1, true );
		type = IStreamBits( IStream, 2, true );

		if ( zlib_lod )
		{
			ECHO
			(
				out,
				fprintf
				(
					out,
					"last = %u, type = %u%u, IStream->used = %" PRIdMAX "\n",
					last, type >> 1, type & 1, IStream->used
				)
			);
		}

		err = ExpandZlibIStreamType[type]( zlib );

		if ( err && err != EOF )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	return 0;
}

/* From Wikipedia */
const uint32_t MOD_ADLER = 65521;

ulong adler32_slow( uch *buff, size_t size)
{
	ulong a = 1, b = 0;
	size_t i;

	/* GProcess each byte of the data in order */
	for ( i = 0; i < size; ++i )
	{
		a = (a + buff[i]) % MOD_ADLER;
		b = (b + a) % MOD_ADLER;
	}

	return (b << 16) | a;
}

/* Modified form of what's provided here:
 * https://github.com/libyal/assorted/blob/main/documentation/Deflate%20(zlib)%20compressed%20data%20format.asciidoc
 */
ulong adler32_fetch( uch *buff, size_t size, ulong prior_key )
{
	size_t i, j;
	uint32_t lower_word    = prior_key & 0xffff;
	uint32_t upper_word    = (prior_key >> 16) & 0xffff;

	lower_word += buff[0];
	upper_word += lower_word;
	--size;

	for( i = j = 1; i < size; ++i, ++j )
	{
		lower_word += buff[i];
		upper_word += lower_word;

		if ( j == 0x15b0 )
		{
			lower_word %= 0xfff1;
			upper_word %= 0xfff1;
			j = 0;
		}
	}

	lower_word += buff[i];
	upper_word += lower_word;

	lower_word %= 0xfff1;
	upper_word %= 0xfff1;

	return( ( upper_word << 16 ) | lower_word );
}

int ExpandZlibArchive( ZLIB *zlib, int DataID, int IntoID )
{
	BLOCKS *Blocks = zlib->Blocks;
	BLOCK *Into = GrabBlock( Blocks, IntoID );
	uch *into;
	long used = Into->used;
	int err;
	intmax_t expect, actual;
	uint method, xflags, check;
	ISTREAM IStream = {0};
	ISTREAM_BLOCK_ID IStreamID = {0};
	FILE *out = Blocks->Alloc->out;

	IStreamID.Blocks = Blocks;
	IStreamID.id = DataID;
	IStream.ud = &IStreamID;
	IStream.DataCB = IStreamDataFromBufferIndex;

	method = IStreamBits( &IStream, 8, true );
	xflags = IStreamBits( &IStream, 8, true );
	check = ((xflags >> 3) & 0x1F);

	zlib->IntoID = IntoID;
	zlib->method = method;
	zlib->xflags = xflags;
	zlib->IStream = &IStream;

	if ( zlib_lod )
	{
		ECHO
		(
			out,
			fprintf
			(
				out,
				"Zlib: method = %X, info = %X, "
				"type = %u, dictionary = %s, check = %X\n",
				(method & 0xF), ((method >> 4) & 0xF),
				(xflags & 3), BOOL2STR(xflags & 1), check
			)
		);
	}

	err = ExpandZlibIStream( zlib, &IStream, IntoID );

	if ( err )
		ECHO( out, ECHO_ERR( out, err ) );

	into = Into->addr;
	expect = IStreamBits( &IStream, 16, true );
	large_endian_to_local_endian( &expect, 2 );
	expect += ((check & 0xF) << 8);
	expect -= ((check & 0x10) << 8);
	actual = adler32_fetch( into + used, Into->used - used, 1 );

	if ( zlib_lod )
	{
		ECHO
		(
			out,
			fprintf
			(
				out,
				"Zlib: method = %X, info = %X, "
				"type = %u, dictionary = %s, check = %02X, "
				"expected = %08lX, actual = %08lX\n",
				(method & 0xF), ((method >> 4) & 0xF),
				(xflags & 3), BOOL2STR(xflags & 1), check,
				expect, actual
			)
		);
	}

	return err;
}
