#ifndef INC_BUFF_ISTREAM_H
#define INC_BUFF_ISTREAM_H

#include "data.h"
#include "memory.h"

typedef struct _ISTREAM_BLOCK_ID
{
	int id;
	BLOCKS *Blocks;
} ISTREAM_BLOCK_ID;

/* In ISTREAM objects 'mem' refers to where to copy to, In OSTREAM objects
 * 'mem' refers to where to copy from */
typedef long (*StreamDataCB)( void *mem, void *ud, long pos, long num );
typedef long (*StreamPrepCB)( void *ud, size_t size );

long IStreamDataFromBufferObject( void *dst, void *ud, long pos, long num );
long IStreamDataFromBufferIndex( void *dst, void *ud, long pos, long num );

typedef struct _ISTREAM
{
	bool		eod;
	int			err;
	long		byte;
	void *		ud;
	StreamDataCB	DataCB;
	intmax_t	used;
	bool		back;
	long		temp;
	ulong		took;
	ulong		have;
	uchar		feed[BUFSIZ*2];
} ISTREAM;

long OStreamDataFromBufferObject( void *dst, void *ud, long pos, long num );
long OStreamDataFromBufferIndex( void *dst, void *ud, long pos, long num );
int OStreamPrepFromBufferObject( void *ud, size_t size );
int OStreamPrepFromBufferIndex( void *ud, size_t size );

typedef struct _OSTREAM
{
	int			err;
	long		byte;
	void *		ud;
	StreamDataCB	DataCB;
	StreamPrepCB	PrepCB;
	intmax_t	gave;
	ulong		give;
	ulong		have;
	uchar		feed[BUFSIZ];
} OSTREAM;

void	  	IStreamPull( ISTREAM *IStream, uint add );
int			IStreamData( ISTREAM *IStream, void *dst, intmax_t get, bool pull );
uintmax_t	IStreamBits( ISTREAM *IStream, uint get, bool pull );
void		EchoIStreamDetails( FILE *file, ISTREAM *IStream );

void		InitDataIStream( ISTREAM *IStream, void *data, uint bit );
void		InitFileIStream( ISTREAM *IStream, FILE *file );

int OStreamPrep( OSTREAM *OStream, size_t size );
int	OStreamPush( OSTREAM *OStream );
int	OStreamData( OSTREAM *OStream, void const *mem, intmax_t num, bool push );
int	OStreamBits( OSTREAM *OStream, uintmax_t val, uint num, bool push );

#endif
