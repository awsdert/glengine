#include "data.h"

int GrowSeries( struct _SERIES *Series, size_t size, int want )
{
	int err;

	Series->Block.Alloc = Series->Alloc;
	Series->Using.Block.Alloc = Series->Alloc;
	err = GrowBinary( &(Series->Using), want );

	if ( err )
		return err;

	return GrowBlock( &(Series->Block), size, want );
}

int		CropSeries( struct _SERIES *Series, size_t size, int want )
{
	if ( Series )
	{
		BINARY *Using = &(Series->Using);
		BLOCK *Block = &(Series->Block);

		if ( want > Block->have )
			return EINVAL;

		if ( want == Block->have )
			return 0;

		if ( want < Block->used )
			UsableEntries( Series, want );

		CropBinary( Using, want );
		CropBlock( Block, size, want );

		return 0;
	}

	return EINVAL;
}

int UsableEntry( SERIES *Series, size_t size, int *I, int add )
{
	BLOCK *Entries = &(Series->Block);
	int i, num = Entries->used;
	uchar *active = Series->Using.Block.addr;
	BIT pos;

	i = ( *I < 1 ) ? 1 : *I + 1;
	GrabBit( &pos, active, i );

	for  ( ; i < num; ++i, IncBit( &pos, active ) )
	{
		if ( *(pos.data) & pos.mask )
			continue;

		*I = i;
		return 0;
	}

	*I = -1;

	if ( add > 0 )
	{
		if ( Entries->have <= num )
		{
			int err = GrowSeries( Series, size, num + add );

			if ( err )
				return err;
		}

		*I = i;

		return 0;
	}

	return ERANGE;
}

int WantEntry( SERIES *Series, size_t size, int *I, int add )
{
	int i = 0, err;
	BIT pos;

	*I = -1;

	err = UsableEntry( Series, size, &i, add );

	if ( err )
	{
		FILE *out = Series->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	UsableEntries( Series, i + 1 );

	*I = i;
	GrabBit( &pos, Series->Using.Block.addr, i );
	*((uchar*)(pos.data)) |= pos.mask;

	return 0;
}

int UsableEntries( struct _SERIES *Series, int count )
{
	BIT *Used;

	if ( Series->Block.have < count )
		return EINVAL;

	Used = &(Series->Using.Used);
	GrabBit( Used, Series->Block.addr, count );
	Series->Using.Block.used = Used->pos + (Used->mask > 1);
	Series->Block.used = count;

	return 0;
}

void * GrabEntry( struct _SERIES *Series, size_t size, int i )
{
	if ( Series )
		return GrabChunk( &(Series->Block), size, i );

	return NULL;
}

void	FreeEntry( struct _SERIES *Series, int i )
{
	if ( Series )
	{
		BIT pos;
		BINARY *Using = &(Series->Using);

		if ( i < 1 || Using->Used.abs <= i )
			return;

		GrabBit( &pos, Using->Block.addr, i );
		*((uchar*)(pos.data)) &= ~(pos.mask);
	}
}
