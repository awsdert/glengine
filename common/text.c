#include "text.h"
#include <stdlib.h>

void FillTokenType( CTOKEN *dst, CHTYPE type )
{
#ifdef _WIN32
	dst->type = type;
#else
	strncpy( dst->type, type, CHTYPE_BUFSIZ );
#endif
}

size_t	HiddenText2Text( CHCONV *ctx )
{
	int err = 0;
	CTOKEN *D = &(ctx->dst), *S = &(ctx->src);
	size_t dnum = D->left, snum = S->left;

	if ( S->wide && D->wide )
	{
		if ( dnum > snum )
		{
			memmove( D->data, S->data, snum );
			((wch*)D->data)[snum] = 0;
			return snum;
		}
		else
		{
			memmove( D->data, S->data, dnum );
			((wch*)D->data)[dnum-1] = 0;
			return dnum - 1;
		}
	}

#ifdef _WIN32

	if ( !(S->wide || D->wide) )
	{
		if ( S->type != D->type )
			return ENOSYS;

		if ( snum > dnum )
		{
			ctx->err = E2BIG;
			return 0;
		}

		memcpy( D->data, S->data, snum );
		D->left -= snum;
		return snum;
	}

	if ( S->wide )
	{
		int failed;
		BOOL didSwap = FALSE;
		WCHAR *	stok = ctx->src.data;
		CHAR *	dtok = ctx->dst.data;

		ctx->src.left /= sizeof(wch);

		/* We clear this to prevent left over errors being picked up */
		SetLastErorr(0);
		WideCharToMultiByteChar
		(
			ctx->target,
			ctx->mode,
			stok, S->left, dtok, D->left,
			ctx->sub, &didSwap
		);
		/* We capture this immeadiatly to minimise the chance of a data race */
		err = GetLastError();

		ctx->did = (didSwap == TRUE);
		ctx->src.left *= sizeof(wch);

		switch ( err )
		{
		case ERROR_INSUFFICIENT_BUFFER:		err = E2BIG;	break;
		case ERROR_INVALID_FLAGS:
		case ERROR_INVALID_PARAMETER:		err = EINVAL;	break;
		case ERROR_NO_UNICODE_TRANSLATION:	err = EILSEQ;	break;
		default: err = 0;
		}
	}

	if ( D->wide )
	{
		int failed;
		BOOL didSwap = FALSE;
		WCHAR *	stok = ctx->src.data;
		CHAR *	dtok = ctx->dst.data;

		ctx->src.left /= sizeof(wch);

		/* We clear this to prevent left over errors being picked up */
		SetLastErorr(0);
		MultiByteToWideChar
		(
			ctx->target,
			ctx->mode,
			stok, S->left, dtok, D->left,
			ctx->sub, &didSwap
		);
		/* We capture this immeadiatly to minimise the chance of a data race */
		err = GetLastError();

		ctx->did = (didSwap == TRUE);
		ctx->src.left *= sizeof(wch);

		switch ( err )
		{
		case ERROR_INSUFFICIENT_BUFFER:		err = E2BIG;	break;
		case ERROR_INVALID_FLAGS:
		case ERROR_INVALID_PARAMETER:		err = EINVAL;	break;
		case ERROR_NO_UNICODE_TRANSLATION:	err = EILSEQ;	break;
		default: err = 0;
		}
	}

	return dnum - (D->left);
#else
	char *dtok = D->data, *stok = S->data;
	size_t did = 0;

	if ( ansicmpi( S->type, D->type ) == 0 )
	{
		if ( snum > dnum )
		{
			ctx->err = E2BIG;
			return 0;
		}

		memcpy( D->data, S->data, snum );
		D->left -= snum;
		return snum;
	}

	do
	{
		/* Reduces mis-interpretations */
		errno = 0;
		did = iconv
		(
			ctx->_iconv,
			&stok, &(ctx->src.left),
			&dtok, &(ctx->dst.left)
		);
		/* Reduces data race collisions */
		err = errno;

		D->data = dtok;
		S->data = stok;

		if ( did == (size_t)-1 )
		{
			if ( ctx->err == 0 )
				ctx->err = err;

			break;
		}
	}
	while ( S->left && D->left && *stok );
#endif

	return dnum - (D->left);
}

size_t	Wide2Text( CHCONV *ctx )
{
	int err = 0;
	CTOKEN *D = &(ctx->dst), *S = &(ctx->src);

	S->wide = true;
	D->wide = false;

	ctx->err = 0;
	ctx->abs = true;

	if ( !(ctx->sub) || !(ctx->sub[0]) )
		ctx->sub = "?";

#ifdef _WIN32
	S->type = 0;

	return HiddenText2Text( ctx );
#else
	memset( S->type, 0, CHTYPE_BUFSIZ );
	strncpy( S->type, "WCHAR_T", CHTYPE_BUFSIZ );

	switch ( ctx->mode )
	{
	case CHMODE_NEAR:
		strncat( D->type, "//TRANSLIT", CHTYPE_BUFSIZ );
		break;
	case CHMODE_SKIP:
		strncat( D->type, "//IGNORE", CHTYPE_BUFSIZ );
	default: break;
	}

	/* Prevents corruption causing long wait times */
	D->type[CHTYPE_BUFSIZ-1] = 0;

	errno = 0;
	ctx->_iconv = iconv_open( D->type, S->type );
	err = errno;

	if ( err )
		ctx->err = err;
	else
	{
		int failed;
		size_t did = HiddenText2Text( ctx );

		errno = 0;
		failed = iconv_close( ctx->_iconv );
		err = errno;

		if ( failed )
		{
			if ( ctx->err == 0 )
				ctx->err = err;
		}

		return did;
	}

	return 0;
#endif
}

size_t	Text2Wide( CHCONV *ctx )
{
	int err = 0;
	CTOKEN *D = &(ctx->dst), *S = &(ctx->src);

	S->wide = false;
	D->wide = true;

	ctx->err = 0;
	ctx->abs = true;
	ctx->sub = "?";
	ctx->mode = 0;

#ifdef _WIN32
	D->type = 0;

	return HiddenText2Text( ctx );
#else
	memset( D->type, 0, CHTYPE_BUFSIZ );
	strncpy( D->type, "WCHAR_T", CHTYPE_BUFSIZ );

	/* Prevents corruption causing long wait times */
	D->type[CHTYPE_BUFSIZ-1] = 0;

	errno = 0;
	ctx->_iconv = iconv_open( D->type, S->type );
	err = errno;

	if ( err )
		ctx->err = err;
	else
	{
		int failed;
		size_t did = HiddenText2Text( ctx );

		errno = 0;
		failed = iconv_close( ctx->_iconv );
		err = errno;

		if ( failed )
		{
			if ( ctx->err == 0 )
				ctx->err = err;
		}

		return did;
	}

	return 0;
#endif
}

size_t	Text2Text( CHCONV *ctx )
{
	int err = 0;
	CTOKEN *D = &(ctx->dst), *S = &(ctx->src);

	S->wide = false;
	D->wide = false;

	ctx->err = 0;
	ctx->abs = true;
	ctx->sub = "?";
	ctx->mode = 0;

#ifdef _WIN32
	return HiddenText2Text( ctx );
#else
	/* Prevents corruption causing long wait times */
	S->type[CHTYPE_BUFSIZ-1] = 0;
	D->type[CHTYPE_BUFSIZ-1] = 0;

	errno = 0;
	ctx->_iconv = iconv_open( D->type, S->type );
	err = errno;

	if ( err )
		ctx->err = err;
	else
	{
		int failed;
		size_t did = HiddenText2Text( ctx );

		errno = 0;
		failed = iconv_close( ctx->_iconv );
		err = errno;

		if ( failed )
		{
			if ( ctx->err == 0 )
				ctx->err = err;
		}

		return did;
	}

	return 0;
#endif
}

dint	 ansicmpi( ach const *a, ach const *b )
{
	dint A, B, C;

	while ( *a && *b )
	{
		A = toupper(*a);
		B = toupper(*b);
		C = A - B;

		if ( C )
			return C;

		++a;
		++b;
	}

	A = *a;
	B = *b;

	return A - B;
}

size_t 	ansinot0( ach const * text )
{
	size_t count = 0;
	while ( *text ) { ++text; ++count; };
	return count;
}

size_t 	utf8not0( uch const * text )
{
	size_t count = 0;
	while ( *text ) { ++text; ++count; };
	return count;
}

size_t 	widenot0( wch const * text )
{
	size_t count = 0;
	while ( *text ) { ++text; ++count; };
	return count;
}

size_t	textnot0( tch const * text )
{
	size_t count = 0;
	while ( *text ) { ++text; ++count; };
	return count;
}

size_t	ansileng( ach const * text )
{
	size_t count = 0;
	while ( *text ) { ++text; ++count; };
	return count;
}

size_t	utf8leng( uch const * text )
{
	size_t count = 0;
	while ( *text ) { count += !(*text & 0x80); ++text; };
	return count;
}

size_t	wideleng( wch const * text )
{
	size_t count = 0;
	while ( *text ) { ++text; ++count; };
	return count;
}

size_t	textleng( tch const * text )
{
	size_t count = 0;
	while ( *text ) { ++text; ++count; };
	return count;
}

uint_least32_t wchtouch( wch c )
{
	uint_least32_t tmp[2] = {0};
	wch wcs[2] = { c, 0 };
	CHCONV ctx = {0};
	CTOKEN *D = &(ctx.dst), *S = &(ctx.src);

	S->left = sizeof(wcs);
	S->data = wcs;

	D->left = sizeof(tmp);
	D->data = tmp;
	FillTokenType( D, CHTYPE_UTF8 );

	ctx.mode = CHMODE_SKIP;

	(void)Wide2Text( &ctx );

	return tmp[0];
}

uint_least32_t wchtoach( wch c )
{
	uint_least32_t tmp[2] = {0};
	wch wcs[2] = { c, 0 };
	CHCONV ctx = {0};
	CTOKEN *D = &(ctx.dst), *S = &(ctx.src);

	S->left = sizeof(wcs);
	S->data = wcs;

	D->left = sizeof(tmp);
	D->data = tmp;
	FillTokenType( D, WhatTextEncoding() );

	ctx.mode = CHMODE_SKIP;

	(void)Wide2Text( &ctx );

	return tmp[0];
}

uint_least32_t tchtouch( tch c )
{
#ifdef _WIN32
	return wchtouch( c );
#else
	return c;
#endif
}

uint_least32_t tchtoach( tch c )
{
#ifdef _WIN32
	return wchtoach( c );
#else
	return c;
#endif
}

dint ansitoansi( ach *into, size_t size, ach const *text )
{
	if ( into && size && text )
	{
		size_t stop = ansileng( text );

		memmove( into, text, ((stop < size) ? stop : size) );
		into[size-1] = 0;
		return 0;
	}

	return EINVAL;
}

dint widetoansi( ach *into, size_t size, wch const *text )
{
	if ( into && size && text )
	{
		CHCONV ctx = {0};
		CTOKEN *D = &(ctx.dst), *S = &(ctx.src);

		S->left = (widenot0(text) + 1) * sizeof(wch);
		S->data = (void*)text;

		D->left = size;
		D->data = into;
		FillTokenType( D, WhatTextEncoding() );

		(void)Wide2Text( &ctx );
		return ctx.err;
	}

	return EINVAL;
}

dint widetowide( wch *into, size_t size, wch const *text )
{
	if ( into && size && text )
	{
		size_t stop = widenot0( text ) + 1;

		memmove( into, text, ((stop < size) ? stop : size) * sizeof(wch) );
		into[size-1] = 0;
		return 0;
	}

	return EINVAL;
}

dint ansi2text( tch *into, size_t size, ach const *text )
{
#ifdef _WIN32
	return ansitowide( into, size, text );
#else
	return ansitoansi( into, size, text );
#endif
}

dint utf8totext( tch *into, size_t size, uch const * text )
{
	if ( into && size && text )
	{
		CHCONV ctx = {0};
		CTOKEN *D = &(ctx.dst), *S = &(ctx.src);

		S->left = (textnot0(text) + 1) * sizeof(uch);
		S->data = (void*)text;
		FillTokenType( S, CHTYPE_UTF8 );

		D->left = size;
		D->data = into;

#ifdef _WIN32
		return Text2Wide( dst, src );
#else
		FillTokenType( D, WhatTextEncoding() );
		(void)Text2Text( &ctx );

		return ctx.err;
#endif
	}

	return EINVAL;
}

dint utf8toutf8( uch *into, size_t size, uch const *text )
{
	if ( into && size && text )
	{
		size_t need = utf8not0( text ) + 1;

		if ( need < size )
		{
			memmove( into, text, need );
			into[need] = 0;
			return 0;
		}

		--size;
		memmove( into, text, size );
		into[size] = 0;
		return 0;
	}

	return EINVAL;
}

dint utf8tobuff( BLOCK *Into, uch const *text )
{
	size_t size = utf8not0( text ) + 1;
	int err;

	Into->used = 0;
	err = GrowBlock( Into, sizeof(uch), size );

	if ( err )
		return err;

	--size;
	memmove( Into->addr, text, size * sizeof(uch) );
	Into->used = size;

	return 0;
}

dint widetotext( tch *into, size_t size, wch const *text )
{
#ifdef _WIN32
	return widetowide( into, size, text );
#else
	return widetoansi( into, size, text );
#endif
}

dint texttotext( tch *into, size_t size, tch const *text )
{
	if ( into && size && text )
	{
		size_t stop = textnot0( text ) + 1;

		memmove( into, text, ((stop < size) ? stop : size) * sizeof(tch) );
		into[size-1] = 0;
		return 0;
	}

	return EINVAL;
}

dint text2buff( BLOCK *Into, tch const * const text )
{
	size_t size = textnot0( text ) + 1;
	int err;

	Into->used = 0;
	err = GrowBlock( Into, sizeof(tch), size );

	if ( err )
		return err;

	--size;
	memcpy( Into->addr, text, size );
	Into->used = size;

	return 0;
}
