#ifndef INC_MAIN_H
#define INC_MAIN_H

#include "engine/core.h"
#include <stddef.h>

extern CORE core;
extern bool test_png;
extern char const *image_file;

dint AppArgErr(     BLOCKS *Blocks, dint err, dint a, dint num, char *argv[] );
dint TryAppArg(     BLOCKS *Blocks, dint a, dint num, char *argv[] );
dint ProcessArgv(   BLOCKS *Blocks, dint argc, char **argv );

void OnResizeCB(	GLFWwindow *Win, int w, int h );
void OnKeyCB(		GLFWwindow *Win, int key, int code, int action, int mods );
void RenderView(	GLFWwindow *Win );

#endif
