# glEngine

Just a project where I learn how to use OpenGL, uploading so that I have
something to show the Job center in regards my programming studies.

Was not until the shortly before the 1st upload that I finally managed to use
index buffers correctly, I plan to focus soley on re-producing the various
things that should be able to do to a triangle before I move onto my
experiments. The first thing will be using different "Models" for the 2
triangles I have started out with on my first upload, this is for the sake
of ensuring I'm able to correctly render multiple models which will be needed
later when I move on to entire scenes.

For anyone struggling with index buffers as I once was, refer to
glfwRefreshCB(), it's contents may change in later uploads (purely because I'm
likely not using it correctly) however as long as you go to the first upload
you'll find a version with everything you need to see contained in
alloc_trigons() & glRefreshCB() - albiet in a mess, not cleaned up unused code,
I also imagine you do not need anything more than the guides for the shaders,
something to note that they don't make clear, input variables like uniforms in
the shaders are treated as constants, I believe the easiest way to think of it
is shared memory that it only has read access to, also treat the shaders as
threads, this gives a clearer image of their relation to the graphics program
and why they only have read access to shared memory, as for automatic variables
like gl_Position, think of them as member variables in a per shader struct, like
you would pass into a thread if you were to multi-thread an CPU side app.
