#include "main.h"

void SetVerbose( BLOCKS *Blocks, char const *path, uint lod )
{
	ALLOC *Alloc = Blocks->Alloc;

	if ( Alloc->lod < lod )
		Alloc->lod = lod;

	if ( path )
	{
		if ( strcmp( path, "2" ) == 0 )
		{
			Alloc->out = stderr;
			return;
		}

		if ( strlen( path ) > 1 )
		{
			Alloc->out = fopen( path, "w" );
			Alloc->out_must_be_shut = true;
			return;
		}
	}

	Alloc->out = errout;
}

dint TryAppArg( BLOCKS *Blocks, int a, int num, char *argv[] )
{
	char *arg = (a >= 0) ? argv[a] : NULL;
	char *val = (arg && num) ? argv[a+num] : NULL;
	FILE *out = Blocks->Alloc->out;

	ECHO
	(
		out,
		fprintf
		(
			out,
			"TryAppArg( %3d, %3d, %p ) arg = '%s', val = '%s'\n",
			a,
			num,
			(void*)argv,
			arg ? arg : argv[0],
			val
		)
	);

	if ( !arg )
		return 0;

	if ( !val )
	{
		val = strchr( arg, '=' );

		if ( val )
			++val;
	}

	if ( strstr( arg, "--test-png" ) == arg )
	{
		test_png = true;
		return 0;
	}

	if ( strstr( arg, "--zlib-lod" ) == arg )
	{
		if ( val )
			sscanf( val, "%u", &zlib_lod );

		if ( !zlib_lod )
			zlib_lod = 1;

		return 0;
	}

	if ( strstr( arg, "--image-lod" ) == arg )
	{
		if ( val )
			sscanf( val, "%u", &image_lod );

		if ( !image_lod )
			image_lod = 1;

		return 0;
	}

	if ( strstr( arg, "--verbose" ) == arg )
	{
		SetVerbose( Blocks, val, 1 );
		return 0;
	}

	if ( strstr( arg, "--detailed" ) == arg )
	{
		SetVerbose( Blocks, val, 2 );
		return 0;
	}

	if ( arg[1] == 'f' )
	{
		FILE *errors = out;

		if ( !val )
		{
			ECHO( errors, fprintf( errors, "No path declared '%s'\n", val ) );
			return EINVAL;
		}

		if ( access(val, 0) != 0 )
		{
			ECHO( errors, fprintf( errors, "Couldn't find '%s'\n", val ) );
			return EACCES;
		}

		image_file = val;
		return 0;
	}

	return EINVAL;
}

dint AppArgErr( BLOCKS *Blocks, dint err, dint a, dint num, char *argv[] )
{
	int stop = a + num;
	FILE *out = Blocks->Alloc->out;
	ECHO( out, ECHO_ERR( out, err ) );
	ECHO( out, fprintf( out, "Invalid argument/s: " ) );

	do fprintf( out, " %s", argv[a++] ); while ( a < stop );

	fprintf( out, "\nUsage: APP [--verbose,--detailed,-f PATH]\n" );
	return EXIT_FAILURE;
}

dint ProcessArgv( BLOCKS *Blocks, dint argc, char **argv )
{
	int a, i = -1, num = 0, err;
	FILE *out = Blocks->Alloc->out;

	ECHO
	(
		out,
		fprintf
		(
			out,
			"ProcessArgv( %p, %d, %p )\n",
			(void*)Blocks, argc, (void*)argv
		)
	);

	for ( a = 0; a < argc; ++a )
	{
		char *arg = argv[a];

		if ( !arg || arg[0] == '-' )
		{
			if ( num || i >= 0 )
			{
				err = TryAppArg( Blocks, i, num, argv );

				if ( err )
					return AppArgErr( Blocks, err, i, num, argv );
			}

			i = a;
			num = 0;

			continue;
		}

		++num;
	}

	if ( num || i >= 0 )
	{
		err = TryAppArg( Blocks, i, num, argv );

		if ( err )
			return AppArgErr( Blocks, err, i, num, argv );
	}

	return 0;
}
