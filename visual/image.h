#ifndef INC_VIEW_IMG_FILE_H
#define INC_VIEW_IMG_FILE_H

#include "view.h"
#include "../common/path.h"

extern uint image_lod;

intmax_t ScaleDepthWithoutFPN( uint dstDepth, uint srcDepth, intmax_t value );

typedef struct _CHUNK
{
	ulong	Size;
	char	Type[8];
	int		DataID;
	ulong	CRC;
} CHUNK;

typedef enum _IMG_FILE_PELS
{
	IMG_FILE_VALUE_R = 0,
	IMG_FILE_VALUE_G,
	IMG_FILE_VALUE_B,
	IMG_FILE_VALUE_A,
	IMG_FILE_VALUE_COUNT
} IMG_FILE_PELS;

#define IMG_FILE_CLUT_GREYSCALE 0
#define IMG_FILE_CLUT_TRUECOLOR 2
#define IMG_FILE_CLUT_PALETTEPOS 3
#define IMG_FILE_CLUT_GREYSCALEA 4
#define IMG_FILE_CLUT_TRUECOLORA 6

typedef struct _IMG_FILE
{
	uchar		Depth;
	uchar		Flags;
	uchar		CMethod;
	uchar		FMethod;
	uchar		IMethod;
	time_t		LastChanged;
	float		GammaF;
	u32			GammaU, Pixels, Cols, Rows;
	u32			sBIT, sBITr, sBITg, sBITb;
	LOC_FILE	BinFile;
	/* Buffer IDs */
	dint		DataID, MainDyesID, MiscDyesID, ChunksID, SeemsLikeCallbacksID;
	BLOCKS		*Blocks;
} IMG_FILE;

/* This will later be used to render images to in raw RGBA format */
typedef struct _IMG
{
	uchar	Depth;
	u32		Rows, Cols;
	dint	ViewID, PathID;
	BLOCKS *Blocks;
} IMG;

void	TermImg( IMG *Img );
dint	InitImg( IMG *Img, BLOCKS *Blocks );
dint	DrawImg( IMG *Img, IMG_FILE *ImgFile );
dint	GiveImgPath( IMG *Img, uch const *path );
uch const * TellImgPath( IMG *Img );

typedef dint (*DrawImgFileCB)(
	IMG *Img, IMG_FILE *ImgFile, dint attempts );

uch const * ImgFileGivenPath( IMG_FILE *ImgFile );
uch const * ImgFileFixedPath( IMG_FILE *ImgFile );
void	EchoImgFile( IMG_FILE *ImgFile );
void	TermImgFile( IMG_FILE *ImgFile );
dint	InitImgFile( IMG_FILE *ImgFile, BLOCKS *Blocks );
dint	SeekImgFile( IMG_FILE *ImgFile, uch const *path, uint opt, uint perm );

BLOCKS *GrabImgFileBlocks( IMG_FILE *ImgFile );
BLOCK * GrabImgFileChunks( IMG_FILE *ImgFile );
CHUNK * GrabImgFileChunk( IMG_FILE *ImgFile, dint i );
dint	WantImgFileChunk( IMG_FILE *ImgFile, dint *I, int need );
dint	FindImgFileChunk( IMG_FILE *ImgFile, CHUNK **tok, char const *desc );
/* Used after rendering an image to inform how many long values are used
 * Row 0 is used to provide a valid index for pixel B relative to any pixel in
 * in the 1st row, this means the 1st pixel is always offset from the start of
 * the buffer, the same applies to the 1st pixel of any row, this is to ensure
 * a valid index for pixel A & pixel C at all times so the rendered buffer
 * should look something like the below where x refers to a pixel and rgba refer
 * to the channels:
 * 00 00 00 00 00
 * 00 xr xg xb xa */
void ImageUpdateUsedValues( IMG_FILE *ImgFile, BLOCK *View );
/* Use with the below functions to get equivalent of raw_ptr + pixel, note
 * these functions are used for internal rendering to avoid conditionals
 * during filtering and/or de-filtering, the destination buffer */
typedef union _PIXEL
{
	c16 raw[IMG_FILE_VALUE_COUNT];
	struct
	{
		c16 r;
		c16 g;
		c16 b;
		c16 a;
	};
} PIXEL;
typedef union _RGBA
{
	float raw[IMG_FILE_VALUE_COUNT];
	struct
	{
		float r;
		float g;
		float b;
		float a;
	};
} RGBA;
PIXEL *	ImgFileDye( IMG_FILE *ImgFile, BLOCK *Temp, ulong px );
c16 *	ImgFileVal( IMG_FILE *ImgFile, BLOCK *Temp, ulong px, IMG_FILE_PELS i );
/* Adds any offset/s to produce the index for channel 0 of said pixel */

#define ImagePfxRows 1
#define ImagePfxCols 1
#define ImageSfxRows 1
#define ImageSfxCols 1

/* Adds any offset/s for local pixel indices, index 0 is used for nil, do NOT
 * fill that index */
#define ImageRowX( ROW ) ((ROW) + ImagePfxRows)
#define ImageColX( COL ) ((COL) + ImagePfxCols)

#define ImageDyeX( IMG_FILE, ROW, COL ) \
	((ImageRowX(ROW) * ImageColX(ImageCols(IMG_FILE))) + ImageColX(COL))

#define ImageRows( IMG_FILE ) ((IMG_FILE)->Rows)
#define ImageCols( IMG_FILE ) ((IMG_FILE)->Cols)
#define ImageRowCap( IMG_FILE ) (ImagePfxRows + ImageRows(IMG_FILE) + ImageSfxRows)
#define ImageColCap( IMG_FILE ) (ImagePfxCols + ImageCols(IMG_FILE) + ImageSfxCols)

typedef struct _IMG_FILTER
{
	long  Limit;
	IMG_FILE *ImgFile;
	BLOCK *Brief;
	PIXEL *X, *A, *B, *C, *D;
	IMG_FILE_PELS Value;
} IMG_FILTER;

dint	SetImgFilterTemp( IMG_FILTER *ImgFilter, IMG_FILE *ImgFile, BLOCK *Brief );
void	SetImgFilterPtrs( IMG_FILTER *ImgFilter, ulong row, ulong col );

typedef c16 (*ImgFilterT)( IMG_FILTER *ImgFilter );

c16 ImgFilterReturn0( IMG_FILTER *ImgFilter );
c16 ImgFilterReturnA( IMG_FILTER *ImgFilter );
c16 ImgFilterReturnB( IMG_FILTER *ImgFilter );
c16 ImgFilterReturnC( IMG_FILTER *ImgFilter );
c16 ImgFilterAverage( IMG_FILTER *ImgFilter );
c16 ImgFilterClosest( IMG_FILTER *ImgFilter );

#endif
