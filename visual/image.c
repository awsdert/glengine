#include "image.h"

void TermImg( IMG *Img )
{
	BLOCKS *Blocks = Img->Blocks;

	FreeBlock( Blocks, Img->ViewID );
	FreeBlock( Blocks, Img->PathID );

	memset( Img, 0, sizeof(IMG) );
}

dint InitImg( IMG *Img, BLOCKS *Blocks )
{
	dint err = 0;
	FILE *out = Blocks->Alloc->out;

	Img->Blocks = Blocks;
	err = WantBlock( Blocks, &(Img->PathID), sizeof(uch), 1 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		goto failed;
	}

	err = WantBlock( Blocks, &(Img->ViewID), sizeof(RGBA), 0 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		failed:
		TermImg( Img );
	}

	return err;
}

dint GiveImgPath( IMG *Img, uch const *path )
{
	return utf8tobuff( GrabBlock( Img->Blocks, Img->PathID ), path );
}

uch const * TellImgPath( IMG *Img )
{
	BLOCK *Path = GrabBlock( Img->Blocks, Img->PathID );
	return Path->addr;
}

uch const *GivenImgFilePath( IMG_FILE *ImgFile )
{
	return LocFileGivenPath( &(ImgFile->BinFile) );
}

uch const *FixedImgFilePath( IMG_FILE *ImgFile )
{
	return LocFileFixedPath( &(ImgFile->BinFile) );
}

void TermImgFile( IMG_FILE *ImgFile )
{
	BLOCKS *Blocks = GrabImgFileBlocks( ImgFile );

	if ( Blocks )
	{
		BLOCK *Chunks = GrabImgFileChunks( ImgFile );

		TermLocFileObject( &(ImgFile->BinFile) );

		if ( Chunks )
		{
			int i;
			CHUNK *chunks = GrabImgFileChunk( ImgFile, 0 ), *Chunk;

			for ( i = 0; i < Chunks->have; ++i )
			{
				Chunk = chunks + i;
				FreeBlock( Blocks, Chunk->DataID );
			}
		}

		FreeBlock( Blocks, ImgFile->MainDyesID );
		FreeBlock( Blocks, ImgFile->MiscDyesID );
		FreeBlock( Blocks, ImgFile->ChunksID );
		FreeBlock( Blocks, ImgFile->DataID );
	}

	memset( ImgFile, 0, sizeof(IMG_FILE) );
}

dint InitImgFile( IMG_FILE *ImgFile, BLOCKS *Blocks )
{
	dint i, err, *IDs[] =
	{
		&(ImgFile->DataID),
		&(ImgFile->MainDyesID),
		&(ImgFile->MiscDyesID),
		&(ImgFile->ChunksID),
		NULL
	};

	ImgFile->Blocks = Blocks;

	for ( i = 0; IDs[i]; ++i )
	{
		err = WantBlock( Blocks, IDs[i], 1, 0 );

		if ( err )
		{
			TermImgFile( ImgFile );
			return err;
		}
	}

	err = InitLocFileObject( &(ImgFile->BinFile), Blocks );

	if ( err )
	{
		TermImgFile( ImgFile );
		return err;
	}

	return 0;
}

dint SeekImgFile( IMG_FILE *ImgFile, uch const *path, uint flags, uint mode )
{
	return SeekBinFile( &(ImgFile->BinFile), path, flags, mode );
}

dint	DrawImg( IMG *Img, IMG_FILE *ImgFile )
{
	BLOCK *SeemsLikeCallbacks = GrabBlock
	(
		GrabImgFileBlocks( ImgFile ),
		ImgFile->SeemsLikeCallbacksID
	);
	ach const *seems = NULL;
	DrawImgFileCB DrawImg = NULL;

	seems = LocFileSeemsLike
	(
		SeemsLikeCallbacks,
		&(ImgFile->BinFile),
		(void**)&DrawImg
	);

	if ( !seems )
	{
		dint err = ENODATA;
		FILE *out = ImgFile->Blocks->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	return DrawImg( Img, ImgFile, 1 );
}

void flip_byte_order( void * data, size_t size )
{
	uchar *dst = data;
	size_t i = 0;
	while ( i < size )
	{
		uchar tmp = dst[--size];
		dst[size] = dst[i];
		dst[i++] = tmp;
	}
}

void fill_at_end( void * dst, size_t dstSize, void *src, size_t srcSize )
{
	uchar *d = dst, *s = src;
	if ( srcSize < dstSize )
	{
		while ( srcSize )
			d[--dstSize] = s[--srcSize];
		while ( dstSize )
			d[--dstSize] = 0;
	}
	else
	{
		while ( dstSize )
			d[--dstSize] = s[--srcSize];
	}
}

void large_endian_to_local_endian( void *data, size_t size )
{
	intmax_t local = 0x12345678;
	uchar *loc = (uchar*)&local;

	switch ( loc[0] )
	{
	case 0x78: flip_byte_order( data, size ); return;
	case 0x12: return;
	}

	/* Make it clear a failure happened */
	memset( data, 0, size );
	return;
}

void small_endian_to_local_endian( void *data, size_t size )
{
	intmax_t local = 0x12345678;
	uchar *loc = (uchar*)&local;

	switch ( loc[0] )
	{
	case 0x12: flip_byte_order( data, size ); return;
	case 0x78: return;
	}

	/* Make it clear a failure happened */
	memset( data, 0, size );
	return;
}

BLOCKS *GrabImgFileBlocks( IMG_FILE *ImgFile )
{
	return ImgFile->Blocks;
}

BLOCK *GrabImgFileChunks( IMG_FILE *ImgFile )
{
	return GrabBlock( GrabImgFileBlocks( ImgFile ), ImgFile->ChunksID );
}

CHUNK *GrabImgFileChunk( IMG_FILE *ImgFile, int i )
{
	return GrabChunk( GrabImgFileChunks( ImgFile ), sizeof(CHUNK), i );
}

int WantImgFileChunk( IMG_FILE *ImgFile, int *I, int need )
{
	BLOCKS *Blocks = GrabImgFileBlocks( ImgFile );
	BLOCK *Chunks = GrabImgFileChunks( ImgFile ), *Data;
	CHUNK *Chunk;
	int num = Chunks->used, err = WantChunk( Chunks, sizeof(CHUNK), I, 4 );

	if ( err )
	{
		Chunks->used = num;
		return err;
	}

	Chunk = GrabChunk( Chunks, sizeof(CHUNK), *I );

	if ( Chunk->DataID < 1 )
		err = WantBlock( Blocks, &(Chunk->DataID), sizeof(uch), need );
	else
	{
		Data = GrabBlock( Blocks, Chunk->DataID );
		err = GrowBlock( Data, sizeof(uch), need );
	}

	if ( err )
	{
		Chunks->used = num;
		return err;
	}

	Chunk->CRC = 0;
	Chunk->Size = need;
	memset( Chunk->Type, 0, sizeof(Chunk->Type) );

	Data = GrabBlock( Blocks, Chunk->DataID );
	memset( Data->addr, 0, Data->size );
	Data->used = 0;

	return 0;
}

dint FindImgFileChunk( IMG_FILE *ImgFile, CHUNK **tok, char const * desc )
{
	BLOCK *Chunks = GrabImgFileChunks( ImgFile );
	CHUNK *chunks = Chunks->addr, *chunk = *tok ? *tok + 1 : chunks;
	dint i = (dint)(chunks - chunk);

	for ( ; i < Chunks->used; ++i, ++chunk )
	{
		char *type;
		type = chunk->Type;

		if ( strstr( type, desc ) == type )
		{
			*tok = chunk;
			return i;
		}
	}

	*tok = NULL;
	return -1;
}

void EchoImgFile( IMG_FILE *ImgFile )
{
	BLOCKS *Blocks = GrabImgFileBlocks( ImgFile );
	FILE *out = Blocks->Alloc->out;
	BLOCK *Dyes = GrabBlock( Blocks, ImgFile->MainDyesID );
	BLOCK *Data = GrabBlock( Blocks, ImgFile->DataID );
	uint size = (ImgFile->Flags & 2) + 1;
	uint depth = ImgFile->Depth;
	uint bits = size * depth;
	uint bytes = (depth / CHAR_BIT) + !!(depth % CHAR_BIT);
	uint given = Data->used;
	ulong Rows = ImgFile->Rows;
	ulong Cols = ImgFile->Cols;
	ulong sBIT = ImgFile->sBIT;
	ulong sBITr = ImgFile->sBITr;
	ulong sBITg = ImgFile->sBITg;
	ulong sBITb = ImgFile->sBITb;
	ulong Pixels = ImgFile->Pixels;
	uint expect = Rows * (Cols + 1 * depth);
	uint colour_total_bits = Dyes->used * CHAR_BIT;
	uint colours = colour_total_bits / bits;
	fprintf( out, "Image Details: " );
	fprintf( out, "Cols = %5lu, ",				Cols			);
	fprintf( out, "Rows = %5lu, ",				Rows			);
	fprintf( out, "Depth = %2u, ",				depth			);
	fprintf( out, "sBIT = %08lX, ",				sBIT			);
	fprintf( out, "Pixels = %10lu\n",			Pixels			);
	fprintf( out, "Bytes = %u, ",				bytes			);
	fprintf( out, "Expect = %u, ", 				expect			);
	fprintf( out, "Given = %u, ",				given			);
	fprintf( out, "Given * Bytes = %u, ",		given * bytes	);
	fprintf( out, "sBITr = %08lX, ",			sBITr			);
	fprintf( out, "sBITg = %08lX, ",			sBITg			);
	fprintf( out, "sBITb = %08lX, ",			sBITb			);
	fprintf( out, "Flags = 0x%08X\n",			ImgFile->Flags	);
	fprintf( out, "Shrink Method = 0x%08X, ",	ImgFile->CMethod	);
	fprintf( out, "Filter Method = 0x%08X, ",	ImgFile->FMethod	);
	fprintf( out, "Enlace Method = 0x%08X, ",	ImgFile->IMethod	);
	fprintf( out, "Max Colours %u\n",			colours			);
}

intmax_t ScaleDepthWithoutFPN( uint dstDepth, uint srcDepth, intmax_t value )
{
	intmax_t limit = 0, scale;
	limit = ~(~limit << (srcDepth + dstDepth));
	scale = value ? limit / (value << srcDepth) : 0;
	return scale ? (limit / scale) >> srcDepth : 0;
}

int SetImgFilterTemp( IMG_FILTER *ImgFilter, IMG_FILE *ImgFile, BLOCK *Brief )
{
	ulong Rows = ImagePfxRows + ImageRows(ImgFile) + ImageSfxRows;
	ulong Cols = ImagePfxCols + ImageCols(ImgFile) + ImageSfxCols;
	Brief->used = 0;
	ImgFilter->ImgFile = ImgFile;
	ImgFilter->Brief = Brief;
	ImgFilter->Limit = ~(~0UL << ImgFile->Depth);
	return GrowBlock( Brief, sizeof(PIXEL), Rows * Cols );
}

void ImageUpdateUsedValues( IMG_FILE *ImgFile, BLOCK *View )
{
	ulong Rows = ImageRowX( ImgFile->Rows );
	ulong Cols = ImageColX( ImgFile->Cols );
	View->used = Rows * Cols;
}

PIXEL * ImgFileDye( IMG_FILE *ImgFile, BLOCK *Temp, ulong px )
{
	PIXEL *temp = Temp->addr;
	return temp + px;
}

c16 * ImgFileVal( IMG_FILE *ImgFile, BLOCK *Temp, ulong px, IMG_FILE_PELS i )
{
	PIXEL *Dye = ImgFileDye( ImgFile, Temp, px );
	return Dye->raw + i;
}

void SetImgFilterPtrs( IMG_FILTER *ImgFilter, ulong row, ulong col )
{
	IMG_FILE *ImgFile = ImgFilter->ImgFile;
	PIXEL *pixels = ImgFilter->Brief->addr, *X, *B;
	ulong Cols = ImageColX(ImageCols(ImgFile));
	ulong Row = ImageRowX(row), Col = ImageColX(col);
	ulong x = (Row * Cols) + Col;

	ImgFilter->X = X = pixels + x;
	ImgFilter->A = X - 1;
	ImgFilter->B = B = X - ImageColX(ImageCols(ImgFile));
	ImgFilter->C = B - 1;
	ImgFilter->D = B + 1;
}

c16 ImgFilterReturn0( IMG_FILTER *ImgFilter )
{
	(void)ImgFilter;
	return 0;
}

c16 ImgFilterReturnA( IMG_FILTER *ImgFilter )
{
	return ImgFilter->A->raw[ImgFilter->Value];
}

c16 ImgFilterReturnB( IMG_FILTER *ImgFilter )
{
	return ImgFilter->B->raw[ImgFilter->Value];
}

c16 ImgFilterReturnC( IMG_FILTER *ImgFilter )
{
	return ImgFilter->C->raw[ImgFilter->Value];
}

c16 ImgFilterReturnD( IMG_FILTER *ImgFilter )
{
	return ImgFilter->D->raw[ImgFilter->Value];
}

c16 ImgFilterAverage( IMG_FILTER *ImgFilter )
{
	long double a = ImgFilterReturnA( ImgFilter );
	long double b = ImgFilterReturnB( ImgFilter );
	return ((a + b) / 2);
}

c16 ImgFilterClosest( IMG_FILTER *ImgFilter )
{
	long a = ImgFilterReturnA( ImgFilter );
	long b = ImgFilterReturnB( ImgFilter );
	long c = ImgFilterReturnC( ImgFilter );
	long p = (a + b) - c;
	long pa = labs(p - a);
	long pb = labs(p - b);
	long pc = labs(p - c);

	if ( pa <= pb && pa <= pc )
		return a;

	if ( pb <= pc )
		return b;

	return c;
}
