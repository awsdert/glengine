#include "ppm.h"

int SaveImgFilePPM( FILE *file, IMG *Img )
{
	ulong Rows = Img->Rows, row;
	ulong Cols = Img->Cols, col, i;
	ulong depth = Img->Depth;
	ulong keep = ~(~0LU << depth);
	BLOCK *View = GrabBlock( Img->Blocks, Img->ViewID );
	RGBA *view = View->addr;

	fprintf( file, "P3\n%lu %lu\n%lu\n", Cols, Rows, keep );

	for ( row = 0; row < Rows; ++row )
	{
		char * whitespace = "\n";
		RGBA *vrow = view + (row * Cols);

		for ( col = 0; col < Cols; ++col )
		{
			RGBA *vcol = vrow + col;

			for ( i = 0; i < IMG_FILE_VALUE_A; ++i )
			{
				ulong val = vcol->raw[i];
				fprintf( file, "%s%5lu", whitespace, val );
				whitespace = " ";
			}
		}
	}

	fputc('\n', file );

	fflush( file );
	return 0;
}
