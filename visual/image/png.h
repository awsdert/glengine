#ifndef INC_VIEW_IMG_FILE_PNG_H
#define INC_VIEW_IMG_FILE_PNG_H

#include "../image.h"

ach const * SeemsLikeImagePNG( LOC_FILE *LocFile );
dint DrawImagePNG( IMG *Img, IMG_FILE *ImgFile, dint attempts );
dint GiveSeemsLikeImagePNG( IMG_FILE *ImgFile );

#endif
