#ifndef IMG_FILE_JPEG_H
#define IMG_FILE_JPEG_H

#include "common.h"

typedef struct _IMG_JPEG_FRAME
{
	bool Progresive;
	uint SpanX;
	uint SpanY;
	uint SubSamples;
	BUFF Components;
} IMG_JPEG_FRAME;

typedef struct _IMG_JPEG_ENTRY
{
	uint Format;
	BUFF Buffer;
} IMG_JPEG_ENTRY;

typedef struct _IMG_JPEG
{
	BUFF GivenPath;
	BUFF Entries;
} IMG_JPEG;

int load_img_jpeg( IMG_JPEG *jpeg, tch const * const path );

#endif
