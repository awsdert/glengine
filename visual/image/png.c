#include "png.h"

uint image_lod = 0;

int LoadPNGChunk_IHDR( IMG_FILE *ImgFile, CHUNK *Chunk )
{
	BLOCK *Data = GrabBlock( ImgFile->Blocks, Chunk->DataID );
	uchar *data = Data->addr;

	ImgFile->Cols = 0; memcpy( &(ImgFile->Cols), data + 0, 4 );
	ImgFile->Rows = 0; memcpy( &(ImgFile->Rows), data + 4, 4 );
	ImgFile->Cols <<= bitsof(u32) - 32;
	ImgFile->Rows <<= bitsof(u32) - 32;
	large_endian_to_local_endian( &(ImgFile->Cols), sizeof(u32) );
	large_endian_to_local_endian( &(ImgFile->Rows), sizeof(u32) );

	ImgFile->Depth = data[8];
	ImgFile->Flags = data[9];
	ImgFile->CMethod = data[10];
	ImgFile->FMethod = data[11];
	ImgFile->IMethod = data[12];

	ImgFile->Pixels = (ImgFile->Cols) * (ImgFile->Rows);
	return 0;
}

int LoadPNGChunk_gAMA( IMG_FILE *ImgFile, CHUNK *Chunk )
{
	u32 *gamma = &(ImgFile->GammaU);
	BLOCK *Gamma = GrabBlock( ImgFile->Blocks, Chunk->DataID );
	ForeBits( gamma, Gamma->addr, 0, Chunk->Size * 8 );
	large_endian_to_local_endian( gamma, Chunk->Size );
	ImgFile->GammaF = *gamma;
	ImgFile->GammaF /= 100000.0;
	return 0;
}

int ImageHeaderIsPNG( IMG_FILE *ImgFile, ISTREAM *IStreamFile )
{
	uch expected[9] = {0x89, 'P', 'N', 'G', '\r', '\n', 0x1A, '\n', 0 };
	uch actually[9] = {0};
	FILE *out = ImgFile->Blocks->Alloc->out;
	int i;

	for ( i = 0; i < 8; ++i )
	{
		actually[i] = IStreamBits( IStreamFile, 8, true );

		if ( IStreamFile->err )
		{
			ECHO( out, ECHO_ERR( out, IStreamFile->err ) );
			EchoIStreamDetails( out, IStreamFile );
			return IStreamFile->err;
		}
	}

	if ( memcmp( actually, expected, 9 * sizeof(uch) ) != 0 )
		return EINVAL;

	return 0;
}

//#define TEST_AGAINST_OFFICIAL_ZLIB
#ifdef TEST_AGAINST_OFFICIAL_ZLIB
#include <zlib.h>

void * png_zlib_alloc( void *ud, uint want, uint size )
{
	(void)ud;
	return malloc( want * size );
}

void png_zlib_free( void *ud, void *ptr )
{
	(void)ud;
	if ( ptr )
		free( ptr );
}

int png_zlib_error( int err )
{
	switch ( err )
	{
	case Z_OK: return 0;
	case Z_STREAM_END: return 0;
	case Z_ERRNO: return errno;
	case Z_NEED_DICT: return ENOENT;
	case Z_STREAM_ERROR: return EINVAL;
	case Z_DATA_ERROR: return ESPIPE;
	case Z_MEM_ERROR: return ENOMEM;
	case Z_VERSION_ERROR: return EDOM;
	}

	return  err;
}

int LoadPNGChunkUsingOfficialZlib_IDAT( IMG_FILE *ImgFile, CHUNK *Chunk, int IntoID )
{
	int err;
	BLOCKS *Blocks = ImgFile->Blocks;
	BLOCK *iData = GrabBlock( Blocks, Chunk->DataID );
	BLOCK *oData = GrabBlock( Blocks, IntoID );
	uchar *idata = iData->addr, *odata;
	FILE *out = Blocks->Alloc->out;
	z_stream zstream = {0};

	err = GrowBlock( oData, sizeof(uch), ImgFile->Pixels * 4 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	odata = oData->addr;

	zstream.opaque	= Image;
	zstream.zalloc	= png_zlib_alloc;
	zstream.zfree	= png_zlib_free;

	zstream.next_in		= idata;
	zstream.avail_in	= iData->used;

	zstream.next_out	= odata + oData->used;
	zstream.avail_out	= oData->size - oData->used;

	err = png_zlib_error(inflateInit( &zstream ));

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	err = png_zlib_error(inflate( &zstream, Z_NO_FLUSH ));

	if ( err )
	{
		ECHO
		(
			out,
			ECHO_ERR( out, err );
			fprintf( out, "Last zlib message: '%s'\n", zstream.msg )
		);
		return err;
	}

	inflateEnd( &zstream );

	oData->used += zstream.total_out;

	return 0;
}

#endif

int LoadPNGChunk_IDAT( IMG_FILE *ImgFile, CHUNK *Chunk, int IntoID )
{
#ifndef TEST_AGAINST_OFFICIAL_ZLIB
	ZLIB zlib = {0};
	SetupZlibObject( &zlib, ImgFile->Blocks );
	return ExpandZlibArchive( &zlib, Chunk->DataID, IntoID );
#else
	return LoadPNGChunkUsingOfficialZlib_IDAT( Image, Chunk, IntoID );
#endif
}

ach const * SeemsLikeImagePNG( LOC_FILE *LocFile )
{
	ISTREAM IStream = {0};
	ISTREAM_BLOCK_ID IStreamID = {0};
	dint TempID = LocFile->BriefDataID;
	BLOCK *File = GrabBlock( LocFile->Blocks, TempID );
	dint err = GrowBlock( File, sizeof(uch), 9 );
	uch expected[9] = { 0x89, 'P', 'N', 'G', '\r', '\n', 0x1A, '\n', 0 };
	uch actually[9] = { 0 };
	FILE *out = File->Alloc->out;
	long done = 0;
	int i;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return NULL;
	}

	err = CopyBinFileRegion( LocFile, File->addr, sizeof(uch) * 8, &done, 1 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return NULL;
	}

	IStreamID.Blocks = LocFile->Blocks;
	IStreamID.id = TempID;
	IStream.ud = &IStreamID;
	IStream.DataCB = IStreamDataFromBufferIndex;
	File->used = 8;

	for ( i = 0; i < 8; ++i )
	{
		actually[i] = IStreamBits( &IStream, 8, true );

		if ( IStream.err )
		{
			ECHO( out, ECHO_ERR( out, IStream.err ) );
			EchoIStreamDetails( out, &IStream );
			LocFile->err = IStream.err;
			return NULL;
		}
	}

	if ( memcmp( actually, expected, 9 * sizeof(uch) ) != 0 )
		return NULL;

	return "PNG Image";
}

/* TODO: Adapt this to CHAR_BIT */
dint InternalDrawImagePNG( IMG_FILE *ImgFile, int DataID, int TempID, IMG *Img );

dint GiveSeemsLikeImagePNG( IMG_FILE *ImgFile )
{
	BLOCK *SeemsLikeCallbacks =
		GrabBlock( ImgFile->Blocks, ImgFile->SeemsLikeCallbacksID );

	return GiveLocFileSeemLikeCB
	(
		SeemsLikeCallbacks,
		SeemsLikeImagePNG,
		sizeof(uch) * 8,
		(void*)(DrawImagePNG)
	);
}

dint DrawImagePNG( IMG *Img, IMG_FILE *ImgFile, dint attempts )
{
	BLOCKS *Blocks = ImgFile->Blocks;
	LOC_FILE *BinFile = &(ImgFile->BinFile);
	ISTREAM IStream = {0};
	ISTREAM_BLOCK_ID IStreamID = {0};
	dint TempID = ImgFile->BinFile.BriefDataID;
	dint DataID = ImgFile->DataID, i, err;
	BLOCK *Temp = GrabBlock( Blocks, TempID ), *Data;
	BLOCK *Chunks = GrabImgFileChunks( ImgFile );
	CHUNK *Chunk;
	uchar *data;
	intmax_t done = 0;
	FILE *out = Blocks->Alloc->out;
	uch header[9] = {0};

	Temp->used = 0;
	done = CopyEntireBinFile( BinFile, Temp, attempts );
	SeekBinFile( BinFile, NULL, 0, 0 );

	if ( done <= 0 )
	{
		ECHO
		(
			out,
			fprintf
			(
				out,
				"Failed to copy PNG file contents into memory space\n"
			)
		);
		return EIO;
	}

	Chunks->used = 0;
	ImgFile->Cols = 0;
	ImgFile->Rows = 0;
	ImgFile->Pixels = 0;
	ImgFile->Flags = 0;
	ImgFile->GammaF = 0.0;
	ImgFile->GammaU = 0;
	ImgFile->sBIT = 0;
	ImgFile->sBITr = 0;
	ImgFile->sBITg = 0;
	ImgFile->sBITb = 0;

	IStreamID.Blocks = Blocks;
	IStreamID.id = TempID;
	IStream.ud = &IStreamID;
	IStream.DataCB = IStreamDataFromBufferIndex;
	Temp->used = done;

	/* We don't need the header but we want the stream to start in the right
	 * place */
	IStreamData( &IStream, header, 8 * 8, true );

	if ( image_lod )
		ECHO( out, fprintf( out, "image_lod = %u\n", image_lod ) );

	if ( IStream.err == EOF )
	{
		ECHO( out, ECHO_ERR( out, EIO ) );
		return EIO;
	}

	while ( IStream.err != EOF )
	{
		intmax_t size = IStreamBits( &IStream, 32, true );
		size <<= bitsof(intmax_t) - 32;
		large_endian_to_local_endian( &size, sizeof(intmax_t) );

		err = WantImgFileChunk( ImgFile, &i, size );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}

		Chunk = GrabImgFileChunk( ImgFile, i );

		memset( Chunk->Type, 0, 8 );
		err = IStreamData( &IStream, Chunk->Type, 32, true );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}

		if ( strcmp( Chunk->Type, "IEND" ) == 0 )
		{
			if ( image_lod )
			{
				ECHO
				(
					out,
					fprintf
					(
						out,
						"chunks[%2u] %12zu %08lX '%s'\n",
						i,
						Chunk->Size,
						Chunk->CRC,
						Chunk->Type
					)
				);
			}
			break;
		}

		Data = GrabBlock( Blocks, Chunk->DataID );
		data = Data->addr;

		if ( !data )
		{
			ECHO( out, ECHO_ERR( out, ENOMEM ) );
			return ENOMEM;
		}

		IStreamData( &IStream, data, size * 8, true );
		Data->used = size;

		Chunk->CRC = IStreamBits( &IStream, 32, true );
		Chunk->CRC <<= bitsof(ulong) - 32;
		large_endian_to_local_endian( &(Chunk->CRC), sizeof(ulong) );

		if ( image_lod )
		{
			ECHO
			(
				out,
				fprintf
				(
					out,
					"chunks[%2u] %12zu %08lX '%s'\n",
					i,
					Chunk->Size,
					Chunk->CRC,
					Chunk->Type
				)
			);
		}
	}

	if ( image_lod )
		ECHO( out, fputs( "Checking IHDR chunk is 1st...\n", out ) );

	Chunk = NULL;
	i = FindImgFileChunk( ImgFile, &Chunk, "IHDR" );

	if ( !Chunk )
	{
		err = ENODATA;
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}
	else if ( i )
		ECHO( out, fputs( "Error: IHDR should be first chunk\n", out ) );

	if ( image_lod )
		ECHO( out, fputs( "Found! Extracting information...\n", out ) );

	err = LoadPNGChunk_IHDR( ImgFile, Chunk );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	Data = GrabBlock( Blocks, DataID );
	Data->used = 0;

	if ( image_lod )
		ECHO( out, fputs( "Decompressing IDAT chunks...\n", out ) );

	Chunk = NULL;
	for ( i = 0, done = 0; i >= 0; ++done, ++i )
	{
		i = FindImgFileChunk( ImgFile, &Chunk, "IDAT" );

		if ( !Chunk )
			break;

		err = LoadPNGChunk_IDAT( ImgFile, Chunk, DataID );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	if ( done < 1 )
	{
		ECHO
		(
			out,
			ECHO_ERR( out, ENODATA );
			fputs( "No IDAT chunks found!\n", out )
		);
		return ENODATA;
	}

	if ( image_lod )
		ECHO( out, fputs( "Processing gAMA chunk if it exists...\n", out ) );

	Chunk = NULL;
	i = FindImgFileChunk( ImgFile, &Chunk, "gAMA" );

	if ( Chunk )
	{
		err = LoadPNGChunk_gAMA( ImgFile, Chunk );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	if ( ImgFile->Flags & 2 )
	{
		ImgFile->sBITr = (ImgFile->sBIT & 0xFF00) >> 8;
		ImgFile->sBITg = (ImgFile->sBIT & 0xFF0000) >> 16;
		ImgFile->sBITb = (ImgFile->sBIT & 0xFF000000) >> 24;
	}

	if ( image_lod )
		ECHO( out, fputs( "Attempting to render to given block...\n", out ) );

	return InternalDrawImagePNG( ImgFile, DataID, TempID, Img );
}

ImgFilterT ImgFiltersPNG[5] =
{
	ImgFilterReturn0,
	ImgFilterReturnA,
	ImgFilterReturnB,
	ImgFilterAverage,
	ImgFilterClosest
};

c16 NextImageSampleByBytePNG( uch **tok, c16 *bit, uch depth, bool bounds_check )
{
	uch *t = *tok;
	c16 val = *t;

	(void)bit;
	(void)bounds_check;

	++t;

	if ( depth > 8 )
	{
		val <<= 8;
		val |= *t;
		++t;
	}

	*tok = t;

	return val;
}

c16 NextImageSampleByBitsPNG( uch **tok, c16 *bit, uch depth, bool bounds_check )
{
	uch *t = *tok, i;
	c16  b = *bit, val = 0, top = 1u << (depth - 1);

	for ( i = 0; b && i < depth; ++i, b >>= 1 )
	{
		val >>= 1;
		val |= !!(*t & b) ? top : 0;
	}

	if ( !b || (bounds_check && b < 0x80) )
	{
		++t;
		b = 0x80;
	}

	*tok = t;
	*bit = b;

	return val;
}

dint InternalDrawImagePNG( IMG_FILE *ImgFile, int DataID, int TempID, IMG *Img )
{
	ulong Cols = ImgFile->Cols, col;
	ulong Rows = ImgFile->Rows, row;
	ulong depth = ImgFile->Depth;
	ulong shift = bitsof(c16) - depth;
	ulong values = (ImgFile->Flags & 2) + 1;
	BLOCKS *Blocks = ImgFile->Blocks;
	FILE *out = Blocks->Alloc->out;
	dint i, err;
	BLOCK *Data = GrabBlock( Blocks, DataID );
	BLOCK *Temp = GrabBlock( Blocks, TempID );
	BLOCK *View = GrabBlock( Blocks, Img->ViewID );
	uch *data = Data->addr;
	c16 bit = 0x80u;
	float max = C16_MAX;
	RGBA *view = NULL;
	IMG_FILTER ImgFilter = {0};
	c16 (*NextImageSample)( uch **tok, c16 *bit, uch depth, bool bounds_check )
		 = (depth < 8) ? NextImageSampleByBitsPNG : NextImageSampleByBytePNG;

	err = SetImgFilterTemp( &ImgFilter, ImgFile, Temp );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	View->used = 0;
	err = GrowBlock( View, sizeof(RGBA), Rows * Cols );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	view = View->addr;

	if ( image_lod )
	{
		ECHO
		(
			out,
			fprintf
			(
				out,
				"Rendering png image, "
				"bitsof(uchar) vs bitsof(uch) vs bistof(c16)"
				" = %zu vs %zu vs %zu ...\n",
				bitsof(uchar), bitsof(uch), bitsof(c16)
			)
			//; EchoIStreamDetails( out, &IStream )
		);
	}

	for ( row = 0; row < Rows; ++row )
	{
		uint filter = *(data++);
		RGBA *vrow = view + (row * Cols);

		if ( filter > 4 )
		{
			err = EINVAL;
			ECHO( out, fprintf( out, "filter = %u (0x%X)!\n", filter, filter ) );
			return err;
		}

		if ( image_lod > 1 )
			fprintf( out, "%3u [", filter );

		for ( col = 0; col < Cols; ++col )
		{
			RGBA *vcol = vrow + col;
			PIXEL *X;

			SetImgFilterPtrs( &ImgFilter, row, col );
			X = ImgFilter.X;

			for ( i = 0; i < values; ++i )
			{
				c16 *PX = X->raw + i;
				float *VX = vcol->raw + i;
				c16 loc;
				c16 val = NextImageSample( &data, &bit, depth, false );

				ImgFilter.Value = i;
				loc = ImgFiltersPNG[filter]( &ImgFilter );

				val <<= shift;

				if ( image_lod > 1 )
					fprintf( out, " %4lX", (ulong)val );

				*PX = loc + val;
				*VX = *PX >> shift;
				*VX /= max;
			}

			for ( ; i < IMG_FILE_VALUE_A; ++i )
			{
				c16 *P0 = X->raw;
				c16 *PX = X->raw + i;
				float *V0 = vcol->raw + 0;
				float *VX = vcol->raw + i;
				*PX = *P0;
				*VX = *V0;
			}
		}

		NextImageSample( &data, &bit, 0, true );

		if ( image_lod > 1 )
		{
			fputs( " ]\n", out );
			//EchoIStreamDetails( out, &IStream );
		}
	}

	View->used = Rows * Cols;
	Img->Rows = Rows;
	Img->Cols = Cols;

	ImageUpdateUsedValues( ImgFile, View );

	return 0;
}
