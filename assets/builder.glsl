#version 330

in vec2 Texel;
in vec4 Color;
in vec3 Place;
in vec3 Normal;
in vec3 Tangent;
in vec3 BiNormal;

struct CHANGE
{
	vec4 Tint;
	vec3 Spot;
	vec3 Span;
};

uniform bool Covered;
uniform CHANGE Strip;
uniform CHANGE Model;
uniform CHANGE Stage;

varying vec2 Texture;
varying vec4 Pigment;

void main()
{
	gl_Position = vec4( Place, 1.0 );
	Pigment = Color;
	Texture = Texel;
}
