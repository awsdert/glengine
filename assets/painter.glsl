#version 330

uniform bool Covered;
uniform sampler2D Cover2D;

varying vec2 Texture;
varying vec4 Pigment;

void main()
{
	gl_FragColor = Covered ? texture( Cover2D, Texture ) : Pigment;
}
