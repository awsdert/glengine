#include "main.h"

int RenderJoint( CORE *Core, GFX_STAGE *Stage, GFX_MODEL *GfxModel, GFX_JOINT *Joint )
{
	GFX *Gfx = GrabCoreGfxHandle( Core );
	GFX_STRIP *Strip = GrabGfxStrip( Gfx, GrabGfxJointStripID( Joint ) );
	GPU_COVERED *GpuCovered = &(Core->GpuCovered);
	GPU_TRAITS *GpuTraits = &(Core->GpuTraits);
	uint TraitVAO = Gfx->CacheVao;
	dint i;

	if ( !Strip || Strip->count < 1 )
		return 0;

	Gfx->BindVaoGroup( TraitVAO );
	glX( NULL, glUniform1ui( GpuCovered->Loc, !!(Joint->GaveImageID) ) );

	if ( Joint->GaveImageID )
	{
		GFX_IMAGE *Image = GrabGfxImage( Gfx, GrabGfxJointImageID( Joint ) );
		DrawRngGfxImage( Image );
		TellGpuOfGfxImage( Image );
		SendGfxImageToGpu( Image );
	}

	for ( i = 0; i < TRAIT_COUNT; ++i )
	{
		ENUM_CACHE f = Gfx->MapTrait[i];
		uint FloatVBO = Gfx->CacheVBOs[f];
		uint TraitVBO = Strip->Vbo;
		uint location = GpuTraits->Loc[i];

		if ( i != TRAIT_TEXEL && i != TRAIT_PLACE && i != TRAIT_COLOR )
			continue;

#if 0
		ECHO
		(
			out,
			fprintf
			(
				out,
				"trait = %u, Cache->size = %zu\n"
				"TraitID = %u, CacheID = %u, IndexID = %u\n",
				trait,
				Cache->size,
				TraitID,
				CacheID,
				IndexID
			)
		);
#endif
		Gfx->BindVboCache( GL_ARRAY_BUFFER, FloatVBO, location, f );
		Gfx->SendVboBlock
		(
			GL_ELEMENT_ARRAY_BUFFER,
			TraitVBO,
			GrabGfxStripBlock( Strip, i ),
			GL_DYNAMIC_DRAW
		);
	}
	//glX( NULL, glInvalidateBriefData( CacheID ) );
	//glX( NULL, glVertexArrayElementBuffer( TraitVBO, IndexVBO ));
	glX( NULL, glDrawElements
	(
		Strip->DrawAs,
		Strip->count,
		GL_UNSIGNED_INT,
		NULL
	));

	return 0;
}

int RenderGfxModel( CORE *Core, GFX_STAGE *Stage, GFX_MODEL *GfxModel )
{
	BLOCKS *Blocks = GrabCoreBlocks( Core );
	FILE *out = Blocks->Alloc->out;
	GFX *Gfx = GrabGfxModelGfxHandle( GfxModel );
	BLOCK *JointIDs = GrabGfxModelJointIDs( GfxModel );
	dint *jointIDs = JointIDs->addr;
	int i, err;

	for ( i = 0; i < JointIDs->used; ++i )
	{
		GFX_JOINT *Joint = GrabGfxJoint( Gfx, jointIDs[i] );

		err = RenderJoint( Core, Stage, GfxModel, Joint );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	return 0;
}

int RenderStage( CORE *Core, GFX_STAGE *Stage )
{
	BLOCKS *Blocks = GrabCoreBlocks( Core );
	FILE *out = Blocks->Alloc->out;
	GFX *Gfx = GrabCoreGfxHandle( Core );
	BLOCK *StageIDs = GrabGfxStageStageIDs( Stage );
	BLOCK *ModelIDs = GrabGfxStageModelIDs( Stage );
	int i, err, *CacheIDs = Gfx->BlockIDs;
	dint *stageIDs = StageIDs->addr, *modelIDs = ModelIDs->addr;

	for ( i = CACHE_QUADS; i >= 0; --i )
	{
		uint vbo = Gfx->CacheVBOs[i];
		BLOCK *F = GrabBlock( Blocks, CacheIDs[i] );

		if ( F->addr )
		{
			glX
			(
				NULL,
				glNamedBufferData( vbo, F->size, F->addr, GL_STATIC_DRAW )
			);
		}
	}

	for ( i = 0; i < ModelIDs->used; ++i )
	{
		GFX_MODEL *GfxModel = GrabGfxModel( Gfx, modelIDs[i] );

		if ( Blocks->Alloc->lod > 2 )
		{
			ECHO( out, fprintf( out, "GfxModels[%u]\n", i ) );
			EchoGfxModel( GfxModel );
		}

		err = RenderGfxModel( Core, Stage, GfxModel );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	for ( i = 0; i < StageIDs->used; ++i )
	{
		GFX_STAGE *GfxStage = GrabGfxStage( Gfx, stageIDs[i] );

		err = RenderStage( Core, GfxStage );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	return 0;
}

void RenderView( GLFWwindow *window )
{
	int i, err;
	CORE *Core = &core;
	BLOCKS *Blocks = GrabCoreBlocks( Core );
	FILE *out = Blocks->Alloc->out;
	// Clear the screen
	glX( NULL, glUseProgram( core.GpuBinary->ID ));
	glX( NULL, glClearColor( 0, 0, 0, 0 ));
	glX( NULL, glClear( GL_COLOR_BUFFER_BIT ));

	for ( i = 0; i < TRAIT_COUNT; ++i )
	{
		if ( i != TRAIT_PLACE && i != TRAIT_COLOR )
			continue;

		glX( NULL, glEnableVertexAttribArray( Core->GpuTraits.Loc[i] ));
	}

	if ( Blocks->Alloc->lod > 2 )
	{
		ECHO( out, fputs( "Root Stage\n", out ) );
		//EchoStage( Core, &(Core->Batch->Stage) );
	}

	err = RenderStage( Core, GrabCoreStage( Core ) );

	if ( err )
		ECHO( out, ECHO_ERR( out, err ) );

	for ( i = 0; i < TRAIT_COUNT; ++i )
	{
		if ( i != TRAIT_PLACE && i != TRAIT_COLOR )
			continue;

		glX( NULL, glDisableVertexAttribArray( Core->GpuTraits.Loc[i] ));
	}

	glfwSwapBuffers(window);
}
