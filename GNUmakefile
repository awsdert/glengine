USING_EOG:=$(if $(filter eog,$(MAKECMDGOALS)),true,)

EXISTS=$(if $1,$(if $(wildcard $2),$2,$1))
PNG:=$(call EXISTS,$(PNG),pngsuite/$(PNG))
PNG:=$(call EXISTS,$(PNG),pngsuite/$(PNG).png)
PNG:=$(call EXISTS,$(PNG),$(PNG).png)
ARGS+=$(if $(PNG),-f $(PNG),)

CC:=gcc
CS:=mcs
LD:=clang
CXX:=clang++
DEFINES?=
CFLAGS?=
CXXFLAGS?=

wildcard3=$(wildcard $1) $(wildcard */$1) $(wildcard */*/$1)

SRC_FILES:=$(call wildcard3,*.c)
CPP_FILES:=$(call wildcard3,*.cpp)
INC_FILES:=$(call wildcard3,*.h)
HPP_FILES:=$(INC_FILES) $(call wildcard3,*.hpp)

CLONED?=..
USR_INC_DIR:=$(if $(wildcard /usr/include),/usr/include)
USR_LIB_DIR:=$(if $(wildcard /usr/lib),/usr/lib)
chk_usr_dir=$(if $(wildcard $2),$2,$1)

CGLM_DIR?=$(CLONED)/cglm
CGLM_INC_DIR?=$(call chk_usr_dir,$(CGLM_DIR)/include,$(USR_INC_DIR)/cglm)
CGLM_LIB_DIR?=$(call chk_usr_dir,$(CGLM_DIR)/lib,$(USR_LIB_DIR)/cglm)

IFLAGS:=-I "$(CGLM_INC_DIR)"
LFLAGS:=-L "$(CGLM_LIB_DIR)"
INC_LIBS:=-l glfw -l GL -l GLEW -lm -lz
DEFINES+= $(if $(USE_GFX_MODEL_BUFFERS),-D USE_GFX_MODEL_BUFFERS)
DEFINES+= $(if $(USING_EOG),-D USING_EOG)
CFLAGS+= -Wall $(IFLAGS) $(DEFINES)
CXXFLAGS+= -Wall $(IFLAGS) $(DEFINES)

build_program:=a.out
build_objects:=$(SRC_FILES:%=%.o) $(CPP_FILES:%=%.o)
build_options:=-D NDEBUG
debug_program:=d.out
debug_objects:=$(SRC_FILES:%=%.d.o) $(CPP_FILES:%=%.d.o)
debug_options:=-ggdb -D _DEBUG

get_flags=$($(if $(substr debug,$1),_,N)DEBUG_FLAGS)

info:
	@echo SRC_FILES=$(SRC_FILES)
	@echo CPP_FILES=$(CPP_FILES)
	@echo obj_files=$(obj_files)
	@echo INC_FILES=$(INC_FILES)
	@echo HPP_FILES=$(HPP_FILES)

cppcheck:
	cppcheck $(SRC_FILES)

check_png:
	$(if $(PNG),pngcheck -cvt $(PNG),@echo)

run: build
	./$(build_program) $(ARGS)

valgrind: debug
	valgrind --leak-check=full --show-leak-kinds=all ./$(debug_program) $(ARGS)

gdb: debug
	gdb -ex run --args ./$(debug_program) $(ARGS)

gede: debug
	gede --args ./$(debug_program) $(ARGS)

rebuild: clean buildall

buildall: build debug

build: $(build_objects)

debug: $(debug_objects)

build debug:
	$(LD) $(LFLAGS) $($@_options) -o $($@_program) $($@_objects) $(INC_LIBS)

objects: $(build_objects) $(debug_objects)

clean:
	rm -f *.o *.out *.zip gede2.ini */*.o */*/*.o

%.c.o: %.c $(INC_FILES)
	$(CC) $(build_options) $(CFLAGS) -o $@ -c $*.c

%.cpp.o: %.cpp $(HPP_FILES)
	$(CXX) $(build_options) $(CXXFLAGS) -o $@ -c $*.cpp

%.c.d.o: %.c $(INC_FILES)
	$(CC) $(debug_options) $(CFLAGS) -o $@ -c $*.c

%.cpp.d.o: %.cpp $(HPP_FILES)
	$(CXX) $(debug_options) $(CXXFLAGS) -o $@ -c $*.cpp

#.PHONY: objects clean run eog gede debug rebuild build info
