#include "core.h"

int InitGpuShader( GPU_SHADER *Shader, BLOCKS *Blocks )
{
	Shader->Blocks = Blocks;

	if ( Blocks )
	{
		int err = WantBlock( Blocks, &(Shader->CodeID), 1, BUFSIZ );

		if ( err )
		{
			FILE *out = Blocks->Alloc->out;
			ECHO( out, ECHO_ERR( out, err ) );
			TermGpuShader( Shader );
			return err;
		}

		return 0;
	}

	return EFAULT;
}

void TermGpuShader( GPU_SHADER *Shader )
{
	BLOCKS *Blocks = Shader->Blocks;

	if ( Blocks )
	{
		if ( Shader->Built )
			glDeleteShader( Shader->ID );

		FreeBlock( Blocks, Shader->CodeID );
	}

	memset( Shader, 0, sizeof(GPU_SHADER) );
}

void   DebugGpuShader( GPU_SHADER *Shader, uch const * const path )
{
	BLOCKS *Blocks = Shader->Blocks;
	char Temp[BUFSIZ] = {0};
	ANSI *Log;
	char *log, *tmp;
	int err, LogID, len = 0, did, n;

	glGetShaderiv( Shader->ID, GL_INFO_LOG_LENGTH, &len );

	err = WantBlock( Blocks, &LogID, 1, len + 1 );

	if ( err )
	{
		len = BUFSIZ - 1;
		log = Temp;
	}
	{
		Log = GrabBlock( Blocks, LogID );
		log = Log->addr;
	}

	glGetShaderInfoLog( Shader->ID, len, NULL, log );
	log[len] = 0;
	did = sscanf( log, "%d", &n );
	tmp = log + did;

	fprintf
	(
		errout,
		"[OPENGL GPU_SHADER ERROR]:\n%s:%s\n",
		path, (*tmp == ':') ? tmp + 1 : log
	);

	if ( LogID > 0 )
		FreeBlock( Blocks, LogID );
}

int MakeGpuShader
(
	GPU_SHADER *Shader,
	uch const * const path,
	uint type
)
{
	char *code;
	char const *codes[] = {NULL,NULL};
	size_t size = 0, done;
	BLOCKS *Blocks = Shader->Blocks;
	FILE *file;
	ANSI *Code;
	int err;

	Code = GrabBlock( Blocks, Shader->CodeID );
	file = fopen( path, "r" );
	err = errno;

	if ( !file )
		return err;

	do
	{
		err = GrowBlock( Code, 1, size + BUFSIZ );

		if ( err )
		{
			fclose(file);
			return err;
		}

		code = Code->addr;
		memset( code + size, 0, Code->size - size );
		size += done = fread( code + size, 1, BUFSIZ, file );
	}
	while ( done == BUFSIZ );

	fclose(file);

	Code->used = size;
	err = GrowBlock( Code, 1, size + 2 );

	if ( err )
		return err;

	code = Code->addr;
	memset( code + size, 0, Code->size - size );

	err = CropBlock( Code, 1, size + 2 );
	code = Code->addr;
	code[size] = '\n';
	Code->used = size + 2;

	codes[0] = code;

	Shader->ID = glCreateShader( type );
	Shader->Type = type;

	glShaderSource( Shader->ID, 1, codes, NULL );
	glCompileShader( Shader->ID );
	glGetShaderiv( Shader->ID, GL_COMPILE_STATUS, &(Shader->Built) );

	if ( !(Shader->Built) )
	{
		DebugGpuShader( Shader, path );
		return -1;
	}

	return 0;
}

int WantGpuShader( BLOCK *Shaders, BLOCKS *Blocks, uch const *path, uint type )
{
	GPU_SHADER *shaders, *Shader;
	int err, used;

	if ( !Shaders )
		return ENONET;

	used = Shaders->used;
	shaders = Shaders->addr;

	if ( used >= Shaders->have )
	{
		err = GrowBlock( Shaders, sizeof(GPU_SHADER), used + 2 );

		if ( err )
			return err;

		shaders = Shaders->addr;
	}

	Shader = shaders + Shaders->used;

	err = InitGpuShader( Shader, Blocks );

	if ( err )
		return err;

	err = MakeGpuShader( Shader, path, type );

	if ( err )
		return err;

	Shaders->used++;

	return 0;
}
