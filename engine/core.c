#include "core.h"

void EchoVec2( FILE *out, vec2 x )
{
	fprintf( out, "{ %+f, %+f }", x[0], x[1] );
}

void EchoVec3( FILE *out, vec3 x )
{
	fprintf( out, "{ %+f, %+f, %+f }", x[0], x[1], x[2] );
}

void EchoVec4( FILE *out, vec4 x )
{
	fprintf( out, "{ %+f, %+f, %+f, %+f }", x[0], x[1], x[2], x[3] );
}



void FreeCaches( BLOCKS *Blocks, int *CacheIDs, int count )
{
	int i;

	if ( count < 1 || count > CACHE_COUNT )
		return;

	for ( i = 0; i < count; ++i )
		FreeBlock( Blocks, CacheIDs[i] );

	memset( CacheIDs, 0, sizeof(int) * count );
}

int WantCaches( BLOCKS *Blocks, int *CacheIDs )
{
	int i;

	for ( i = 0; i < CACHE_COUNT; ++i )
	{
		int *ID = CacheIDs + i;
		int err = WantBlock( Blocks, ID, sizeof(float), 4 );

		if ( err )
		{
			FILE *out = Blocks->Alloc->out;
			ECHO( out, ECHO_ERR( out, err ) );
			FreeCaches( Blocks, CacheIDs, i + 1 );
			return err;
		}
	}

	return 0;
}

BLOCKS *	GrabCoreBlocks( CORE *Core )
{
	return GrabCoreImgFile(Core)->Blocks;
}

GFX_STAGE *		GrabCoreStage( CORE *Core )
{
	return GrabGfxStages( GrabCoreGfxHandle( Core ) )->addr;
}

IMG_FILE *	GrabCoreImgFile( CORE *Core )
{
	return GrabGfxImgFile( GrabCoreGfxHandle( Core ) );
}

GFX *		GrabCoreGfxHandle( CORE *Core )
{
	return &(Core->Gfx);
}

dint	InitGfx( GFX *Gfx, BLOCKS *Blocks )
{
	FILE *out = Blocks->Alloc->out;
	dint *BlockIDs = Gfx->BlockIDs;
	dint i, err = InitImgFile( GrabGfxImgFile( Gfx ), Blocks );
	size_t sizes[GFX_BLOCK_COUNT] =
	{
		sizeof(float),
		sizeof(vec2s),
		sizeof(vec3s),
		sizeof(vec4s),
		1,
		sizeof(GFX_STAGE),
		sizeof(GFX_MODEL),
		sizeof(GFX_JOINT),
		sizeof(GFX_STRIP),
		sizeof(GFX_IMAGE)
	};
	typedef dint (*initGfx_cb)( GFX *Gfx );
	initGfx_cb initGfx[GFX_BLOCK_COUNT] =
	{
		InitGfxSolos,
		InitGfxPairs,
		InitGfxTrios,
		InitGfxQuads,
		InitGfxBuffer,
		InitGfxStages,
		InitGfxModels,
		InitGfxJoints,
		InitGfxStrips,
		InitGfxImages
	};

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfx( Gfx );
		return err;
	}

	ENUM_CACHE * MapTrait = Gfx->MapTrait;

	for ( i = 0; i < TRAIT_COUNT; ++i )
		MapTrait[i] = CACHE_TRIOS;

	MapTrait[TRAIT_COLOR] = CACHE_QUADS;
	MapTrait[TRAIT_TEXEL] = CACHE_PAIRS;

	memcpy( Gfx->MapSizes, sizes, sizeof(sizes) );

	for ( i = 0; i < GFX_BLOCK_COUNT; ++i )
	{
		err = WantBlock( Blocks, BlockIDs + i, sizes[i], BUFSIZ / sizes[i] );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			TermGfx( Gfx );
			return err;
		}

		err = initGfx[i]( Gfx );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			TermGfx( Gfx );
			return err;
		}
	}

	return 0;
}

dint WantGpuGroups( uint *list, uint count )
{
	glX( NULL, glGenVertexArrays( count, list ) );
	return 0;
}

dint WantGpuGroup( uint *id )
{
	return WantGpuGroups( id, 1 );
}

void BindGpuGroup( uint id )
{
	glX( NULL, glBindVertexArray( id ));
}

void FreeGpuGroups( uint *list, uint count )
{
	glX( NULL, glDeleteVertexArrays( count, list ) );
}

void FreeGpuGroup( uint id )
{
	FreeGpuGroups( &id, 1 );
}

dint WantGpuBlocks( uint *list, uint count )
{
	glX( NULL, glGenBuffers( count, list ) );
	return 0;
}

dint WantGpuBlock( uint *id )
{
	return WantGpuBlocks( id, 1 );
}

void FreeGpuBlocks( uint *list, uint count )
{
	glX( NULL, glDeleteBuffers( count, list ) );
}

void FreeGpuBlock( uint id )
{
	FreeGpuBlocks( &id, 1 );
}

void BindGpuBlock( uint type, uint vbo )
{
	glX( NULL, glBindBuffer( type, vbo ) );
}

void BindGpuCache( uint type, uint vbo, uint loc, ENUM_CACHE f )
{
	BindGpuBlock( type, vbo );
	glX( NULL, glVertexAttribPointer( loc, ++f, GL_FLOAT, GL_FALSE, 0, NULL ));
}

void SendGpuBlock( uint type, uint vbo, BLOCK *Block, uint DrawAs )
{
	BindGpuBlock( type, vbo );
	glX( NULL, glBufferData( type, Block->size, Block->addr, DrawAs ));
}

dint WantGpuImages( uint *list, uint count )
{
	glX( NULL, glGenTextures( count, list ) );
	return 0;
}

dint WantGpuImage( uint *id )
{
	return WantGpuImages( id, 1 );
}

void FreeGpuImages( uint *list, uint count )
{
	glX( NULL, glDeleteTextures( count, list ) );
}

void FreeGpuImage( uint id )
{
	FreeGpuImages( &id, 1 );
}

int InitCore( CORE *Core, BLOCKS *Blocks )
{
	dint err;
	GFX *Gfx = GrabCoreGfxHandle( Core );
	FILE *out = Blocks->Alloc->out;

	Gfx->WantVaoGroups = WantGpuGroups;
	Gfx->FreeVaoGroups = FreeGpuGroups;
	Gfx->WantVaoGroup = WantGpuGroup;
	Gfx->FreeVaoGroup = FreeGpuGroup;
	Gfx->BindVaoGroup = BindGpuGroup;

	Gfx->WantVboBlocks = WantGpuBlocks;
	Gfx->FreeVboBlocks = FreeGpuBlocks;
	Gfx->FreeVboBlock = FreeGpuBlock;
	Gfx->WantVboBlock = WantGpuBlock;
	Gfx->BindVboBlock = BindGpuBlock;
	Gfx->SendVboBlock = SendGpuBlock;

	Gfx->WantImgBlocks = WantGpuImages;
	Gfx->FreeImgBlocks = FreeGpuImages;
	Gfx->WantImgBlock = WantGpuImage;
	Gfx->FreeImgBlock = FreeGpuImage;

	Gfx->BindVboCache = BindGpuCache;

	Core->GpuBinary = &(Core->GpuBinary1);
	err = InitGfx( Gfx, Blocks );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermCore( Core );
		return err;
	}

	err = NameGpuConfig( Core );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermCore( Core );
		return err;
	}

	err = InitGfxStage( GrabCoreStage( Core ), Gfx );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermCore( Core );
		return err;
	}

	return 0;
}

void TermGfx( GFX *Gfx )
{
	dint i;
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	typedef void (*termGfx_cb)( GFX *Gfx );
	termGfx_cb termGfx[GFX_BLOCK_COUNT] =
	{
		TermGfxSolos,
		TermGfxPairs,
		TermGfxTrios,
		TermGfxQuads,
		TermGfxBuffer,
		TermGfxStages,
		TermGfxModels,
		TermGfxJoints,
		TermGfxStrips,
		TermGfxImages
	};

	for ( i = GFX_BLOCK_COUNT; i; )
	{
		termGfx[--i]( Gfx );
		FreeBlock( Blocks, Gfx->BlockIDs[i] );
		Gfx->BlockIDs[i] = -1;
	}

	TermImgFile( GrabGfxImgFile( Gfx ) );
}

void TermCore( CORE *Core )
{
	BLOCKS *Blocks = GrabCoreBlocks(Core);

	if ( Blocks )
	{
		if ( Core->GpuBinary->Built )
			FreeGpuIDs( Core );

		TermGpuBinary( &(Core->GpuBinary1) );
		TermGpuBinary( &(Core->GpuBinary2) );

		if ( Core->TermWindow )
			Core->TermWindow( Core->Window );
	}

	memset( Core, 0, sizeof(CORE) );
}

dint WantGpuIDs( CORE *Core )
{
	dint i, err;
	GFX	* Gfx = GrabCoreGfxHandle( Core );
	GPU_TRAITS *GpuTraits = &(Core->GpuTraits);
	BLOCK *Buffer, *Strips, *Images;
	uint *VboList;

	Gfx->WantVaoGroup( &(Gfx->CacheVao) );
	Gfx->WantVboBlocks( Gfx->CacheVBOs, CACHE_COUNT );

	for ( i = 0; i < CACHE_COUNT; ++i )
		Gfx->BindVboBlock( GL_ARRAY_BUFFER, Gfx->CacheVBOs[i] );

	Gfx->WantVboBlock( &(GpuTraits->Vbo) );

	for ( i = 0; i < TRAIT_COUNT; ++i )
	{
		ENUM_CACHE cache = Gfx->MapTrait[i];
		uint CacheVBO = Gfx->CacheVBOs[cache];
		uint TraitVBO = GpuTraits->Vbo;

		Gfx->BindVaoGroup( Gfx->CacheVao );
		Gfx->BindVboBlock( GL_ARRAY_BUFFER, CacheVBO );
		Gfx->BindVboBlock( GL_ELEMENT_ARRAY_BUFFER, TraitVBO );
		glX( NULL, glVertexArrayElementBuffer( Gfx->CacheVao, CacheVBO ));
	}

	GpuTraits->Del = true;

	Strips = GrabGfxStrips( Gfx );
	Buffer = GrabGfxBuffer( Gfx );
	Buffer->used = 0;
	GrowBlock( Buffer, sizeof(uint), Strips->used );
	VboList = Buffer->addr;
	Gfx->WantVboBlocks( VboList, Strips->used );

	for ( i = 1; i < Strips->used; ++ i )
	{
		GFX_STRIP *Strip = GrabGfxStrip( Gfx, i );

		Strip->Vbo = VboList[i];
		Strip->FreeVbo = true;

		Gfx->BindVboBlock( GL_ARRAY_BUFFER, VboList[i] );
	}

	Gfx->FreeVboBlock( VboList[0] );

	Images = GrabGfxImages( Gfx );

	for ( i = 1; i < Images->used; ++ i )
	{
		GFX_IMAGE *Image = GrabGfxImage( Gfx, i );

		err = WantGfxImageGpuId( Image, GL_TEXTURE_2D, GL_NEAREST );

		if ( err )
		{
			FILE *out = Images->Alloc->out;
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	return 0;
}

dint WantGfxBriefChunk( GFX * Gfx, dint *I )
{
	(void)Gfx;
	*I = -1;
	return ENOSYS;
}

dint WantGfxChunk( GFX *Gfx, ENUM_GFX_BLOCKS i, dint *J )
{
	typedef dint (*WantGfxChunk_cb)( GFX *Gfx, dint *I );
	WantGfxChunk_cb WantGfxChunkCBs[GFX_BLOCK_COUNT] = {NULL};

	WantGfxChunkCBs[CACHE_SOLOS] = WantGfxSolo;
	WantGfxChunkCBs[CACHE_PAIRS] = WantGfxPair;
	WantGfxChunkCBs[CACHE_TRIOS] = WantGfxTrio;
	WantGfxChunkCBs[CACHE_QUADS] = WantGfxQuad;
	WantGfxChunkCBs[GFX_BLOCK_BUFFER] = WantGfxBriefChunk;
	WantGfxChunkCBs[GFX_BLOCK_STAGES] = WantGfxStage;
	WantGfxChunkCBs[GFX_BLOCK_MODELS] = WantGfxModel;
	WantGfxChunkCBs[GFX_BLOCK_JOINTS] = WantGfxJoint;
	WantGfxChunkCBs[GFX_BLOCK_STRIPS] = WantGfxStrip;
	WantGfxChunkCBs[GFX_BLOCK_IMAGES] = WantGfxImage;

	return WantGfxChunkCBs[i]( Gfx, J );
}

void FreeGpuIDs( CORE *Core )
{
	GFX *Gfx = GrabCoreGfxHandle( Core );
	GPU_TRAITS *GpuTraits = &(Core->GpuTraits);

	if ( GpuTraits->Del )
	{
		Gfx->FreeVboBlocks( &(GpuTraits->Vbo), 1 );
		Gfx->FreeVboBlocks( Gfx->CacheVBOs, CACHE_COUNT );
		Gfx->FreeVaoGroups( &(Gfx->CacheVao), TRAIT_COUNT );
	}

	memset( GpuTraits, 0, sizeof(GPU_TRAITS) );
}

BLOCK *GrabGfxBlock( GFX *Gfx, ENUM_GFX_BLOCKS i )
{
	return GrabBlock( GrabGfxBlocks( Gfx ), Gfx->BlockIDs[i] );
}

void TermGfxBuffer( GFX *Gfx ) {}
dint InitGfxBuffer( GFX *Gfx ) { return 0; }
