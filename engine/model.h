#ifndef INC_ENGINE_GFX_MODEL_H
#define INC_ENGINE_GFX_MODEL_H
#include "core_common.h"
#include "strip.h"

struct _GFX_MODEL
{
	uint		id;
	GFX *		Gfx;
	dint		JointIDs;
	uint		Vao;
	uint		FreeVao : 1;
	CHANGE		Change;
};

void	EchoGfxModel(	GFX_MODEL *GfxModel );
void	TermGfxModel(	GFX_MODEL *GfxModel );
dint	InitGfxModel(	GFX_MODEL *GfxModel, GFX *Gfx );
void	GiveGfxModelVao( GFX_MODEL *GfxModel, uint Vao, bool FreeOnTerm );
GFX *	GrabGfxModelGfxHandle( GFX_MODEL *GfxModel );
BLOCK * GrabGfxModelJointIDs( GFX_MODEL *GfxModel );
dint *	GrabGfxModelJointID( GFX_MODEL *GfxModel, dint i );
dint 	WantGfxModelJointID( GFX_MODEL *GfxModel, dint *I );

#endif
