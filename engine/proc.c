#include "core.h"

BLOCK * GrabGpuShaders( GPU_BINARY *GpuBinary )
{
	return GrabBlock( GpuBinary->Blocks, GpuBinary->ShadersID );
}

void TermGpuBinary( GPU_BINARY *GpuBinary )
{
	BLOCKS *Blocks = GpuBinary->Blocks;

	if ( Blocks )
	{
		BLOCK *Shaders = GrabGpuShaders( GpuBinary );

		if ( GpuBinary->Built )
			glDeleteProgram( GpuBinary->ID );

		if ( Shaders )
		{
			int i;
			GPU_SHADER *shaders = Shaders->addr;
			for ( i = 0; i < Shaders->used; ++i )
				TermGpuShader( shaders + i );
		}

		FreeBlock( Blocks, GpuBinary->ShadersID );
	}

	memset( GpuBinary, 0, sizeof(GPU_BINARY) );
}

int InitGpuBinary( GPU_BINARY *GpuBinary, BLOCKS *Blocks )
{
	int err;

	GpuBinary->Blocks = Blocks;
	err = WantBlock( Blocks, &(GpuBinary->ShadersID), sizeof(GPU_SHADER), 2 );

	if ( err )
	{
		FILE *out = Blocks->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		TermGpuBinary( GpuBinary );
		return err;
	}

	GpuBinary->ID = glCreateProgram();

	return 0;
}



void   DebugGpuBinary( GPU_BINARY *GpuBinary )
{
	BLOCKS *Blocks = GpuBinary->Blocks;
	char Temp[BUFSIZ] = {0};
	ANSI *Log;
	char *log;
	int err, LogID, len = 0;

	glGetProgramiv( GpuBinary->ID, GL_INFO_LOG_LENGTH, &len );
	err = WantBlock( Blocks, &LogID, 1, len + 1 );

	if ( err )
	{
		len = BUFSIZ - 1;
		log = Temp;
	}
	else
	{
		Log = GrabBlock( Blocks, LogID );
		log = Log->addr;
	}

	glGetProgramInfoLog( GpuBinary->ID, len, NULL, log );
	log[len] = 0;

	fprintf( errout, "[OPENGL GPU_BINARY ERROR]: \"%s\"\n", log );
#ifdef _DEBUG
	assert(false);
#endif

	if ( LogID > 0 )
		FreeBlock( Blocks, LogID );
}

int MakeGpuBinary( GPU_BINARY *GpuBinary )
{
	BLOCK *Shaders = GrabBlock( GpuBinary->Blocks, GpuBinary->ShadersID );
	GPU_SHADER *shaders = Shaders->addr;

	for ( uint task = 0; task < Shaders->used; ++task )
		glAttachShader( GpuBinary->ID, shaders[task].ID );

	glLinkProgram( GpuBinary->ID );
	glGetProgramiv( GpuBinary->ID, GL_LINK_STATUS, &(GpuBinary->Built) );
	glValidateProgram( GpuBinary->ID );
	glGetProgramiv( GpuBinary->ID, GL_VALIDATE_STATUS, &(GpuBinary->Valid) );

	if ( !(GpuBinary->Built) || !(GpuBinary->Valid) )
	{
		DebugGpuBinary( GpuBinary );
		return -1;
	}

	return 0;
}
