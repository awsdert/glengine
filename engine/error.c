#include "core_common.h"

void glClearErrors()
{
	while ( glGetError() );
}
bool glHadNoErrors
(
	void *ud,
	char const * const path,
	uint line,
	char const * const call
)
{
	int fault = glGetError();

	if ( fault )
	{
		LECHO
		(
			path, line, errout,
			fprintf( errout, "%s\n\t%s\n", call, glReportToString(fault) )
		);
		return false;
	}

	return true;
}
void GLAPIENTRY glDebugMsgCB
(
	GLenum source
	, GLenum report
	, GLuint id
	, GLenum weight
	, GLsizei length
	, const GLchar *msg
	, const void *ud
)
{
	fprintf
	(
		stderr
		, "%s (%u) source %s, weight, %s:\n\n%s\n"
		, glReportToString( report )
		, id
		, glSourceToString( source )
		, glWeightToString( weight )
		, msg
	);
}

char const * glSourceToString( GLenum source )
{
	switch ( source )
	{
	case GL_DEBUG_SOURCE_API:			return "GL_DEBUG_SOURCE_API";
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:	return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		return "GL_DEBUG_SOURCE_SHADER_COMPILER";
	case GL_DEBUG_SOURCE_THIRD_PARTY:	return "GL_DEBUG_SOURCE_THIRD_PARTY";
	case GL_DEBUG_SOURCE_APPLICATION:	return "GL_DEBUG_SOURCE_APPLICATION";
	case GL_DEBUG_SOURCE_OTHER:			return "GL_DEBUG_SOURCE_OTHER";
	}

	return "GL_SOURCE_UNKNOWN";
}

char const * glReportToString( GLuint report )
{
	switch ( report )
	{
		case 0:							return "GL_REPORT";
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
		case GL_DEBUG_TYPE_ERROR:		return "GL_DEBUG_TYPE_ERROR";
		case GL_DEBUG_TYPE_MARKER:		return "GL_DEBUG_TYPE_MARKER";
		case GL_DEBUG_TYPE_OTHER:		return "GL_DEBUG_TYPE_OTHER";
		case GL_DEBUG_TYPE_PERFORMANCE:	return "GL_DEBUG_TYPE_PERFORMANCE";
		case GL_DEBUG_TYPE_PORTABILITY:	return "GL_DEBUG_TYPE_PORTABILITY";
		case GL_DEBUG_TYPE_PUSH_GROUP:	return "GL_DEBUG_TYPE_PUSH_GROUP";
		case GL_DEBUG_TYPE_POP_GROUP:	return "GL_DEBUG_TYPE_POP_GROUP";
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
		case GL_CONTEXT_LOST:			return "GL_CONTEXT_LOST";
		case GL_INVALID_ENUM:			return "GL_INVALID_ENUM";
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case GL_INVALID_OPERATION:		return "GL_INVALID_OPERATION";
		case GL_INVALID_VALUE:			return "GL_INVALID_VALUE";
		case GL_STACK_OVERFLOW:			return "GL_STACK_OVERFLOW";
		case GL_STACK_UNDERFLOW:		return "GL_STACK_UNDERFLOW";
		case GL_OUT_OF_MEMORY:			return "GL_OUT_OF_MEMORY";
		case GL_TABLE_TOO_LARGE:		return "GL_TABLE_TOO_LARGE";
	}

	return "GL_ERROR_UNKNOWN";
}

char const * glWeightToString( GLenum weight )
{
	switch ( weight )
	{
	case GL_DEBUG_SEVERITY_HIGH:		return "GL_DEBUG_SEVERITY_HIGH";
	case GL_DEBUG_SEVERITY_MEDIUM:		return "GL_DEBUG_SEVERITY_MEDIUM";
	case GL_DEBUG_SEVERITY_LOW:			return "GL_DEBUG_SEVERITY_LOW";
	case GL_DEBUG_SEVERITY_NOTIFICATION:return "GL_DEBUG_SEVERITY_NOTIFICATION";
	}

	return "GL_SEVERITY_UNKNOWN";
}

char const * glPursueToString( GLuint pursue )
{
	switch ( pursue )
	{
	case GL_BUFFER:						return "GL_BUFFER";
	case GL_SHADER:						return "GL_SHADER";
	case GL_PROGRAM:					return "GL_PROGRAM";
	case GL_VERTEX_ARRAY:				return "GL_VERTEX_ARRAY";
	case GL_QUERY:						return "GL_QUERY";
	case GL_PROGRAM_PIPELINE:			return "GL_PROGRAM_PIPELINE";
	case GL_TRANSFORM_FEEDBACK:			return "GL_TRANSFORM_FEEDBACK";
	case GL_SAMPLER:					return "GL_SAMPLER";
	case GL_TEXTURE:					return "GL_TEXTURE";
	case GL_RENDERBUFFER:				return "GL_RENDERBUFFER";
	case GL_FRAMEBUFFER:				return "GL_FRAMEBUFFER";
	}

	return "GL_ID_UNKOWN";
}
