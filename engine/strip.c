#include "strip.h"

void	EchoGfxHeldPos( GFX *Gfx, POS_HELD *Held );
void	EchoGfxPosTrio( GFX *Gfx, POS_TRIO *Trio );
void	EchoGfxStrip( GFX_STRIP *GfxStrip );
void	EchoGfxJoint( GFX_JOINT *GfxJoint );

GFX * GrabGfxJointGfxHandle( GFX_JOINT *GfxJoint )
{
	return GfxJoint->Gfx;
}

BLOCKS *GrabGfxJointBlocks( GFX_JOINT *GfxJoint )
{
	return GrabGfxBlocks( GrabGfxJointGfxHandle( GfxJoint ) );
}

void	TermGfxJoints( GFX *Gfx ){}
dint	InitGfxJoints( GFX *Gfx )
{
	dint err;
	BLOCK *Joints = GrabGfxJoints( Gfx );

	err = GrowBlock( Joints, sizeof(GFX_JOINT), 1 );

	if ( err )
	{
		FILE *out = Joints->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxJoints( Gfx );
		return err;
	}

	Joints->used = 1;

	return 0;
}

void *	GrabGfxChunk( GFX *Gfx, ENUM_GFX_BLOCKS i, dint j )
{
	return GrabChunk( GrabGfxJoints( Gfx ), Gfx->MapSizes[i], j );
}

GFX * GrabGfxStripGfxHandle( GFX_STRIP *GfxStrip )
{
	return GfxStrip->Gfx;
}

void		TermGfxStrip( GFX_STRIP *GfxStrip )
{
	dint i;
	dint *TraitIDs = GfxStrip->TraitIDs;
	GFX *Gfx = GrabGfxStripGfxHandle( GfxStrip );

	if ( GfxStrip->FreeVbo )
		Gfx->FreeVboBlock( GfxStrip->Vbo );

	for ( i = 0; i < TRAIT_COUNT; ++i )
		FreeBlock( GrabGfxBlocks( Gfx ), TraitIDs[i] );

	memset( GfxStrip, 0, sizeof(GFX_STRIP) );
}
dint	InitGfxStrip( GFX_STRIP *GfxStrip, GFX *Gfx )
{
	dint i, err;
	dint *TraitIDs = GfxStrip->TraitIDs;
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	FILE *out = Blocks->Alloc->out;

	GfxStrip->Gfx = Gfx;

	for ( i = 0; i < TRAIT_COUNT; ++i )
	{
		err = WantBlock( Blocks, TraitIDs + i, sizeof(int), 1 );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			TermGfxStrip( GfxStrip );
			return err;
		}
	}

	return 0;
}

dint		WantGfxStrip( GFX *Gfx, dint *I )
{
	BLOCK *Strips = GrabGfxStrips( Gfx );
	FILE *out = Strips->Alloc->out;
	dint i, num = Strips->used;
	dint err = WantChunk( Strips, sizeof(GFX_STRIP), &i, 16 );
	GFX_STRIP *Strip;

	*I = -1;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Strips->used = num;
		return err;
	}

	Strip = GrabGfxStrip( Gfx, i );
	err = InitGfxStrip( Strip, Gfx );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Strips->used = num;
		return err;
	}

	*I = i;
	return 0;
}

dint		WantGfxJoint( GFX *Gfx, dint *I )
{
	BLOCK *Joints = GrabGfxJoints( Gfx );
	FILE *out = Joints->Alloc->out;
	dint i, num = Joints->used;
	dint err = WantChunk( Joints, sizeof(GFX_JOINT), &i, 16 );
	GFX_JOINT *Joint;

	*I = -1;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Joints->used = num;
		return err;
	}

	Joint = GrabGfxJoint( Gfx, i );
	err = InitGfxJoint( Joint, Gfx );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Joints->used = num;
		return err;
	}

	*I = i;
	return 0;
}

dint		 GiveGfxStripPos( GFX_STRIP *GfxStrip, POS_GIVE *Pos )
{
	dint i, num = GfxStrip->count;
	dint *TraitIDs = GfxStrip->TraitIDs;
	BLOCKS *Blocks = GrabGfxBlocks( GrabGfxStripGfxHandle( GfxStrip ) );
	size_t need;

	num = (num > 0) ? num : 0;
	++num;

	need = num * sizeof(dint);

	if ( GfxStrip->bytes < need )
	{
		dint get = ((num / 32) + 1) * 32;

		for ( i = 0; i < TRAIT_COUNT; ++i )
		{
			BLOCK *Indices = GrabBlock( Blocks, TraitIDs[i] );
			dint err = GrowBlock( Indices, sizeof(dint), get );

			if ( err )
			{
				FILE *out = Blocks->Alloc->out;
				ECHO( out, ECHO_ERR( out, err ) );
				return err;
			}
		}

		GfxStrip->total = get;
		GfxStrip->bytes = get * sizeof(dint);
	}

	for ( i = 0; i < TRAIT_COUNT; ++i )
	{
		BLOCK *Indices = GrabBlock( Blocks, TraitIDs[i] );
		int *indices = Indices->addr;
		indices[num] = Pos->raw[i];
		Indices->used = num + 1;
	}

	GfxStrip->count = num + 1;
	return 0;
}

dint	GiveGfxStripPositions( GFX_STRIP *GfxStrip, POS_GIVE *Give, dint num )
{
	dint i;

	for ( i = 0; i < num; ++i )
	{
		dint err = GiveGfxStripPos( GfxStrip, Give + i );

		if ( err )
		{
			GFX *Gfx = GrabGfxStripGfxHandle( GfxStrip );
			FILE *out = GrabGfxBlocks( Gfx )->Alloc->out;
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	return 0;
}

dint		GiveGfxStripQuad( GFX_STRIP *GfxStrip, POS_QUAD *Quad )
{
	return GiveGfxStripPositions( GfxStrip, Quad->raw, 4 );
}

dint		GiveGfxStripTrio( GFX_STRIP *GfxStrip, POS_TRIO *Trio )
{
	return GiveGfxStripPositions( GfxStrip, Trio->raw, 3 );
}

POS_HELD	 GrabPosHeld( GFX_STRIP *GfxStrip, int i )
{
	POS_HELD Pos = {{NULL}};

	if ( GfxStrip->count > i )
	{
		dint j;
		dint *TraitIDs = GfxStrip->TraitIDs;
		BLOCKS *Blocks = GrabGfxBlocks( GrabGfxStripGfxHandle( GfxStrip ) );

		for ( j = 0; j < TRAIT_COUNT; ++j )
		{
			BLOCK *Indices = GrabBlock( Blocks, TraitIDs[i] );
			int *indices = Indices->addr;
			Pos.raw[j] = indices + i;
		}
	}

	return Pos;
}
float *		 GrabGfxFloats( GFX *Gfx, ENUM_CACHE i, int j )
{
	BLOCK *Floats = GrabBlock( GrabGfxBlocks( Gfx ), Gfx->BlockIDs[i] );
	float *floats = Floats->addr;
	return floats + (j * ++i);
}

float *		GrabToldFloats(	GFX *Gfx, POS_GIVE *Told, ENUM_TRAIT i )
{
	return GrabGfxFloats( Gfx, Gfx->MapTrait[i], Told->raw[i] );
}
float *		GrabHeldFloats(	GFX *Gfx, POS_HELD *Held, ENUM_TRAIT i )
{
	return GrabGfxFloats( Gfx, Gfx->MapTrait[i], *(Held->raw[i]) );
}

void	TermGfxJoint( GFX_JOINT *GfxJoint )
{
	memset( GfxJoint, 0, sizeof(GFX_JOINT) );
}

dint	InitGfxJoint( GFX_JOINT *GfxJoint, GFX *Gfx )
{
	GfxJoint->StripID = -1;
	GfxJoint->ImageID = -1;
	GfxJoint->Gfx = Gfx;
	return 0;
}

void	GiveGfxJointStripID( GFX_JOINT *GfxJoint, dint GfxStripID )
{
	GfxJoint->StripID = GfxStripID;
	GfxJoint->GaveStripID = (GfxStripID > 0);
}

void	 GiveGfxJointCoverID( GFX_JOINT *GfxJoint, dint GfxImageID )
{
	GfxJoint->ImageID = GfxImageID;
	GfxJoint->GaveImageID = (GfxImageID > 0);
}

BLOCK *		GrabGfxStripBlock( GFX_STRIP *GfxStrip, ENUM_TRAIT i )
{
	return GrabBlock
	(
		GrabGfxBlocks( GrabGfxStripGfxHandle( GfxStrip ) ),
		GfxStrip->TraitIDs[i]
	);
}

POS_HELD	GrabGfxStripPos( GFX_STRIP *GfxStrip, dint i )
{
	BLOCKS *Blocks = GrabGfxBlocks( GrabGfxStripGfxHandle( GfxStrip ) );
	POS_HELD Held = {{NULL}};

	if ( i >= 0 && GfxStrip->count > i )
	{
		dint j;

		for ( j = 0; j < TRAIT_COUNT; ++j )
		{
			BLOCK *Indices = GrabBlock( Blocks, GfxStrip->TraitIDs[j] );
			int *indices = Indices->addr;
			Held.raw[j] = indices + i;
		}
	}

	return Held;
}

dint	GrabGfxJointStripID( GFX_JOINT *GfxJoint ) { return GfxJoint->StripID; }
dint	GrabGfxJointCoverID( GFX_JOINT *GfxJoint ) { return GfxJoint->ImageID; }

void	TermGfxStrips( GFX *Gfx )
{
	BLOCK *Strips = GrabGfxStrips( Gfx );

	if ( Strips )
	{
		dint i;
		GFX_STRIP *strips = Strips->addr;

		for ( i = 1; i < Strips->used; ++i )
			TermGfxStrip( strips + i );
	}
}

dint	InitGfxStrips( GFX *Gfx )
{
	BLOCK *Strips = GrabGfxStrips( Gfx );
	FILE *out = Strips->Alloc->out;
	dint err = GrowBlock( Strips, sizeof(GFX_STRIP), 1 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxStrips( Gfx );
		return err;
	}

	Strips->used = 1;

	return 0;
}

dint WantGfxStripGpuId( GFX_STRIP *GfxStrip )
{
	GFX *Gfx = GrabGfxStripGfxHandle( GfxStrip );
	Gfx->WantVboBlock( &(GfxStrip->Vbo ) );
	return 0;
}
