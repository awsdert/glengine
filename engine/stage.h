#ifndef INC_ENGINE_STAGE_H
#define INC_ENGINE_STAGE_H
#include "core_common.h"
#include "model.h"

struct _GFX_STAGE
{
	uint	id;
	GFX	*	Gfx;
	int		StageIDs;
	int		ModelIDs;
	int		Parent;
	ivec3	Bounds;
	CHANGE	Change;
};

void		TermGfxStage( GFX_STAGE *Stage );
int			InitGfxStage( GFX_STAGE *Stage, GFX *Gfx );
BLOCK *		GrabGfxStageStageIDs( GFX_STAGE *Stage );
BLOCK *		GrabGfxStageModelIDs( GFX_STAGE *Stage );
dint *		GrabGfxStageStageID( GFX_STAGE *Stage, int i );
dint *		GrabGfxStageModelID( GFX_STAGE *Stage, int i );
dint		WantGfxStageStageID( GFX_STAGE *Stage, int *I );
dint		WantGfxStageModelID( GFX_STAGE *Stage, int *I );

#endif
