#include "core.h"

void	EchoFloats( FILE *out, float *floats, ENUM_CACHE count )
{
	int i;
	fputc( '{', out );
	for ( i = 0; i < count; ++i )
		fprintf( out, " %f", floats[i] );
	fputs( " }\n", out );
}

void	TermGfxSolos( GFX *Gfx ) {}
void	TermGfxPairs( GFX *Gfx ) {}
void	TermGfxTrios( GFX *Gfx ) {}
void	TermGfxQuads( GFX *Gfx ) {}

dint	InitGfxSolos( GFX *Gfx ) { return 0; }
dint	InitGfxPairs( GFX *Gfx ) { return 0; }
dint	InitGfxTrios( GFX *Gfx ) { return 0; }
dint	InitGfxQuads( GFX *Gfx ) { return 0; }


dint FindGfxFloats( GFX *Gfx, ENUM_CACHE cache, float const *src )
{
	BLOCK *Floats = GrabGfxBlock( Gfx, cache );
	int i, num = cache + 1;
	float *floats = Floats->addr;
	size_t size = num * sizeof(float);

	for ( i = 0; Floats->used > i; ++i )
	{
		if ( memcmp( floats + (i * num), src, size ) == 0 )
			return i;
	}

	return -1;
}

int GiveGfxFloats( GFX *Gfx, ENUM_CACHE cache, int *I, float const *src )
{
	BLOCK *Floats = GrabGfxBlock( Gfx, cache );
	int i, num = cache + 1;
	float *floats = Floats->addr;
	size_t size = num * sizeof(float);

	if ( i >= 0 )
	{
		*I = i;
		return 0;
	}

	*I = -1;
	i = Floats->used;

	if ( Floats->have <= i )
	{
		int err = WantChunk( Floats, size, &i, BUFSIZ );

		if ( err )
		{
			FILE *out = Floats->Alloc->out;
			fputs( "Not enough floats available\n", out );
			return err;
		}

		floats = Floats->addr;
	}

	memcpy( floats + (i * num), src, size );
	Floats->used = i + 1;
	*I = i;
	return 0;
}

dint WantGfxFloats( GFX *Gfx, ENUM_CACHE i, dint *J )
{
	size_t size = Gfx->MapSizes[i];
	return WantChunk( GrabGfxBlock( Gfx, i ), size, J, BUFSIZ / size );
}

dint WantGfxSolo( GFX *Gfx, dint *I )
{
	return WantGfxFloats( Gfx, CACHE_SOLOS, I );
}

dint WantGfxPair( GFX *Gfx, dint *I )
{
	return WantGfxFloats( Gfx, CACHE_PAIRS, I );
}

dint WantGfxTrio( GFX *Gfx, dint *I )
{
	return WantGfxFloats( Gfx, CACHE_TRIOS, I );
}

dint WantGfxQuad( GFX *Gfx, dint *I )
{
	return WantGfxFloats( Gfx, CACHE_QUADS, I );
}
