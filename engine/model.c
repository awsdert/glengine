#include "model.h"
#include "strip.h"

void EchoGfxModel( GFX_MODEL *GfxModel )
{
	GFX *Gfx = GrabGfxModelGfxHandle( GfxModel );
	BLOCK *JointIDs = GrabGfxModelJointIDs( GfxModel );
	dint i, *jointIDs = JointIDs->addr;
	FILE *out = JointIDs->Alloc->out;

	for ( i = 0; i < JointIDs->used; ++i )
	{
		GFX_JOINT *Joint = GrabGfxJoint( Gfx, jointIDs[i] );
		GFX_STRIP *Strip = GrabGfxStrip( Gfx, GrabGfxJointStripID( Joint ) );
		dint j;

		ECHO( out, fprintf( out, "GfxModel->Joints[%3u]\n", i ) );

		for ( j = 0; j < Strip->count; ++j )
		{
			POS_HELD Pos = GrabGfxStripPos( Strip, j );
			float *Place = GrabHeldFloats( Gfx, &Pos, TRAIT_PLACE );
			float *Color = GrabHeldFloats( Gfx, &Pos, TRAIT_COLOR );

			if ( !Place || !Color )
			{
				ECHO( out, ECHO_ERR( out, ENODATA ) );
				continue;
			}

			fprintf
			(
				out,
				"Joint->Points[%d] "
				"Place = %4d [%+f, %+f, %+f]; "
				"Color = %4d [%+f, %+f, %+f, %+f]\n",
				j, *(Pos.Place), Place[0], Place[1], Place[2],
				*(Pos.Color), Color[0], Color[1], Color[2], Color[3]
			);
		}
	}
}

GFX * GrabGfxModelGfxHandle( GFX_MODEL *GfxModel )
{
	return GfxModel->Gfx;
}

BLOCK * GrabGfxModelJointIDs( GFX_MODEL *GfxModel )
{
	return GrabGfxJoints( GrabGfxModelGfxHandle( GfxModel ) );
}

dint *	GrabGfxModelJointID( GFX_MODEL *GfxModel, int i )
{
	return GrabChunk( GrabGfxModelJointIDs( GfxModel ), sizeof(dint), i );
}

int InitGfxModel( GFX_MODEL *GfxModel, GFX *Gfx )
{
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	dint err = WantBlock( Blocks, &(GfxModel->JointIDs), sizeof(dint), 0 );

	GfxModel->Gfx = Gfx;

	if ( err )
	{
		FILE *out = Blocks->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxModel( GfxModel );
		return err;
	}

	return 0;
}

void TermGfxModel( GFX_MODEL *GfxModel )
{
	GFX *Gfx = GrabGfxModelGfxHandle( GfxModel );
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );

	if ( GfxModel->FreeVao )
		Gfx->FreeVaoGroup( GfxModel->Vao );

	FreeBlock( Blocks, GfxModel->JointIDs );

	memset( GfxModel, 0, sizeof(GFX_MODEL) );
}

dint WantGfxModelJointID( GFX_MODEL *GfxModel, dint *I )
{
	return WantChunk( GrabGfxModelJointIDs( GfxModel ), sizeof(dint), I, 16 );
}

dint GrabGfxJointImageID( GFX_JOINT *GfxJoint ) { return GfxJoint->ImageID; }
void GiveGfxJointImageID( GFX_JOINT *GfxJoint, dint ImageID )
{
	GfxJoint->ImageID = ImageID;
	GfxJoint->GaveImageID = (ImageID > 0);
}

dint WantGfxModel( GFX *Gfx, dint *I )
{
	BLOCK *Models = GrabGfxModels( Gfx );
	FILE *out = Models->Alloc->out;
	dint i, num = Models->used;
	dint err = WantChunk( Models, sizeof(GFX_MODEL), &i, 16 );
	GFX_MODEL *Model;

	*I = -1;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Models->used = num;
		return err;
	}

	Model = GrabGfxModel( Gfx, i );
	err = InitGfxModel( Model, Gfx );

	if ( err )
	{
		/* Just in case the address changed during the initialisation attempt */
		Models = GrabGfxModels( Gfx );
		ECHO( out, ECHO_ERR( out, err ) );
		Models->used = num;
		return err;
	}

	*I = i;

	return 0;
}

void TermGfxModels( GFX *Gfx )
{
	BLOCK *Models = GrabGfxModels( Gfx );
	GFX_MODEL *models = Models->addr;
	dint i;

	for ( i = 0; i < Models->used; ++i )
		TermGfxModel( models + i );
}

dint InitGfxModels( GFX *Gfx )
{
	BLOCK *Models = GrabGfxModels( Gfx );
	dint err = GrowBlock( Models, sizeof(GFX_MODEL), 1 );

	if ( err )
	{
		FILE *out = Models->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	Models->used = 1;

	return 0;
}
