#ifndef INC_ENGINE_GFX_JOINT_H
#define INC_ENGINE_GFX_JOINT_H
#include "core_common.h"

typedef union _POS_GIVE
{
	int raw[TRAIT_COUNT];
	struct
	{
		int Texel;
		int Color;
		int Place;
		int Normal;
		int Tangent;
		int BiNormal;
	};
} POS_GIVE;

typedef union _POS_HELD
{
	int *raw[TRAIT_COUNT];
	struct
	{
		int *Texel;
		int *Color;
		int *Place;
		int *Normal;
		int *Tangent;
		int *BiNormal;
	};
} POS_HELD;

/* I find I tend to mis-type triangle as triangle a lot, trigon is a synonym
 * for triangle and is easier to type right the first time, would use trio but
 * I use Trios instead of Vec3s as A. it sounds better and B. Is less likely
 * to be confused with the vec3s of cglm, as a nice bonus it happens to have
 * the same character count as 'sphere' so it lines up nicely when coding */
typedef union _POS_TRIO
{
	POS_GIVE raw[3];
	struct
	{
		POS_GIVE a;
		POS_GIVE b;
		POS_GIVE c;
	};
} POS_TRIO;

typedef union _POS_QUAD
{
	POS_GIVE raw[4];
	struct
	{
		POS_GIVE a;
		POS_GIVE b;
		POS_GIVE c;
		POS_GIVE d;
	};
} POS_QUAD;

typedef void (*RngSeed_cb)( void *ud );
typedef uint (*RngNext_cb)( void *ud );

typedef struct _RNG_NEXT
{
	void *ud;
	RngSeed_cb Seed;
	RngNext_cb Next;
} RNG_NEXT;

typedef struct _RNG
{
	u32 Rows;
	u32 Cols;
	union
	{
		RNG_NEXT raw[4];
		struct
		{
			RNG_NEXT r;
			RNG_NEXT g;
			RNG_NEXT b;
			RNG_NEXT a;
		};
	};
} RNG;

struct _GFX_IMAGE
{
	GFX	*	Gfx;
	IMG		Img;
	RNG		Rng;
	uint	FreeVbo : 1;
	uint	Vbo;
	uint	UsedAs;
	uint	Filter;
	uint	MipMap;
};

struct _GFX_STRIP
{
	uint	id;
	GFX	*	Gfx;
	size_t	bytes;
	dint	count;
	dint	total;
	dint	TraitIDs[TRAIT_COUNT];
	uint	Vbo;
	uint	FreeVbo : 1;
	uint	DrawAs;
};

struct _GFX_JOINT
{
	uint	id;
	GFX	*	Gfx;
	dint	ImageID;
	dint	StripID;
	uint	GaveStripID : 1;
	uint	GaveImageID : 1;
	CHANGE	Change;
};


void		EchoGfxPos(		GFX *GfxJoints, POS_HELD *PosHeld );
void		EchoGfxPoss(	GFX *GfxJoints, GFX_STRIP *GfxStrip );
void		EchoGfxTrio(	GFX *GfxJoints, POS_TRIO *GfxTrio );
void		EchoGfxQuad(	GFX *GfxJoints, POS_QUAD *GfxQuad );

dint		FindImgGfxImage( GFX *Gfx, uch const *path );
dint		WantRngGfxImage( GFX *Gfx, dint *I, RNG *Rng );
dint		WantImgGfxImage( GFX *Gfx, dint *I, uch const *path );

void		DrawRngGfxImage( GFX_IMAGE *GfxImage );

float *		GrabToldFloats(	GFX *Gfx, POS_GIVE *Told, ENUM_TRAIT i );
float *		GrabHeldFloats(	GFX *Gfx, POS_HELD *Held, ENUM_TRAIT i );

void		TermGfxStrip(	GFX_STRIP *GfxStrip );
dint		InitGfxStrip(	GFX_STRIP *GfxStrip, GFX *Gfx );
dint		GiveGfxStripPos(	GFX_STRIP *GfxStrip, POS_GIVE *Give );
dint		GiveGfxStripPositions( GFX_STRIP *GfxStrip, POS_GIVE *Give, int num );
dint		GiveGfxStripTrio( GFX_STRIP *GfxStrip, POS_TRIO *Trio );
dint		GiveGfxStripQuad( GFX_STRIP *GfxStrip, POS_QUAD *Quad );
BLOCK *		GrabGfxStripBlock( GFX_STRIP *GfxStrip, ENUM_TRAIT i );
POS_HELD	GrabGfxStripPos(	GFX_STRIP *GfxStrip, dint i );

void		TermGfxJoint(		GFX_JOINT *GfxJoint );
dint		InitGfxJoint(		GFX_JOINT *GfxJoint, GFX *Gfx );
POS_HELD	GrabGfxJointPos(	GFX_JOINT *GfxJoint, dint i );
dint		GrabGfxJointStripID( GFX_JOINT *GfxJoint );
dint		GrabGfxJointImageID( GFX_JOINT *GfxJoint );
void		GiveGfxJointStripID( GFX_JOINT *GfxJoint, dint GfxStripID );
void		GiveGfxJointImageID( GFX_JOINT *GfxJoint, dint GfxImageID );


#endif
