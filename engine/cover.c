#include "cover.h"

dint	FindImgGfxImage( GFX *Gfx, uch const *path )
{
	dint i;
	BLOCK *Images = GrabGfxImages( Gfx );
	GFX_IMAGE *images = Images->addr;

	for ( i = 0; i < Images->used; ++i )
	{
		GFX_IMAGE *GfxImage = images + i;

		if ( strcmp( TellImgPath( &(GfxImage->Img) ), path ) == 0 )
			return i;
	}

	return -1;
}

dint	InitGfxImage( GFX_IMAGE *GfxImage, GFX *Gfx, u32 Rows, u32 Cols )
{
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	int err, need = Cols * Rows;
	FILE *out = Blocks->Alloc->out;
	IMG *Img = &(GfxImage->Img);
	RNG *Rng = &(GfxImage->Rng);

	GfxImage->Gfx = Gfx;

	err = InitImg( Img, Blocks );
	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxImage( GfxImage );
		return err;
	}

	err = WantBlock( Blocks, &(Img->PathID), sizeof(uch), 0 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxImage( GfxImage );
		return err;
	}

	err = WantBlock( Blocks, &(Img->ViewID), sizeof(RGBA), need );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxImage( GfxImage );
		return err;
	}

	if ( Cols > 0 && Rows > 0 )
	{
		Img->Cols = Rng->Cols = Cols;
		Img->Rows = Rng->Rows = Rows;
	}

	return 0;
}

void TermGfxImage( GFX_IMAGE *GfxImage )
{
	GFX *Gfx = GrabGfxImageGfxHandle( GfxImage );
	IMG *Img = &(GfxImage->Img);
	BLOCKS *Blocks = Img->Blocks;
	BLOCK *View = GrabBlock( Blocks, Img->ViewID );

	if ( View )
	{
		if ( GfxImage->FreeVbo )
			Gfx->FreeImgBlock( GfxImage->Vbo );

		TermImg( Img );
	}

	memset( GfxImage, 0, sizeof(GFX_IMAGE) );
}

void DrawRngGfxImage( GFX_IMAGE *GfxImage )
{
	IMG *Img = &(GfxImage->Img);
	GFX *Gfx = GrabGfxImageGfxHandle( GfxImage );
	BLOCK *View = GrabBlock( GrabGfxBlocks( Gfx ), Img->ViewID );
	dint i, j;
	RNG *Rng = &(GfxImage->Rng);
	vec4s *vecs = View->addr, *vec;
	double max = UINT_MAX, colors[IMG_FILE_VALUE_COUNT];
	bool gen = false;

	for ( i = 0; i < IMG_FILE_VALUE_COUNT; ++i )
	{
		RNG_NEXT *Sample = Rng->raw + i;

		if ( Sample->Seed )
		{
			gen = true;
			Sample->Seed( Sample->ud );
		}
	}

	if ( !gen )
		return;

	for ( i = 0; i < View->used; ++i )
	{
		vec = vecs + i;

		for ( j = 0; j < IMG_FILE_VALUE_COUNT; ++j )
		{
			RNG_NEXT *Sample = Rng->raw + j;
			colors[j] = Sample->Next( Sample->ud );
			vec->raw[j] = colors[j] / max;
		}
	}
}

void TellGpuOfGfxImage( GFX_IMAGE *GfxImage )
{
	glX( NULL, glActiveTexture( GL_TEXTURE0 ) );
	glX( NULL, glBindTexture( GfxImage->UsedAs, GfxImage->Vbo ) );
}

void SendGfxImageToGpu( GFX_IMAGE *GfxImage )
{
	IMG *Img = &(GfxImage->Img);
	BLOCK *View = GrabBlock( Img->Blocks, Img->ViewID );

	glX( NULL, glTexImage2D
	(
		GfxImage->UsedAs, 0, GL_RGBA,
		Img->Cols, Img->Rows, 0, GL_RGBA,
		GL_FLOAT, View->addr
	));
	glX( NULL, glGenerateMipmap(GfxImage->UsedAs) );
}

dint WantGfxImageGpuId( GFX_IMAGE *GfxImage, uint UsedAs, uint Filter )
{
	GFX *Gfx = GrabGfxImageGfxHandle( GfxImage );

	Gfx->WantImgBlock( &(GfxImage->Vbo) );
	GfxImage->FreeVbo = true;

	GfxImage->UsedAs = UsedAs;
	GfxImage->Filter = Filter;

	TellGpuOfGfxImage( GfxImage );
	glX( NULL, glTexParameteri( UsedAs, GL_TEXTURE_MIN_FILTER, Filter ) );
	glX( NULL, glTexParameteri( UsedAs, GL_TEXTURE_MAG_FILTER, Filter ) );
	SendGfxImageToGpu( GfxImage );

	return 0;
}

void DiscoSeed( void *ud ) { *((uint*)ud) = clock(); }
uint DiscoNext( void *ud ) { return rand_r( ud ); }

void DiscoRng( RNG *Rng, uint *seeds, u32 Rows, u32 Cols )
{
	dint i;

	Rng->Rows = Rows;
	Rng->Cols = Cols;

	for ( i = 0; i < IMG_FILE_VALUE_COUNT; ++i )
	{
		RNG_NEXT *Channel = Rng->raw + i;

		Channel->ud = seeds + i;
		Channel->Seed = DiscoSeed;
		Channel->Next = DiscoNext;
	}
}



dint WantGfxImage( GFX *Gfx, int *I )
{
	BLOCK *Images = GrabGfxImages( Gfx );
	FILE *out = Images->Alloc->out;
	dint i, num = Images->used;
	dint err = WantChunk( Images, sizeof(GFX_IMAGE), &i, 32 );
	GFX_IMAGE *GfxImage;

	*I = -1;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Images->used = num;
		return err;
	}

	GfxImage = GrabGfxImage( Gfx, i );
	err = InitGfxImage( GfxImage, Gfx, 1, 1 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Images->used = num;
		return err;
	}

	*I = i;

	return 0;
}

GFX *	GrabGfxImageGfxHandle( GFX_IMAGE *GfxImage )
{
	return GfxImage->Gfx;
}

BLOCK * GrabGfxImageView( GFX_IMAGE *GfxImage )
{
	return GrabBlock
	(
		GrabGfxBlocks( GrabGfxImageGfxHandle( GfxImage ) ),
		GfxImage->Img.ViewID
	);
}

dint GrowGfxImage( GFX_IMAGE *GfxImage, u32 Rows, u32 Cols )
{
	return GrowBlock( GrabGfxImageView( GfxImage ), sizeof(RGBA), Rows * Cols );
}

dint WantRngGfxImage( GFX *Gfx, int *I, RNG *Rng )
{
	BLOCK *Images = GrabGfxImages( Gfx );
	int i, num = Images->used, err = WantGfxImage( Gfx, &i );
	FILE *out = Images->Alloc->out;
	GFX_IMAGE *GfxImage;

	*I = -1;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Images->used = num;
		return err;
	}

	GfxImage = GrabGfxImage( Gfx, i );
	err = GrowGfxImage( GfxImage, Rng->Rows, Rng->Cols );
	GfxImage->Rng = *Rng;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Images->used = num;
		return err;
	}

	*I = i;

	return 0;
}

dint WantImgGfxImage( GFX *Gfx, int *I, uch const *path )
{
	BLOCK *Images = GrabGfxImages( Gfx );
	IMG_FILE *ImgFile = GrabGfxImgFile( Gfx );
	int i, num = Images->used, err;
	FILE *out = Images->Alloc->out;
	GFX_IMAGE *GfxImage;
	IMG *Img;

	i = FindImgGfxImage( Gfx, path );

	if ( i > 0 )
	{
		*I = i;
		return 0;
	}

	*I = -1;
	err = SeekImgFile( ImgFile, path, 04, 0 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		goto failed;
	}

	err = WantGfxImage( Gfx, &i );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		goto failed;
	}

	GfxImage = GrabGfxImage( Gfx, i );
	Img = &(GfxImage->Img);
	err = DrawImg( Img, ImgFile );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		goto failed;
	}

	err = GiveImgPath( Img, path );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		failed:
		Images->used = num;
	}

	SeekImgFile( ImgFile, NULL, 0, 0 );
	*I = i;

	return err;
}

IMG_FILE *	GrabGfxImgFile( GFX *Gfx )
{
	return &(Gfx->ImgFile);
}

BLOCKS *	GrabGfxBlocks( GFX *Gfx )
{
	return GrabImgFileBlocks( GrabGfxImgFile( Gfx ) );
}


void TermGfxImages( GFX *Gfx )
{
	BLOCK *Images = GrabGfxImages( Gfx );

	if ( Images )
	{
		int i;
		GFX_IMAGE *images = Images->addr;

		for ( i = 0; i < Images->used; ++i )
			TermGfxImage( images + i );
	}
}

dint InitGfxImages( GFX *Gfx )
{
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	FILE *out = Blocks->Alloc->out;
	dint i, err;

	err = WantGfxImage( Gfx, &i );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxImages( Gfx );
		return err;
	}

	return 0;
}
