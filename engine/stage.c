#include "stage.h"

GFX * GrabGfxStageGfxHandle( GFX_STAGE *Stage )
{
	return Stage->Gfx;
}

BLOCKS * GrabGfxStageBlocks( GFX_STAGE *Stage )
{
	return GrabGfxBlocks( GrabGfxStageGfxHandle( Stage ) );
}

BLOCK * GrabGfxStageStageIDs( GFX_STAGE *Stage )
{
	return GrabBlock( GrabGfxStageBlocks( Stage ), Stage->StageIDs );
}

dint * GrabGfxStageStageID( GFX_STAGE *Stage, int i )
{
	return GrabChunk( GrabGfxStageStageIDs( Stage ), sizeof(dint), i );
}

BLOCK * GrabGfxStageModelIDs( GFX_STAGE *Stage )
{
	return GrabBlock( GrabGfxStageBlocks( Stage ), Stage->ModelIDs );
}

dint * GrabGfxStageModelID( GFX_STAGE *Stage, int i )
{
	return GrabChunk( GrabGfxStageModelIDs( Stage ), sizeof(dint), i );
}

GFX *GrabStageGfxHandle( GFX_STAGE *Stage )
{
	return Stage->Gfx;
}

int InitGfxStage( GFX_STAGE *Stage, GFX *Gfx  )
{
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	FILE *out = Blocks->Alloc->out;
	dint err;

	Stage->Gfx = Gfx;
	err = WantBlock( Blocks, &(Stage->StageIDs), sizeof(dint), 0 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxStage( Stage );
		return err;
	}

	err = WantBlock( Blocks, &(Stage->ModelIDs), sizeof(dint), 0 );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		TermGfxStage( Stage );
		return err;
	}

	return 0;
}

dint InitGfxStages( GFX *Gfx )
{
	BLOCK *Stages = GrabGfxStages( Gfx );
	dint err = GrowBlock( Stages, sizeof(GFX_STAGE), 1 );

	if ( err )
	{
		FILE *out = Stages->Alloc->out;
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	Stages->used = 1;

	return 0;
}

void TermGfxStages( GFX *Gfx )
{
	BLOCK *Stages = GrabGfxStages( Gfx );
	GFX_STAGE *stages = Stages->addr;
	dint i;

	for ( i = 0; i < Stages->used; ++i )
		TermGfxStage( stages + i );
}

void TermGfxStage( GFX_STAGE *Stage )
{
	BLOCKS *Blocks = GrabGfxStageBlocks( Stage );

	if ( Blocks )
	{
		FreeBlock( Blocks, Stage->ModelIDs );
		FreeBlock( Blocks, Stage->StageIDs );
	}

	memset( Stage, 0, sizeof(GFX_STAGE) );
}

dint WantGfxStage( GFX *Gfx, dint *I )
{
	BLOCK *Stages = GrabGfxBlock( Gfx, GFX_BLOCK_STAGES );
	FILE *out = Stages->Alloc->out;
	dint i, num = Stages->used;
	dint err = WantChunk( Stages, sizeof(GFX_STAGE), &i, 4 );

	*I = -1;

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Stages->used = num;
		return err;
	}

	err = InitGfxStage( GrabGfxStage( Gfx, i ), Gfx );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		Stages->used = num;
		return err;
	}

	*I = i;

	return 0;
}

dint WantGfxStageStageID( GFX_STAGE *Stage, int *I )
{
	return WantChunk( GrabGfxStageStageIDs( Stage ), sizeof(dint), I, 8 );
}

dint WantGfxStageModelID( GFX_STAGE *Stage, int *I )
{
	return WantChunk( GrabGfxStageModelIDs( Stage ), sizeof(dint), I, 8 );
}
