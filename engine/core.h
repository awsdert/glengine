#ifndef CORE_H
#define CORE_H

#include "core_common.h"
#include "cover.h"
#include "stage.h"

typedef struct _GPU_SHADER
{
	uint ID, Type;
	BLOCKS *Blocks;
	int Built;
	int CodeID;
} GPU_SHADER;

typedef struct _GPU_BINARY
{
	uint ID;
	BLOCKS *Blocks;
	int Built, Valid, ShadersID;
} GPU_BINARY;

typedef struct _SPHERE
{
	float Span;
	uint  Pos;
} SPHERE;

typedef struct _GPU_THINGS
{
	uint Loc[THING_COUNT];
	char const *Var[THING_COUNT];
} GPU_THINGS;

typedef struct _GPU_TRAITS
{
	bool Del;
	uint Vbo;
	uint Loc[TRAIT_COUNT];
	char const *Var[TRAIT_COUNT];
} GPU_TRAITS;

typedef struct _GPU_COVERED
{
	uint 	Loc;
	char *	Var;
} GPU_COVERED;

typedef void (*TermWindow_cb)( void *window );


#define TEST_TRIO_COUNT 4

typedef enum _ENUM_TEST_TRIO_GFX
{
	TEST_TRIO_GFX_MODEL = 0,
	TEST_TRIO_GFX_JOINT,
	TEST_TRIO_GFX_STRIP,
	TEST_TRIO_GFX_COUNT
} ENUM_TEST_TRIO_GFX;

typedef struct _TEST_TRIO
{
	dint modelID;
	dint jointID;
	dint gid[TEST_TRIO_GFX_COUNT];
	dint image;
	vec4s colors[3];
	vec3s places[3];
	vec2s texels[3];
	POS_TRIO points;
	POS_HELD held[3];
} TEST_TRIO;

typedef struct _CORE
{
	bool TestMode;
	uint TestSeeds[IMG_FILE_VALUE_COUNT];
	TEST_TRIO TestTrios[TEST_TRIO_COUNT];
	dint BriefDataID;

	/* Containing Window */
	void *Window;
	vec3s Span;
	TermWindow_cb TermWindow;

	/* Gfx Stuff */
	GFX Gfx;
	GFX_STAGE *Stage;

	/* Gpu Stuff */
	GPU_BINARY	GpuBinary1, GpuBinary2, *GpuBinary;
	GPU_COVERED	GpuCovered;
	GPU_TRAITS	GpuTraits;
	GPU_THINGS	GpuThings;
} CORE;

dint		WantTestGfxModels( CORE *Core );
GFX *		GrabCoreGfxHandle( CORE *Core );

IMG_FILE *	GrabCoreImgFile( CORE *Core );
BLOCKS *	GrabCoreBlocks( CORE *Core );
GFX_STAGE *	GrabCoreStage( CORE *Core );

void		TermGpuShader( GPU_SHADER *Shader );
dint		InitGpuShader( GPU_SHADER *Shader, BLOCKS *Blocks );
dint		MakeGpuShader( GPU_SHADER *Shader, uch const * path, uint type );
void		ScanGpuShader( GPU_SHADER *Shader, uch const * path );
dint		WantGpuShader( BLOCK *Shaders, BLOCKS *Blocks, uch const * path, uint type );

void		TermGpuBinary( GPU_BINARY *GpuBinary );
dint		InitGpuBinary( GPU_BINARY *GpuBinary, BLOCKS *Blocks );
dint		MakeGpuBinary( GPU_BINARY *GpuBinary );
void		ScanGpuBinary( GPU_BINARY *GpuBinary );
BLOCK *		GrabGpuShaders( GPU_BINARY *GpuBinary );
GPU_SHADER *GrabGpuShader( GPU_BINARY *GpuBinary );

void	TermCore( CORE *Core );
dint	InitCore( CORE *Core, BLOCKS *Blocks );

dint	NameGpuConfig( CORE *Core );
dint	FindGpuConfig( CORE *Core, GPU_BINARY *GpuBinary );

uint add_trigon( CORE *Core );
uint add_trigon_to_index_buffers( CORE *Core, POS_TRIO *Trio );

void	FreeGpuIDs( CORE *Core );
dint	WantGpuIDs( CORE *Core );

#endif
