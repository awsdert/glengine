#include "core.h"

int InternalTestColors( TEST_TRIO *Trios, int i )
{
	TEST_TRIO *T = Trios + i;
	vec4s colors[3] =
	{
		{{ 0.0, 0.1, 0.2, 1.0 }},
		{{ 0.0, 0.4, 0.5, 1.0 }},
		{{ 0.0, 0.7, 0.9, 1.0 }},
	};
	int j;

	switch ( i )
	{
		case 0:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->colors[j].y = colors[j].y;
				T->colors[j].z = colors[j].z;
				T->colors[j].w = 1.0;
			}

			return 0;
		}
		case 1:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->colors[j].y = colors[j].z;
				T->colors[j].z = colors[j].y;
				T->colors[j].w = 1.0;
			}

			return 0;
		}
		case 2:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->colors[j].x = colors[j].y;
				T->colors[j].y = colors[j].z;
				T->colors[j].w = 1.0;
			}

			return 0;
		}
		case 3:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->colors[j].x = colors[j].z;
				T->colors[j].y = colors[j].y;
				T->colors[j].w = 1.0;
			}

			return 0;
		}
	}

	return ERANGE;
}

int InternalTestPlaces( TEST_TRIO *Trios, int i )
{
	TEST_TRIO *T = Trios + i;
	vec3s places[3] =
	{
		{{ -0.25, 0.25, 0.0 }},
		{{  0.25, 0.25, 0.0 }},
		{{  0.0,  0.5,  0.0 }}
	};
	int j;

	switch ( i )
	{
		case 0:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->places[j].x = places[j].x;
				T->places[j].y = places[j].y;
			}

			return 0;
		}
		case 1:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->places[j].x = places[j].x;
				T->places[j].y = -(places[j].y);
			}

			return 0;
		}
		case 2:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->places[j].x = places[j].y;
				T->places[j].y = places[j].x;
			}

			return 0;
		}
		case 3:
		{
			for ( j = 0; j < 3; ++j )
			{
				T->places[j].x = -(places[j].y);
				T->places[j].y = places[j].x;
			}

			return 0;
		}
	}

	return ERANGE;
}

int InternalTestCovers( TEST_TRIO *Trios, int i )
{
	TEST_TRIO *T = Trios + i;
	vec2s texels[3] =
	{
		{{ 0.0, 0.0 }},
		{{ 1.0, 0.0 }},
		{{ 0.5, 1.0 }}
	};
	int j;

	switch ( i )
	{
		case 2: case 3:
			for ( j = 0; j < 3; ++j )
			{
				T->texels[j].x = texels[j].x;
				T->texels[j].y = texels[j].y;
			}
		case 0: case 1: return 0;
	}

	return ERANGE;
}

void InternalInitTestFloats( TEST_TRIO *Trios )
{
	int i;
	for ( i = 0; i < TEST_TRIO_COUNT; ++i )
	{
		(void)InternalTestColors( Trios, i );
		(void)InternalTestPlaces( Trios, i );
		(void)InternalTestCovers( Trios, i );
	}
}

int InternalGiveTestFloats( GFX *Gfx, TEST_TRIO *Trios )
{
	int i, err;
	FILE *out = GrabGfxBlocks( Gfx )->Alloc->out;

	for ( i = 0; i < TEST_TRIO_COUNT; ++i )
	{
		int j;
		TEST_TRIO *T = Trios + i;
		POS_TRIO *Trio = &(T->points);

		for ( j = 0; j < 3; ++j )
		{
			POS_GIVE *Pos = Trio->raw + j;
			int *id[2] = { &(Pos->Color), &(Pos->Place) }, k;
			ENUM_CACHE caches[] = { CACHE_QUADS, CACHE_TRIOS };
			float *floats[] = { T->colors[j].raw, T->places[j].raw };

			for ( k = 0; k < 2; ++k )
			{
				err = GiveGfxFloats( Gfx, caches[k], id[k], floats[k] );

				if ( err )
				{
					err = ENODATA;
					ECHO( out, ECHO_ERR( out, err ) );
					return err;
				}
			}
		}
	}

	for ( i = 0; i < 3; ++i )
	{
		TEST_TRIO *T = Trios + 2;
		POS_TRIO *Trio = &(T->points);
		POS_GIVE *Pos = Trio->raw + i;
		float *texels = T->texels[i].raw;

		err = GiveGfxFloats( Gfx, CACHE_PAIRS, &(Pos->Texel), texels );

		if ( Pos->Texel < 0 )
		{
			err = ENODATA;
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}
	}

	return 0;
}

int InternalWantTestGfxModels( CORE *Core, TEST_TRIO *Trios )
{
	int i, err;
	GFX *Gfx = GrabCoreGfxHandle( Core );
	GFX_STAGE *Stage = GrabCoreStage( Core );
	FILE *out = GrabCoreBlocks( Core )->Alloc->out;
	uint DrawAs[4] =
		{ GL_TRIANGLES, GL_TRIANGLES, GL_TRIANGLE_STRIP, GL_TRIANGLE_STRIP };

	for ( i = 0; i < TEST_TRIO_COUNT; ++i )
	{
		TEST_TRIO *T = Trios + i;
		POS_TRIO *Trio = &(T->points);
		GFX_MODEL *Model;
		GFX_STRIP *Strip;
		GFX_JOINT *Joint;
		dint *id, j;

		for ( j = 0; j < TEST_TRIO_GFX_COUNT; ++j )
		{
			err = WantGfxChunk( Gfx, j + GFX_BLOCK_MODELS, T->gid + j );

			if ( err )
			{
				ECHO( out, ECHO_ERR( out, err ) );
				return err;
			}
		}

		/* Add mesh */

		Strip = GrabGfxStrip( Gfx, T->gid[TEST_TRIO_GFX_STRIP] );
		Strip->DrawAs = DrawAs[i];
		err = GiveGfxStripTrio( Strip, Trio );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}

		/* Link mesh to model */

		Joint = GrabGfxJoint( Gfx, T->gid[TEST_TRIO_GFX_JOINT] );
		GiveGfxJointStripID( Joint, T->gid[TEST_TRIO_GFX_STRIP] );
		Model = GrabGfxModel( Gfx, T->gid[TEST_TRIO_GFX_MODEL] );
		err = WantGfxModelJointID( Model, &(T->jointID) );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}

		id = GrabGfxModelJointID( Model, T->jointID );
		*id = T->gid[TEST_TRIO_GFX_JOINT];

		/* Link model to stage/scene */

		err = WantGfxStageModelID( Stage, &(T->modelID) );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}

		id = GrabGfxStageModelID( Stage, T->modelID );
		*id = T->gid[TEST_TRIO_GFX_MODEL];
	}

	return 0;
}

int WantTestGfxModels( CORE *Core )
{
	GFX *Gfx = GrabCoreGfxHandle( Core );
	BLOCKS *Blocks = GrabGfxBlocks( Gfx );
	FILE *out = Blocks->Alloc->out;
	TEST_TRIO *Trios = Core->TestTrios;
	int i, err;

	InternalInitTestFloats( Trios );
	err = InternalGiveTestFloats( Gfx, Trios );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	/* Add each trigon */

	err = InternalWantTestGfxModels( Core, Trios );

	if ( err )
	{
		ECHO( out, ECHO_ERR( out, err ) );
		return err;
	}

	/* Keeps scope clean and as a side bonus makes it easier for me to later
	 * cleanup any non-C89 compliant code, this method gets around the
	 * problem of declaring variables at the top of scope only and since a
	 * false is never found the CPU will always preload the correct
	 * instructions (assuming it preloads at all) */
	if ( 1 )
	{
		TEST_TRIO *T = Trios + 2;
		GFX_JOINT *Joint;
		GFX_IMAGE *Image;
		RNG Rng;
		u32 max = (UCH_MIN != 0) ? (UCH_MAX << 1) | 1 : UCH_MAX;

		DiscoRng( &Rng, Core->TestSeeds, max, max );
		err = WantRngGfxImage( Gfx, &(T->image), &Rng );

		if ( err )
		{
			ECHO( out, ECHO_ERR( out, err ) );
			return err;
		}

		Image = GrabGfxImage( Gfx, T->image );
		Joint = GrabGfxJoint( Gfx, T->gid[TEST_TRIO_GFX_JOINT] );
		GiveGfxJointImageID( Joint, T->image );

		(void)Image;

		err = 0;
	}

	/* Report results */

	if ( Blocks->Alloc->lod )
	{
		for ( i = 0; i < TEST_TRIO_COUNT; ++i )
		{
			TEST_TRIO *T = Trios + i;
			dint id = T->gid[TEST_TRIO_GFX_MODEL];

			ECHO
			(
				out,
				fprintf( out, "GfxModel %d (%d)\n", i, id );
				EchoGfxModel( GrabGfxModel( Gfx, id ) )
			);
		}
	}

	return 0;
}
