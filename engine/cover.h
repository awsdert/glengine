#ifndef INC_ENGINE_COVER_H
#define INC_ENGINE_COVER_H
#include "strip.h"

void	TermGfxImage( GFX_IMAGE *GfxImage );
dint	InitGfxImage( GFX_IMAGE *GfxImage, GFX *Gfx, u32 Rows, u32 Cols );
GFX *	GrabGfxImageGfxHandle( GFX_IMAGE *GfxImage );

void	DiscoRng( RNG *Rng, uint *seeds, u32 Rows, u32 Cols );

dint	WantGfxImageGpuId( GFX_IMAGE *GfxImage, uint UsedAs, uint Filter );
void	TellGpuOfGfxImage( GFX_IMAGE *GfxImage );
void	SendGfxImageToGpu( GFX_IMAGE *GfxImage );

#endif
