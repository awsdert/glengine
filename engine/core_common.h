#ifndef INC_ENGINE_COMMON_H
#define INC_ENGINE_COMMON_H

#include "../common/unit.h"
#include "../common/data.h"
#include "../common/path.h"
#include "../visual/view.h"
#include "../visual/image.h"
#include "../visual/image/png.h"
#include "../visual/image/ppm.h"
#include <cglm/struct.h>
#include <cglm/cglm.h>
#include <cglm/call.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <stddef.h>

typedef char TEMP[BUFSIZ];

/* IMPORTANT: DO NOT CHANGE ORDER OF CACHE'S */
typedef enum _ENUM_CACHE
{
	CACHE_SOLOS = 0,
	CACHE_PAIRS,
	CACHE_TRIOS,
	CACHE_QUADS,
	CACHE_COUNT
} ENUM_CACHE;

typedef enum _ENUM_TRAIT
{
	/* vec2, Texture Coordinate */
	TRAIT_TEXEL = 0,
	/* vec4 */
	TRAIT_COLOR,
	/* vec3 */
	TRAIT_PLACE,
	TRAIT_NORMAL,
	TRAIT_TANGENT,
	TRAIT_BINORMAL,
	TRAIT_COUNT
} ENUM_TRAIT;

typedef enum _ENUM_THING
{
	THING_STRIP = 0,
	THING_GFX_MODEL,
	THING_STAGE,
	THING_COUNT
} ENUM_THING;

typedef union _CHANGE
{
	float raw[10];
	struct
	{
		vec4 Tint;
		vec3 Spot;
		vec3 Span;
	};
} CHANGE;

typedef dint (*WantGpuBlock_cb)( uint *id );
typedef void (*FreeGpuBlock_cb)( uint id );
typedef void (*BindGpuGroup_cb)( uint id );
typedef dint (*WantGpuBlocks_cb)( uint *list, uint count );
typedef void (*FreeGpuBlocks_cb)( uint *list, uint count );
typedef void (*BindGpuBlock_cb)( uint type, uint id );
typedef void (*BindGpuCache_cb)( uint type, uint id, uint loc, ENUM_CACHE i );
typedef void (*SendGpuBlock_cb)( uint type, uint id, BLOCK *Block, uint DrawAs );

typedef enum _ENUM_GFX_BLOCKS
{
	GFX_BLOCK_BUFFER = CACHE_COUNT,
	GFX_BLOCK_STAGES,
	GFX_BLOCK_MODELS,
	GFX_BLOCK_JOINTS,
	GFX_BLOCK_STRIPS,
	GFX_BLOCK_IMAGES,
	GFX_BLOCK_COUNT
} ENUM_GFX_BLOCKS;

typedef struct _GFX
{
	dint	UniqueIdCount;
	IMG_FILE ImgFile;
	dint	BlockIDs[GFX_BLOCK_COUNT];
	size_t	MapSizes[GFX_BLOCK_COUNT];
	ENUM_CACHE MapTrait[TRAIT_COUNT];
	uint	CacheVao;
	uint	CacheVBOs[CACHE_COUNT];
	WantGpuBlocks_cb WantVaoGroups, WantVboBlocks, WantImgBlocks;
	FreeGpuBlocks_cb FreeVaoGroups, FreeVboBlocks, FreeImgBlocks;
	WantGpuBlock_cb WantVaoGroup, WantVboBlock, WantImgBlock;
	FreeGpuBlock_cb FreeVaoGroup, FreeVboBlock, FreeImgBlock;
	BindGpuGroup_cb BindVaoGroup;
	BindGpuBlock_cb BindVboBlock;
	BindGpuCache_cb BindVboCache;
	SendGpuBlock_cb SendVboBlock;
} GFX;

typedef struct _GFX_OBJECT
{
	GFX *Gfx;
	dint BufferId;
	dint UniqueId;
	dint ThreadId;
	dint DubbedId;
} GFX_OBJECT;

typedef struct _GFX_STAGE GFX_STAGE;
typedef struct _GFX_MODEL GFX_MODEL;
typedef struct _GFX_JOINT GFX_JOINT;
typedef struct _GFX_STRIP GFX_STRIP;
typedef struct _GFX_IMAGE GFX_IMAGE;

BLOCKS	*	GrabGfxBlocks( GFX *Gfx );
IMG_FILE *	GrabGfxImgFile( GFX *Gfx );

dint	InitGfx( GFX *Gfx, BLOCKS *Blocks );
/* These are for internal separation of initialisation, only call InitGfx() */
dint	InitGfxSolos( GFX *Gfx );
dint	InitGfxPairs( GFX *Gfx );
dint	InitGfxTrios( GFX *Gfx );
dint	InitGfxQuads( GFX *Gfx );
dint	InitGfxBuffer( GFX *Gfx );
dint	InitGfxStages( GFX *Gfx );
dint	InitGfxModels( GFX *Gfx );
dint	InitGfxJoints( GFX *Gfx );
dint	InitGfxStrips( GFX *Gfx );
dint	InitGfxImages( GFX *Gfx );

void	TermGfx( GFX *Gfx );
/* These are for internal separation of termination, only call TermGfx() */
void	TermGfxSolos( GFX *Gfx );
void	TermGfxPairs( GFX *Gfx );
void	TermGfxTrios( GFX *Gfx );
void	TermGfxQuads( GFX *Gfx );
void	TermGfxBuffer( GFX *Gfx );
void	TermGfxStages( GFX *Gfx );
void	TermGfxModels( GFX *Gfx );
void	TermGfxJoints( GFX *Gfx );
void	TermGfxStrips( GFX *Gfx );
void	TermGfxImages( GFX *Gfx );

BLOCK *	GrabGfxBlock(	GFX *Gfx, ENUM_GFX_BLOCKS i );
#define GrabGfxSolos(	GFX ) GrabGfxBlock( GFX, CACHE_SOLOS )
#define GrabGfxPairs(	GFX ) GrabGfxBlock( GFX, CACHE_PAIRS )
#define GrabGfxTrios(	GFX ) GrabGfxBlock( GFX, CACHE_TRIOS )
#define GrabGfxQuads(	GFX ) GrabGfxBlock( GFX, CACHE_QUADS )
#define GrabGfxBuffer(	GFX ) GrabGfxBlock( GFX, GFX_BLOCK_BUFFER )
#define GrabGfxStages(	GFX ) GrabGfxBlock( GFX, GFX_BLOCK_STAGES )
#define GrabGfxModels(	GFX ) GrabGfxBlock( GFX, GFX_BLOCK_MODELS )
#define GrabGfxJoints(	GFX ) GrabGfxBlock( GFX, GFX_BLOCK_JOINTS )
#define GrabGfxStrips(	GFX ) GrabGfxBlock( GFX, GFX_BLOCK_STRIPS )
#define GrabGfxImages(	GFX ) GrabGfxBlock( GFX, GFX_BLOCK_IMAGES )

void * GrabGfxChunk(	GFX *Gfx, ENUM_GFX_BLOCKS i, dint j );
#define GrabGfxSolo(	GFX, I ) GrabGfxChunk( GFX, CACHE_SOLOS, I )
#define GrabGfxPair(	GFX, I ) GrabGfxChunk( GFX, CACHE_PAIRS, I )
#define GrabGfxTrio(	GFX, I ) GrabGfxChunk( GFX, CACHE_TRIOS, I )
#define GrabGfxQuad(	GFX, I ) GrabGfxChunk( GFX, CACHE_QUADS, I )
#define GrabGfxStage(	GFX, I ) GrabGfxChunk( GFX, GFX_BLOCK_STAGES, I )
#define GrabGfxModel(	GFX, I ) GrabGfxChunk( GFX, GFX_BLOCK_MODELS, I )
#define GrabGfxJoint(	GFX, I ) GrabGfxChunk( GFX, GFX_BLOCK_JOINTS, I )
#define GrabGfxStrip(	GFX, I ) GrabGfxChunk( GFX, GFX_BLOCK_STRIPS, I )
#define GrabGfxImage(	GFX, I ) GrabGfxChunk( GFX, GFX_BLOCK_IMAGES, I )

dint		WantGfxChunk(	GFX *Gfx, ENUM_GFX_BLOCKS i, dint *J );
dint		WantGfxSolo(	GFX *Gfx, dint *I );
dint		WantGfxPair(	GFX *Gfx, dint *I );
dint		WantGfxTrio(	GFX *Gfx, dint *I );
dint		WantGfxQuad(	GFX *Gfx, dint *I );
dint		WantGfxBrief(	GFX *Gfx, size_t size );
dint		WantGfxStage(	GFX *Gfx, dint *I );
dint		WantGfxModel(	GFX *Gfx, dint *I );
dint		WantGfxJoint(	GFX *Gfx, dint *I );
dint		WantGfxStrip(	GFX *Gfx, dint *I );
dint		WantGfxImage(	GFX *Gfx, dint *I );
dint		WantGfxMemo(	GFX *Gfx, dint *I, uch const *memo );

void	EchoFloats(		FILE *out, float *floats, ENUM_CACHE count );

dint	UsedGfxFloats( GFX *Gfx, ENUM_CACHE cache );
dint	HaveGfxFloats( GFX *Gfx, ENUM_CACHE cache );
float *	GrowGfxFloats( GFX *Gfx, ENUM_CACHE cache, int want );
float * GrabGfxFloats( GFX *Gfx, ENUM_CACHE cache, int i );
dint	FindGfxFloats( GFX *Gfx, ENUM_CACHE cache, float const *src );
dint	GiveGfxFloats( GFX *Gfx, ENUM_CACHE cache, int *I, float const *src );

char const * glSourceToString( GLenum source );
char const * glReportToString( GLuint report );
char const * glWeightToString( GLenum weight );
char const * glPursueToString( GLuint pursue );

void glClearErrors();
bool glHadNoErrors
	( void *ud, char const * file, uint line, char const * call );
void GLAPIENTRY glDebugMsgCB
(
	GLenum source
	, GLenum report
	, GLuint id
	, GLenum weight
	, GLsizei length
	, const GLchar *msg
	, const void *ud
);

#ifdef _DEBUG
#define glX( UD, CALL ) \
	if ( 1 ) \
	{ \
		glClearErrors(); \
		CALL; \
		glHadNoErrors( UD, __FILE__, __LINE__, #CALL ); \
	}
#else
#define glX( UD, CALL ) CALL
#endif

#endif
